from tempfile import TemporaryDirectory

from shyft.time_series import no_utctime, time, Int64Vector, utctime_now
from shyft.energy_market.core import ModelInfo, run_state, Run, RunVector, RunClient, RunServer


def test_run_info():
    a = Run()
    assert a
    assert a.id == 0
    assert len(a.name) == 0
    assert len(a.json) == 0
    assert a.created == no_utctime
    t0 = time('2018-10-11T01:02:03Z')

    b = Run(id=1, name='xtra', created=t0, json='{abc:1}', mid=2)
    assert a != b
    assert b.id == 1
    assert b.name == 'xtra'
    assert b.created == t0
    assert b.json == '{abc:1}'
    assert b.mid == 2
    assert b.state == run_state.R_CREATED
    b.state = run_state.R_FROZEN
    assert b.state == run_state.R_FROZEN

    v = RunVector()
    v[:] = [a, b]
    assert len(v) == 2
    assert v[0] == a
    assert v[1] == b


def test_run_client_server():
    with TemporaryDirectory() as root_dir:
        s = RunServer(str(root_dir))
        port = s.start_server()
        c = RunClient(host_port=f'localhost:{port}', timeout_ms=1000)
        assert s
        assert c
        t0 = time('2018-10-11T01:02:03Z')
        mids = Int64Vector()
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        m = Run(id=0, name='x', created=t0, json='some', mid=123)
        m.id = 0
        mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
        mid = c.store_model(m=m, mi=mi)
        m.id = mid
        mr = c.read_model(mid=mid)
        mi.name = 'Hello world'
        mi.id = mid
        c.update_model_info(mid=mid, mi=mi)
        assert mr
        assert mr == m
        c.close()  # just to illustrate we can disconnect, and reconnect automagigally
        mis = c.get_model_infos(mids)
        assert len(mis) == 1
        assert mis[0].name == mi.name
        c.remove_model(mid)
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
