from .utility import create_hydro_power_system
from shyft.energy_market.core import HydroPowerSystem
from shyft.energy_market.core import Model, ModelBuilder


class TestWithHydroPowerSystem:
    def test_can_create_hps(self):
        a = create_hydro_power_system(hps_id=1, name='hps')
        assert a is not None
        assert len(a.reservoirs) >0
        assert a.reservoirs[0].hps is not None
        assert a.reservoirs[0].hps.name == a.name

        mb = ModelBuilder(Model(1, "m1"))
        ma = mb.create_model_area(1, "a")
        ma.detailed_hydro = a
        assert ma.detailed_hydro is not None


    def test_equal_structure(self):
        a = create_hydro_power_system(hps_id=1, name='a')
        b = create_hydro_power_system(hps_id=2, name='b')
        assert a.equal_structure(b)

    def test_serialize_and_deserialize_xml(self):
        b = create_hydro_power_system(hps_id=1, name='hps')
        blob_of_b = b.to_blob()
        b_2 = HydroPowerSystem.from_blob(blob_of_b)
        assert b.equal_structure(b_2)

if __name__ == '__main__':
    t = TestWithHydroPowerSystem()
    t.test_equal_structure()
