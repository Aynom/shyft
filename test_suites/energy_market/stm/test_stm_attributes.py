from shyft.energy_market.stm import Reservoir, HydroPowerSystem


def test_reservoir_attributes():
    hps = HydroPowerSystem(1, "Reservoir hps")
    res = hps.create_reservoir(2, "Reservoir one", "")
    assert hasattr(res, "lrl")
    assert hasattr(res, "hrl")
    assert hasattr(res, "volume_max_static")
    assert hasattr(res, "volume_descr")
    assert hasattr(res, "spill_descr")
    assert hasattr(res, "endpoint_desc")
    assert hasattr(res, "inflow")
    assert hasattr(res, "level_historic")
    assert hasattr(res, "level_schedule")
    assert hasattr(res, "level_min")
    assert hasattr(res, "level_max")
    assert hasattr(res, "ramping_level_up_h")
    assert hasattr(res, "ramping_level_up_d")
    assert hasattr(res, "ramping_level_down_h")
    assert hasattr(res, "ramping_level_down_d")
    assert hasattr(res, "level_max")
    assert hasattr(res, "volume")
    assert hasattr(res, "level")


def test_aggregate_attributes():
    hps = HydroPowerSystem(1, "Aggregate hps")
    agg = hps.create_aggregate(2, "Aggregate one", "")
    assert hasattr(agg, "production_min_static")
    assert hasattr(agg, "production_max_static")
    assert hasattr(agg, "generator_efficiency")
    assert hasattr(agg, "turbine_description")
    assert hasattr(agg, "unavailability")
    assert hasattr(agg, "production_min")
    assert hasattr(agg, "production_max")
    assert hasattr(agg, "production_schedule")
    assert hasattr(agg, "discharge_min")
    assert hasattr(agg, "discharge_max")
    assert hasattr(agg, "discharge_schedule")
    assert hasattr(agg, "cost_start")
    assert hasattr(agg, "cost_stop")
    assert hasattr(agg, "production")
    assert hasattr(agg, "discharge")


def test_power_station_attributes():
    hps = HydroPowerSystem(1, "Power station hps")
    pwr_station = hps.create_power_station(2, "Power station", "")
    assert hasattr(pwr_station, "outlet_level")
    assert hasattr(pwr_station, "mip")
    assert hasattr(pwr_station, "unavailability")
    assert hasattr(pwr_station, "production_min")
    assert hasattr(pwr_station, "production_max")
    assert hasattr(pwr_station, "production_schedule")
    assert hasattr(pwr_station, "discharge_min")
    assert hasattr(pwr_station, "discharge_max")
    assert hasattr(pwr_station, "discharge_schedule")


def test_water_route_attributes():
    hps = HydroPowerSystem(1, "Water route hps")
    wtr_route = hps.create_water_route(2, "Water route", "")
    assert hasattr(wtr_route, "discharge_max_static")
    assert hasattr(wtr_route, "head_loss_coeff")
    assert hasattr(wtr_route, "head_loss_func")
    assert hasattr(wtr_route, "discharge")


def test_gate_attributes():
    hps = HydroPowerSystem(1, "Water route hps")
    gate = hps.create_water_route(2, "Water route", "").add_gate(3, "Gate", "")
    assert hasattr(gate, "opening_schedule")
    assert hasattr(gate, "discharge_schedule")
    assert hasattr(gate, "discharge")
