from tempfile import TemporaryDirectory
from pathlib import Path
from shyft.time_series import Int64Vector, utctime_now, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE, time
from shyft.energy_market.core import ModelInfo
from shyft.energy_market.stm import HpsClient, HpsServer, StmClient, StmServer
from shyft.energy_market.stm import StmSystem, MarketArea
from .models import create_test_hydro_power_system


def test_hps_client_server():
    with TemporaryDirectory() as root_dir:
        s = HpsServer(str(root_dir))
        port = s.start_server()
        c = HpsClient(host_port=f'localhost:{port}', timeout_ms=1000)
        assert s
        assert c
        mids = Int64Vector()
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        m = create_test_hydro_power_system(hps_id=0, name='hps m1')
        m.id = 0
        mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
        mid = c.store_model(m=m, mi=mi)
        m.id = mid
        mr = c.read_model(mid=mid)
        mi.name = 'Hello world'
        mi.id = mid
        c.update_model_info(mid=mid, mi=mi)
        assert mr
        # equality not yet impl: assert mr == m
        c.close()  # just to illustrate we can disconnect, and reconnect automagigally
        mis = c.get_model_infos(mids)
        assert len(mis) == 1
        assert mis[0].name == mi.name
        c.remove_model(mid)
        mis = c.get_model_infos(mids)
        assert len(mis) == 0


def create_stm_sys(stm_id: int, name: str, json: str) -> StmSystem:
    a = StmSystem(uid=stm_id, name=name, json=json)
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    return a


def test_stm_client_server():
    with TemporaryDirectory() as root_dir:
        s = StmServer(str(root_dir))
        port = s.start_server()
        c = StmClient(host_port=f'localhost:{port}', timeout_ms=1000)
        assert s
        assert c
        mids = Int64Vector()
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        m = create_stm_sys(stm_id=0, name='stm m1', json="{}")
        m.id = 0
        mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
        mid = c.store_model(m=m, mi=mi)
        m.id = mid
        mr = c.read_model(mid=mid)
        mi.name = 'Hello world'
        mi.id = mid
        c.update_model_info(mid=mid, mi=mi)
        assert mr
        # equality not yet impl: assert mr == m
        c.close()  # just to illustrate we can disconnect, and reconnect automagigally
        mis = c.get_model_infos(mids)
        assert len(mis) == 1
        assert mis[0].name == mi.name
        c.remove_model(mid)
        mis = c.get_model_infos(mids)
        assert len(mis) == 0


def test_stm_client_server_read_old_data():
    root_dir = Path(__file__).parent/"model_data"
    s = StmServer(str(root_dir))
    port = s.start_server()
    c = StmClient(host_port=f'localhost:{port}', timeout_ms=1000)
    mid = 1
    mr = c.read_model(mid)
    m = create_stm_sys(stm_id=1, name='stm m1', json="{}")
    assert m.id == mr.id
    assert m.hydro_power_systems[0].equal_structure(mr.hydro_power_systems[0]), 'ensure hps are equal'
