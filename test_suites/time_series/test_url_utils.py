from shyft.time_series import (urlencode, urldecode, extract_shyft_url_query_parameters, extract_shyft_url_path, extract_shyft_url_container, shyft_url)

""" Verify the python exposure for url-utils """


def test_url_encode_decode():
    for s in ['', 'a', ' ', 'This is æøåÆØÅ', 'Something Special %12!']:
        assert urldecode(urlencode(s)) == s
        assert urldecode(urlencode(s, False), False) == s


def test_shyft_url():
    assert shyft_url('test', 'a/ts/path.db') == 'shyft://test/a/ts/path.db'
    assert shyft_url('test', 'a/ts/path.db', {'a': '123'}) == 'shyft://test/a/ts/path.db?a=123'


def test_extract_shyft_url_container():
    assert 'test' == extract_shyft_url_container('shyft://test/something')


def test_extract_shyft_url_path():
    assert 'something/strange/here.db' == extract_shyft_url_path('shyft://test/something/strange/here.db')


def test_extract_shyft_query_parameters():
    assert {'a': '123'} == extract_shyft_url_query_parameters('shyft://test/something/strange/here.db?a=123')
