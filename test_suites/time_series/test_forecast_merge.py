
import numpy as np

from shyft.time_series import Calendar
from shyft.time_series import TsVector
from shyft.time_series import TimeSeries
from shyft.time_series import TimeAxis
from shyft.time_series import point_interpretation_policy as ts_point_fx
from shyft.time_series import deltahours
from shyft.time_series import DoubleVector as dv


def _create_forecasts(t0: int, dt: int, n: int, fc_dt: int, fc_n: int) -> TsVector:
    tsv = TsVector()
    stair_case = ts_point_fx.POINT_AVERAGE_VALUE
    for i in range(fc_n):
        ta = TimeAxis(t0 + i*fc_dt, dt, n)
        mrk = (i + 1)/100.0
        v = dv.from_numpy(np.linspace(1 + mrk, 1 + n + mrk, n, endpoint=False))
        tsv.append(TimeSeries(ta, v, stair_case))
    return tsv


def test_merge_arome_forecast():
    utc = Calendar()
    t0 = utc.time(2017, 1, 1)
    dt = deltahours(1)
    n = 66  # typical arome
    fc_dt_n_hours = 6
    fc_dt = deltahours(fc_dt_n_hours)
    fc_n = 4*10  # 4 each day 10 days
    fc_v = _create_forecasts(t0, dt, n, fc_dt, fc_n)

    m0_6 = fc_v.forecast_merge(deltahours(0), fc_dt)
    m1_7 = fc_v.forecast_merge(deltahours(1), fc_dt)
    assert m0_6 is not None
    assert m1_7 is not None
    # check returned sizes
    assert m0_6.size() == fc_n*fc_dt_n_hours
    assert m1_7.size() == fc_n*fc_dt_n_hours
    # some few values (it's fully covered in c++ tests
    assert round(abs(m0_6.value(0) - 1.01), 7) == 0
    assert round(abs(m0_6.value(6) - 1.02), 7) == 0
    assert round(abs(m1_7.value(0) - 2.01), 7) == 0
    assert round(abs(m1_7.value(6) - 2.02), 7) == 0
