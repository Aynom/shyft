from builtins import range
from shyft.time_series import (Calendar, TimeAxis, TimeAxisFixedDeltaT, deltahours, TimeAxisByPoints, UtcTimeVector, npos)
import numpy as np


class TimeAxisUtil:
    """Verify and illustrate TimeAxis
       defined as n periods non-overlapping ascending
        
     """

    def __init__(self):
        self.c = Calendar()
        self.d = deltahours(1)
        self.n = 24
        self.t = self.c.trim(self.c.time(1969, 12, 31, 0, 0, 0), self.d)
        self.ta = TimeAxis(self.t, self.d, self.n)


def test_index_of():
    u = TimeAxisUtil()
    assert u.ta.index_of(u.t) == 0
    assert u.ta.index_of(u.t, 0) == 0
    assert u.ta.index_of(u.t - 3600) == npos
    assert u.ta.open_range_index_of(u.t) == 0
    assert u.ta.open_range_index_of(u.t, 0) == 0
    assert u.ta.open_range_index_of(u.t - 3600) == npos


def test_create_timeaxis():
    u = TimeAxisUtil()
    assert u.ta.size() == u.n
    assert len(u.ta) == u.n
    assert u.ta(0).start == u.t
    assert u.ta(0).end == u.t + u.d
    assert u.ta(1).start == u.t + u.d
    assert u.ta.total_period().start == u.t
    va = np.array([86400, 3600, 3], dtype=np.int64)
    xta = TimeAxisFixedDeltaT(int(va[0]), int(va[1]), int(va[2]))
    assert xta.size() == 3


def test_iterate_timeaxis():
    u = TimeAxisUtil()
    tot_dt = 0
    for p in u.ta:
        tot_dt += p.timespan()
    assert tot_dt == u.n*u.d


def test_timeaxis_str():
    u = TimeAxisUtil()
    s = str(u.ta)
    assert len(s) > 10
    assert repr(u.ta) == str(u.ta)


def test_point_time_axis_():
    """
    A point time axis takes n+1 points do describe n-periods, where
    each period is defined as [ point_i .. point_i+1 >
    """
    u = TimeAxisUtil()
    all_points = UtcTimeVector([t for t in range(int(u.t), int(u.t + (u.n + 1)*u.d), int(u.d))])
    tap = TimeAxisByPoints(all_points)
    assert tap.size() == u.ta.size()
    for i in range(u.ta.size()):
        assert tap(i) == u.ta(i)
    assert tap.t_end == all_points[-1], "t_end should equal the n+1'th point if supplied"
    s = str(tap)
    assert len(s) > 0
    assert str(tap) == repr(tap)


def test_generic_time_axis():
    c = Calendar('Europe/Oslo')
    dt = deltahours(1)
    n = 240
    t0 = c.time(2016, 4, 10)

    tag1 = TimeAxis(t0, dt, n)
    assert len(tag1) == n
    assert tag1.time(0) == t0

    tag2 = TimeAxis(c, t0, dt, n)
    assert len(tag2) == n
    assert tag2.time(0) == t0
    assert tag2.calendar_dt.calendar is not None
    assert repr(tag1) == str(tag1)
    assert repr(tag2) == str(tag2)


def test_time_axis_time_points():
    c = Calendar('Europe/Oslo')
    dt = deltahours(1)
    n = 240
    t0 = c.time(2016, 4, 10)
    ta = TimeAxis(c, t0, dt, n)
    tp = ta.time_points
    assert tp is not None
    assert len(tp) == n + 1
    assert len(TimeAxis(c, t0, dt, 0).time_points) == 0


def test_time_axis_time_points_double():
    dt = 1.5
    n = 240
    t0 = 0
    ta = TimeAxis(t0, dt, n)
    tp = ta.time_points_double
    assert tp is not None
    assert len(tp) == n + 1
    assert len(TimeAxis(t0, dt, 0).time_points_double) == 0
    assert ta.time_points_double[1] == 1.5
