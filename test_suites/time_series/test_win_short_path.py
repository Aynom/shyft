from shyft.time_series import win_short_path, win_set_priority
import os


def test_basic_translate():
    long_path = r"C:\Program Files"
    s = win_short_path(long_path)
    if os.name == 'nt':
        assert s == r'C:\PROGRA~1'
        s2 = win_short_path(r"C:\Program File")
        assert not s2
    else:
        assert s == long_path


def test_win_set_priority():
    win_set_priority(-1)  # below normal and no io/mem performance
    win_set_priority(0)  # normal, hopefully a sane setting
