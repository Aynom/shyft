@echo off
@rem simple conda build/upload script that
@rem a) set version
@rem b) conda build (and upload if you have set upload to true)
@rem preconditions: shyft is built and tested

pushd "%~dp0"\..

git rev-list --count HEAD >VERSION.tmp
set /p minor=<VERSION.tmp
echo 4.7.%minor% >VERSION
set SHYFT_VERSION=4.7.%minor%
echo Starting to build shyft.time_series  %SHYFT_VERSION% numpy %SHYFT_BOOST_NUMPY_VERSION%
call conda build --numpy %SHYFT_BOOST_NUMPY_VERSION% --no-test --no-copy-test-source-files --label %1 conda_recipe\time_series
echo Starting to build shyft complete  %SHYFT_VERSION% numpy %SHYFT_BOOST_NUMPY_VERSION%
call conda build --numpy %SHYFT_BOOST_NUMPY_VERSION% --no-test --no-copy-test-source-files --label %1 conda_recipe\all
popd
exit /b %errorlevel%
