# CMake file for compiling the C++ core library
# library sources
set(core_sources   ../dtss/dtss_client.cpp ../hydrology/optimizers/dream_optimizer.cpp ../hydrology/optimizers/sceua_optimizer.cpp
    ../time_series/time_series_dd.cpp core_serialization.cpp ../time_series/time_series_serialization.cpp
    ../time/utctime_utilities.cpp ../time_series/expression_serialization.cpp ../time_series/time_series_dd_eval.cpp
    ../web_api/beast_server.cpp ../web_api/dtss_web_api.cpp ../web_api/dtss_web_api_server.cpp
    ../web_api/grammar/time.cpp ../web_api/grammar/utcperiod.cpp ../web_api/grammar/read_ts_request.cpp
    ../web_api/grammar/find_ts_request.cpp ../web_api/grammar/info_request.cpp ../web_api/grammar/ts_points.cpp
    ../web_api/grammar/time_points.cpp ../web_api/grammar/ts_values.cpp ../web_api/grammar/time_axis.cpp 
    ../web_api/grammar/apoint_ts.cpp ../web_api/grammar/ats_vector.cpp ../web_api/grammar/average_ts_request.cpp
    ../web_api/grammar/percentile_ts_request.cpp ../web_api/grammar/store_ts_request.cpp 
    ../web_api/grammar/web_request.cpp  ../web_api/grammar/error_handler.cpp ../web_api/grammar/quoted_string.cpp
)
add_library(shyft_core STATIC ${core_sources})
if (MSVC)
    set_target_properties(shyft_core PROPERTIES
        DEBUG_POSTFIX "_debug"
        COMPILE_PDB_NAME "shyft_core"
        COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    )
endif()
target_link_libraries(shyft_core PUBLIC OpenSSL::SSL OpenSSL::Crypto ${boost_link_libraries})
if(BUILD_COVERAGE)
    include(${PROJECT_SOURCE_DIR}/build_support/CodeCoverage.cmake)
    APPEND_COVERAGE_COMPILER_FLAGS()
    set(COVERAGE_EXCLUDES '/usr/include/*' '*/boost/*' '*/armadillo*' '*/dlib/*' '*/doctest/*' '${SHYFT_DEPENDENCY_DIR}/include/*' )
    target_link_libraries(shyft_core PUBLIC gcov)
endif()


set(inst_root "${SHYFT_DEPENDENCIES}/")

install(TARGETS shyft_core DESTINATION ${inst_root}lib)
if (MSVC AND CMAKE_BUILD_TYPE STREQUAL "Debug")
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/shyft_core.pdb DESTINATION ${inst_root}lib )
endif()
install(FILES ${CMAKE_SOURCE_DIR}/build_support/shyft_core-config.cmake
    DESTINATION ${inst_root}lib/cmake/shyft_core
    )
install(DIRECTORY  ${CMAKE_SOURCE_DIR}/cpp/shyft
    DESTINATION ${inst_root}include
    FILES_MATCHING PATTERN "*.h"
    PATTERN "obj" EXCLUDE
    PATTERN "x64" EXCLUDE
    )
