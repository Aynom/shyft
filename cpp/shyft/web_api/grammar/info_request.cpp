#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline info_request mk_info_request(std::string const&req_id) {
        return info_request{req_id};
    }

    template<typename Iterator,typename Skipper>
    info_request_grammar<Iterator,Skipper>::info_request_grammar() : info_request_grammar::base_type(start,"info_request_grammar") {

            start = (
                 lit("info")>lit('{')
                    > lit("\"request_id\"")   > ':' > quoted_string >
                 lit('}')
            )
            [ _val = phx::bind(mk_info_request,_1) ];
            start.name("info_request");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

        template struct info_request_grammar<request_iterator_t,request_skipper_t>;
}
