#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
 
    template<typename Iterator,typename Skipper>
    ts_values_grammar<Iterator,Skipper>::ts_values_grammar():ts_values_grammar::base_type(start,"ts_values") {
            start =
            '[' >
             -( (double_|nan_symbol) % ',') >
            ']'
            ;
            start.name("values");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

    template struct ts_values_grammar<request_iterator_t,request_skipper_t>;
       
}
