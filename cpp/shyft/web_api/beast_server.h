#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include "targetver.h"

//
// Copyright (c) 2016-2017 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//


//------------------------------------------------------------------------------
// NOTE:  This is all based on  Vinnie Falco boost.beast work,
// example: Advanced server, flex (plain + SSL)
// With minor adaptions and changes to allow for
// dispatch to long running background tasks.
//
//------------------------------------------------------------------------------
#include <boost/beast/version.hpp>

#if BOOST_BEAST_VERSION >= 248

//-- BOOST 1.70 and new BEAST simplifies a lot, breaking changes though.

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/strand.hpp>
#include <shyft/web_api/server_certificate.h>

#include <optional>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace shyft::web_api {
namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                    // from <boost/asio.hpp>
namespace ssl = boost::asio::ssl;               // from <boost/asio/ssl.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>

// Return a reasonable mime type based on the extension of a file.
extern beast::string_view mime_type( beast::string_view path );

// Append an HTTP rel-path to a local filesystem path.
// The returned path is normalized for the platform.
extern std::string path_cat( beast::string_view base,beast::string_view path );

// Report a failure
extern void fail( beast::error_code ec, char const* what );

// handle error, return
#define return_on_error(ec,diag) if((ec)) {fail((ec),(diag));return;}

    /** @brief bg_worker, bacground work
     *
     * This class have the role to enable dispatching messages to bacground_work class
     * that have it's own boost::asio::io_context.
     * This is to ensure that the front-end threads serving the web is not exhausted
     * doing long running operations.
     *
     * This class is used as parameter to the root of the session-handlers
     * so it is quite essential (but not complex) for the server-framework.
     *
     * Since we would like to use the same web-server framework for
     * different backend services, we keep the server-class implementation
     * as a template parameter.
     *
     * Typically the server class S, does stuff like this:
     *
     *  -# parse input (text/buffer from web)
     *  -# compute/do stuff based on parsed input parameters
     *  -# generate output/response(s) as multi_buffer/buffer
     *  -# then post/replie(s) back to front-end io-threads running the io-services.
     *
     *  @tparam S bacground server providing the do_the_work(msg)->buffer method
     */
    template <class S>
    struct bg_worker {
        S& server;///< not owning, lifetime exeeds this object
        net::io_context& ioc;///< post bg work to this ioc
    };
    
    /** This function produces an HTTP response for the given
     * request. The type of the response object depends on the
     * contents of the request, so the interface requires the
     * caller to pass a generic lambda for receiving the response.
     */
    template<
        class Body, class Allocator,
        class Send>
    void
    handle_request( beast::string_view doc_root,http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send ) {
        // Returns a bad request response
        auto const bad_request =
        [&req]( beast::string_view why ) {
            http::response<http::string_body> res {http::status::bad_request, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = std::string( why );
            res.prepare_payload();
            return res;
        };

        // Returns a not found response
        auto const not_found =
        [&req]( beast::string_view target ) {
            http::response<http::string_body> res {http::status::not_found, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = "The resource '" + std::string( target ) + "' was not found.";
            res.prepare_payload();
            return res;
        };

        // Returns a server error response
        auto const server_error =
        [&req]( beast::string_view what ) {
            http::response<http::string_body> res {http::status::internal_server_error, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = "An error occurred: '" + std::string( what ) + "'";
            res.prepare_payload();
            return res;
        };

        // Make sure we can handle the method
        if ( req.method() != http::verb::get &&
                req.method() != http::verb::head )
            return send( bad_request( "Unknown HTTP-method" ) );

        // Request path must be absolute and not contain "..".
        if ( req.target().empty() ||
                req.target() [0] != '/' ||
                req.target().find( ".." ) != beast::string_view::npos )
            return send( bad_request( "Illegal request-target" ) );

        // Build the path to the requested file
        std::string path = path_cat( doc_root, req.target() );
        if ( req.target().back() == '/' )
            path.append( "index.html" );

        // Attempt to open the file
        beast::error_code ec;
        http::file_body::value_type body;
        body.open( path.c_str(), beast::file_mode::scan, ec );

        // Handle the case where the file doesn't exist
        if ( ec == beast::errc::no_such_file_or_directory )
            return send( not_found( req.target() ) );

        // Handle an unknown error
        if ( ec )
            return send( server_error( ec.message() ) );

        // Cache the size since we need it after the move
        auto const size = body.size();

        // Respond to HEAD request
        if ( req.method() == http::verb::head ) {
            http::response<http::empty_body> res {http::status::ok, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, mime_type( path ) );
            res.content_length( size );
            res.keep_alive( req.keep_alive() );
            return send( std::move( res ) );
        }

        // Respond to GET request
        http::response<http::file_body> res {
            std::piecewise_construct,
            std::make_tuple( std::move( body ) ),
            std::make_tuple( http::status::ok, req.version() )
        };
        res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
        res.set( http::field::content_type, mime_type( path ) );
        res.content_length( size );
        res.keep_alive( req.keep_alive() );
        return send( std::move( res ) );
    }


    /** web socket session handler
    *
    * This class is common base-class , so that plain ws, and ssl ws is derived from
    * this using CRTP pattern. Somewhat involved in our case, - but the boiler plate
    * from beast was using this, so we just extend it somewhat to allow to
    * dispatch work to the bacground worker io-service, that does
    * the service-kind of work.
    *
    * @tparam Derived<bg_work> a nested template parameter (a parameter that is it self a template)
    * @tparam bg_work the background worker class that keeps the io_context and the server type.
    *
    */
    template<class Derived,class bg_work>
    class websocket_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        Derived& derived() { return static_cast<Derived&>( *this );}

        beast::flat_buffer buffer_;
        beast::flat_buffer out_buffer_;
        bg_work& bgw_;

        // Start the asynchronous operation
        template<class Body, class Allocator>
        void do_accept( http::request<Body, http::basic_fields<Allocator>> req ) {
            // Set suggested timeout settings for the websocket
            derived().ws().set_option(
                websocket::stream_base::timeout::suggested( beast::role_type::server )
            );

            derived().ws().set_option(  // Set a decorator to change the Server of the handshake
            websocket::stream_base::decorator( []( websocket::response_type& res ) {
                res.set( http::field::server,std::string( BOOST_BEAST_VERSION_STRING ) + " advanced-server-flex" );
            }
                                                 )
            );

            derived().ws().async_accept( req,  // Accept the websocket handshake
                                         beast::bind_front_handler(
                                             &websocket_session::on_accept,
                                             derived().shared_from_this()
                                         )
                                       );
        }

        void on_accept( beast::error_code ec ) {
            return_on_error( ec, "accept" )
            do_read(); // Read a message
        }

        void do_read() {
            // Read a message into our buffer
            derived().ws().async_read( buffer_,
                                       beast::bind_front_handler(
                                           &websocket_session::on_read,
                                           derived().shared_from_this()
                                       )
                                     );
        }

        void on_read( beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );
            if ( ec == websocket::error::closed ) // This indicates that the websocket_session was closed
                return;

            return_on_error( ec, "read" )
            // Echo the message
                        //---
            // here buffer_ is complete message
            // forward the text/message to the ws-handler
            auto req=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());// empty the buffer,
            
            //on_bg_work_ready(beast::error_code{},bgw_.server.do_the_work(std::move(req)));// working

            net::post(bgw_.ioc,[reqst=std::move(req),me=derived().shared_from_this()]{ // me=shared_from_this ensure the session stays around while doing back work.
                // need to post back to this ioc/executor, because several responses might be in the pipeline.
                net::post(me->derived().ws().get_executor(),[me,reqstx=std::move(reqst)]{// move into the inner context, really needed
                    me->on_bg_work_ready(beast::error_code {},me->bgw_.server.do_the_work(std::move(reqstx)));// finally, move on to the call
                });
            });
            do_read();// just continue reading more messages, the write will complete once bg worker is done
        }
        void on_bg_work_ready(beast::error_code ec, beast::flat_buffer b) {
            return_on_error(ec,"bg-work failed")
            out_buffer_=std::move(b); //moving into buffer_, ensure that buffer_ outlives async_write
            derived().ws().text(true);
            derived().ws().async_write( out_buffer_.data(),
               beast::bind_front_handler(
                   &websocket_session::on_write,
                    derived().shared_from_this()
                )
            );
        }

        /** Called each time bgw is done with the job */
        void on_write( beast::error_code ec,std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );
            return_on_error( ec, "write" )
            out_buffer_.consume( out_buffer_.size() );  // Clear the buffer
        }

    public:
        
        explicit websocket_session(bg_work& bgw):bgw_{bgw}{}
        // Start the asynchronous operation
        template<class Body, class Allocator>
        void run( http::request<Body, http::basic_fields<Allocator>> req ) {
            do_accept( std::move( req ) );  // Accept the WebSocket upgrade request
        }
    };


    /** @brief handles ssl web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, plain_websocket_session
    */
    template <class bg_work>
    struct plain_websocket_session
        : public websocket_session<plain_websocket_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<plain_websocket_session<bg_work>> {
        using base=websocket_session<plain_websocket_session<bg_work>,bg_work>;
        using stream_t =websocket::stream<beast::tcp_stream>;

        explicit // Create the session
        plain_websocket_session( beast::tcp_stream&& stream,bg_work& bgw)
            :base(bgw), ws_( std::move( stream ) ) {
        }

        // Called by the base class
        stream_t& ws() {return ws_;}
        private:
            stream_t ws_;
    };


    /** @brief handles ssl web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, plain_websocket_session
    */

    template <class bg_work>
    struct ssl_websocket_session
        : public websocket_session<ssl_websocket_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<ssl_websocket_session<bg_work>> {
    
        using base=websocket_session<ssl_websocket_session<bg_work>,bg_work>;
        using stream_t = websocket::stream<beast::ssl_stream<beast::tcp_stream>>;

        // Create the ssl_websocket_session
        explicit ssl_websocket_session( beast::ssl_stream<beast::tcp_stream>&& stream, bg_work& bgw )
            :base(bgw),ws_( std::move( stream ) ) {
        }

        // Called by the base class
        stream_t& ws() { return ws_;}
        private:
            stream_t ws_;
    };

    //------------------------------------------------------------------------------

    template<class Body, class Allocator,class bg_work>
    void make_websocket_session( beast::tcp_stream stream, http::request<Body, http::basic_fields<Allocator>> req, bg_work& bgw) {
        std::make_shared<plain_websocket_session<bg_work>> ( std::move( stream ),bgw )->run( std::move( req ) );
    }

    template<class Body, class Allocator,class bg_work>
    void make_websocket_session( beast::ssl_stream<beast::tcp_stream> stream, http::request<Body, http::basic_fields<Allocator>> req, bg_work& bgw ) {
        std::make_shared<ssl_websocket_session<bg_work>> ( std::move( stream ),bgw )->run( std::move( req ) );
    }

    /** @brief common base http-session handler  for both plain and ssl
    *
    * This class handles http-sessions, used as base-class for the plain and ssl based
    * web-handlers, so that we can keep common code in one place.
    * It's using CRTP, as the boost beast boiler-plate code did,
    * but extended with the bg_work class so that we *could* dispatch
    * time-consuming work to the background io_context and server.
    *
    * @tparam Derived<bg_work> A template parameter taking bg_work as parameter
    * @tparam bg_work the bacground worker containing io_context ioc and a server that deals with the requests
    *
    */
    template<class Derived, class bg_work>
    class http_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        Derived& derived() { return static_cast<Derived&>( *this );}

        // This queue is used for HTTP pipelining.
        class queue {
                enum { limit = 8 };// Maximum number of responses we will queue
                // The type-erased, saved work item
                struct work {
                    virtual ~work() = default;
                    virtual void operator()() = 0;
                };
                http_session& self_;
                std::vector<std::unique_ptr<work>> items_;

            public:
                explicit queue( http_session& self ): self_( self ) {
                    static_assert( limit > 0, "queue limit must be positive" );
                    items_.reserve( limit );
                }

                // Returns `true` if we have reached the queue limit
                bool is_full() const { return items_.size() >= limit; }

                // Called when a message finishes sending
                // Returns `true` if the caller should initiate a read
                bool on_write() {
                    BOOST_ASSERT( ! items_.empty() );
                    auto const was_full = is_full();
                    items_.erase( items_.begin() );
                    if ( ! items_.empty() )
                        ( *items_.front() )();
                    return was_full;
                }

                // Called by the HTTP handler, ref to template<..> handle_message(...Send&&) to send a response.
                template<bool isRequest, class Body, class Fields>
                void operator()( http::message<isRequest, Body, Fields>&& msg ) {
                    // This holds a work item
                    struct work_impl : work {
                        http_session& self_;
                        http::message<isRequest, Body, Fields> msg_;

                        work_impl( http_session& self, http::message<isRequest, Body, Fields>&& msg )
                            : self_( self )
                            , msg_( std::move( msg ) ) {
                        }

                        void operator()() {
                            http::async_write( self_.derived().stream(), msg_,
                                               beast::bind_front_handler(
                                                   &http_session::on_write,
                                                   self_.derived().shared_from_this(),
                                                   msg_.need_eof()
                                               )
                                             );
                        }
                    };

                    // Allocate and store the work
                    items_.push_back( std::make_unique<work_impl> ( self_, std::move( msg ) ) );
                    // If there was no previous work, start this one
                    if ( items_.size() == 1 )
                        ( *items_.front() )();
                }
        };

        std::shared_ptr<std::string const> doc_root_;
        queue queue_;

        // The parser is stored in an optional container so we can
        // construct it from scratch it at the beginning of each new message.
        std::optional<http::request_parser<http::string_body>> parser_;

    protected:
        beast::flat_buffer buffer_;
        bg_work& bgw_;
    public:
        // Construct the session
        http_session( beast::flat_buffer buffer, std::shared_ptr<std::string const> const& doc_root, bg_work&bgw)
            : doc_root_( doc_root )
            , queue_( *this )
            , buffer_( std::move( buffer ) )
            , bgw_{bgw}{
        }

        void do_read() {
            // Construct a new parser for each message
            parser_.emplace();

            // Apply a reasonable limit to the allowed size
            // of the body in bytes to prevent abuse.
            parser_->body_limit( 10000 );

            // Set the timeout.
            beast::get_lowest_layer( derived().stream() ).expires_after( std::chrono::seconds( 30 ) );

            // Read a request using the parser-oriented interface
            http::async_read( derived().stream(),buffer_, *parser_,
                              beast::bind_front_handler(
                                  &http_session::on_read,
                                  derived().shared_from_this()
                              )
                            );
        }

        void
        on_read( beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );

            // This means they closed the connection
            if ( ec == http::error::end_of_stream )
                return derived().do_eof();

            return_on_error( ec, "read" )

            // See if it is a WebSocket Upgrade
            if ( websocket::is_upgrade( parser_->get() ) ) {
                // Disable the timeout.
                // The websocket::stream uses its own timeout settings.
                beast::get_lowest_layer( derived().stream() ).expires_never();

                // Create a websocket session, transferring ownership
                // of both the socket and the HTTP request.
                return make_websocket_session( derived().release_stream(),parser_->release(),bgw_ );
            }

            // Send the response
            handle_request( *doc_root_, parser_->release(), queue_ );

            // If we aren't at the queue limit, try to pipeline another request
            if ( ! queue_.is_full() )
                do_read();
        }

        void
        on_write( bool close, beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );

            return_on_error( ec, "write" );

            if ( close ) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return derived().do_eof();
            }

            // Inform the queue that a write completed
            if ( queue_.on_write() ) {
                // Read another request
                do_read();
            }
        }
    };

    /** @brief plain http session handler
    *
    * This class uses the http_session as base class to implement
    * a plain http session. This class is only responsible for
    * keeping the tcp socket, timeout/termination. All real work
    * is done in the base class handler.
    *
    * @see http_session for detailed description
    * @tparam bg_work bacground worker type
    */
    template <class bg_work>
    class plain_http_session
        : public http_session<plain_http_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<plain_http_session<bg_work>> {
        beast::tcp_stream stream_;
    public:
        using base=http_session<plain_http_session<bg_work>,bg_work>;
        // Create the session
        plain_http_session( beast::tcp_stream&& stream, beast::flat_buffer&& buffer,std::shared_ptr<std::string const> const& doc_root, bg_work& bgw )
            : base ( std::move( buffer ),doc_root,bgw)
            , stream_( std::move( stream ) ) {
        }

        // Start the session
        void run() {this->do_read();}

        // Called by the base class
        beast::tcp_stream& stream() { return stream_; }

        // Called by the base class
        beast::tcp_stream release_stream() { return std::move( stream_ );}

        // Called by the base class
        void do_eof() {
            beast::error_code ec;
            stream_.socket().shutdown( tcp::socket::shutdown_send, ec );
            // At this point the connection is closed gracefully
        }
    };

    /** @brief ssl http (https) session handler
    *
    * This class uses the http_session as base class to implement
    * a  https session. This class is only responsible for
    * keeping the tcp socket, timeout/termination. All real work
    * is done in the base class handler.
    *
    * @see http_session for detailed description
    * @tparam bg_work bacground worker type
    */

    template<class bg_work>
    class ssl_http_session
        : public http_session<ssl_http_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<ssl_http_session<bg_work>> {
        beast::ssl_stream<beast::tcp_stream> stream_;

    public:
        using base=http_session<ssl_http_session<bg_work>,bg_work>;
        using base::buffer_,base::do_read;
        // Create the http_session
        ssl_http_session( beast::tcp_stream&& stream, ssl::context& ctx, beast::flat_buffer&& buffer, std::shared_ptr<std::string const> const& doc_root, bg_work& bgw )
            : base ( std::move( buffer ), doc_root,bgw )
            , stream_( std::move( stream ), ctx ) {
        }

        // Start the session
        void run() {
            beast::get_lowest_layer( stream_ ).expires_after( std::chrono::seconds( 30 ) );    // Set the timeout.
            // Perform the SSL handshake
            // Note, this is the buffered version of the handshake.
            stream_.async_handshake( ssl::stream_base::server,buffer_.data(),
                                     beast::bind_front_handler(
                                         &ssl_http_session::on_handshake,
                                         this->shared_from_this()
                                     )
                                   );
        }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream>& stream() { return stream_; }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream>  release_stream() { return std::move( stream_ );}

        // Called by the base class
        void do_eof() {
            beast::get_lowest_layer( stream_ ).expires_after( std::chrono::seconds( 30 ) );   // Set the timeout.
            stream_.async_shutdown(
                beast::bind_front_handler(
                    &ssl_http_session::on_shutdown, // Perform the SSL shutdown
                    this->shared_from_this()
                )
            );
        }

    private:
        void on_handshake( beast::error_code ec,std::size_t bytes_used ) {
            return_on_error( ec, "handshake" )
            // Consume the portion of the buffer used by the handshake
            buffer_.consume( bytes_used );
            do_read();
        }

        void on_shutdown( beast::error_code ec ) {
            return_on_error( ec, "shutdown" )
            // At this point the connection is closed gracefully
        }
    };

    /** @brief detect session, and forward it
    *
    * Detects SSL handshakes
    * and launches either a http-plain or https session object that
    * handle the request(s), doing the real work.
    * at that stage the socket_ ,ctx_, doc_root_ and buffer_
    * is handed over to that new session.
    * the strand_ is local, not forwarded, as it's seem to be
    * just a short-hand for socket_.get_executor(), ref. constructor.
    *
    * @tparam bg_work the background worker type containing io_context and server to do long running work
    */
    template<class bg_work>
    class detect_session : public std::enable_shared_from_this<detect_session<bg_work>> {
        beast::tcp_stream stream_;
        ssl::context& ctx_;
        std::shared_ptr<std::string const> doc_root_;
        beast::flat_buffer buffer_;
        bg_work& bgw_;
    public:
        explicit detect_session(
            tcp::socket&& socket,
            ssl::context& ctx,
            std::shared_ptr<std::string const> const& doc_root,
            bg_work& bgw
                               )
            : stream_( std::move( socket ) )
            , ctx_( ctx )
            , doc_root_( doc_root )
            , bgw_{bgw}{
        }

        // Launch the detector
        void run() {
            // Set the timeout.
            stream_.expires_after( std::chrono::seconds( 30 ) );

            beast::async_detect_ssl( stream_, buffer_,
                                     beast::bind_front_handler(
                                         &detect_session::on_detect,
                                         this->shared_from_this()
                                     )
                                   );
        }

        void
        on_detect( beast::error_code ec, boost::tribool result ) {
            return_on_error( ec, "detect" )

            if ( result ) { // Launch SSL session
                std::make_shared<ssl_http_session<bg_work>> ( std::move( stream_ ),ctx_,std::move( buffer_ ),doc_root_,bgw_ )->run();
            } else { // Launch plain session
                std::make_shared<plain_http_session<bg_work>> ( std::move( stream_ ),std::move( buffer_ ),doc_root_,bgw_ )->run();
            }
        }
    };

    /** @brief the listener
    *
    *  Accepts incoming connections and launches the sessions.
    *  -That is, what it *really* does, is that it launces the
    *  detect_session that takes the work determining http or https
    *  before forwarding all the stash (socket_, ctx_ doc_root_)
    *  to either http or https handlers.
    *
    *  Since  work/sessions is originated from there, it needs to keep the
    *  bg_work class instance that is common for all work, so that it can
    *  forward that to the detectect-session (that forward it to ws-session or http-session when done)
    *
    *
    * @tparam bg_work the background worker class to do long running work, needed to create forwarders
    */
    template <class bg_work>
    struct listener : public std::enable_shared_from_this<listener<bg_work>> {
        using base=std::enable_shared_from_this<listener<bg_work>>;
        listener( net::io_context& ioc, ssl::context& ctx, tcp::endpoint endpoint, std::shared_ptr<std::string const> const& doc_root, bg_work& bgw )
            : ioc_( ioc )
            , ctx_( ctx )
            , acceptor_( net::make_strand( ioc ) )
            , doc_root_( doc_root )
            , bgw_{bgw}{
            beast::error_code ec;
            acceptor_.open( endpoint.protocol(), ec ); return_on_error( ec,"open" )
            acceptor_.set_option( net::socket_base::reuse_address( true ), ec ); return_on_error( ec,"set_option" )
            acceptor_.bind( endpoint, ec ); return_on_error( ec,"bind" )
            acceptor_.listen( net::socket_base::max_listen_connections, ec ); return_on_error( ec,"listen" )
        }

        // Start accepting incoming connections
        void run() {
            do_accept();
        }

    private:
        using base::shared_from_this;
        net::io_context& ioc_;
        ssl::context& ctx_;
        tcp::acceptor acceptor_;
        std::shared_ptr<std::string const> doc_root_;
        bg_work& bgw_;

        void
        do_accept() {
            // The new connection gets its own strand
            acceptor_.async_accept( net::make_strand( ioc_ ),
                beast::bind_front_handler(
                    &listener::on_accept,
                    shared_from_this()
                )
            );
        }

        void
        on_accept( beast::error_code ec, tcp::socket socket ) {
            if ( ec ) {
                fail( ec, "accept" );
            } else {
                // Create the detector http_session and run it
                std::make_shared<detect_session<bg_work>> (
                    std::move( socket ),
                    ctx_,
                    doc_root_,
                    bgw_
                )->run();
            }
            do_accept(); // Accept another connection
        }
    };
    #undef return_on_error

    using std::string;
    using std::shared_ptr;
    using std::make_shared;
    using std::optional;
    
    /** @brief run_web_server
    *
    * Start the beast flex advanced web-server with bacground request-handler for long running ops,
    * with specified document root and port parameters.
    *
    * To stop the server, use std::raise(SIGTERM) (from another thread)
    *
    * When signals are catched, the web-server will try to do graceful close, join the worker threads, and return 0.
    *
    * @tparam request_handler the type that is required to keep the bg-server, and providing the do_the_work(req)->buffer method
    *
    * @param bg_server of type request_handler, reference, we do not take a copy, just forward the reference, so it's life time should outperform this call
    * @param address_s the listening ip-port number, so that you can be selective regarding scope
    * @param port the ip port number to listen on
    * @param doc_root the document root where you can stash the serverside plain mime-type docs.
    * @param threads number of io-threads to serve the front-end
    * @param bg_threads number of io-threads to serve for long running requests
    */
    template <class request_handler>
    int run_web_server(request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads, int bg_threads) {
        auto const address = boost::asio::ip::make_address(address_s);
        // shorthand for now, using web-api request handler.
        using bg_work=bg_worker<request_handler>;
        // The io_context is required for all I/O
        net::io_context ioc{threads};
        net::io_context bg_ioc{bg_threads};
        optional<net::io_context::work> bg_work_lock;
        bg_work_lock.emplace(std::ref(bg_ioc)); // keep alive the bg_work io-service until we zero out the work flag
        bg_work bgw{bg_server,bg_ioc};// bacground worker consist of a server and an ioc that we can post on

        // The SSL context is required, and holds certificates
        ssl::context ctx{ssl::context::sslv23};

        // This holds the self-signed certificate used by the server
        load_server_certificate(ctx);

        // Create and launch a listening port
        // it needs to carry all the stuff
        // that needs to passed on to
        // the down-stream classes to handle
        // incoming connections, including the background worker
        make_shared<listener<bg_work>>(
            ioc,
            ctx,
            tcp::endpoint{address, port},
            doc_root,
            bgw)->run(); // will hopefully post work to the ioc, that will get alive later when we start the run.

        // Capture SIGINT and SIGTERM to perform a clean shutdown
        boost::asio::signal_set signals(ioc, SIGINT, SIGTERM);
        signals.async_wait(
            [&](beast::error_code const&, int)
            {
                // Stop the `io_context`. This will cause `run()`
                // to return immediately, eventually destroying the
                // `io_context` and all of the sockets in it.
                ioc.stop();
                bgw.ioc.stop();//ok stopping this loop
            });

        // Run the I/O service on the requested number of threads
        std::vector<std::thread> v;
        v.reserve(threads - 1);
        for(auto i = threads - 1; i > 0; --i)
            v.emplace_back( [&ioc] { ioc.run(); });
        for(auto i = threads; i > 0; --i)
            v.emplace_back( [&bg_ioc] { bg_ioc.run(); });
        ioc.run();

        // (If we get here, it means we got a SIGINT or SIGTERM)
        bg_work_lock=std::nullopt;// remove the motivation for the bg.io_context to stay alive (but we did actually stop above..)
        // Block until all the threads exit
        for(auto& t : v)
            t.join();

        return 0;
    }

}

#else

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/experimental/core/ssl_stream.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/make_unique.hpp>
#include <boost/config.hpp>
#include <shyft/web_api/detect_ssl.h>
#include <shyft/web_api/server_certificate.h>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <optional>


namespace shyft::web_api {

    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace ssl = boost::asio::ssl;               // from <boost/asio/ssl.hpp>
    namespace http = boost::beast::http;            // from <boost/beast/http.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>

    using std::shared_ptr;
    using std::string;
    using std::optional;
    using std::make_shared;
    using std::enable_shared_from_this;

    using boost::system::error_code;
    using boost::beast::string_view;
    using boost::beast::flat_buffer;
    using boost::beast::multi_buffer;
    using boost::asio::io_context;
    using boost::asio::strand;
    using boost::asio::steady_timer;
    using boost::asio::bind_executor;

    /** @brief bg_worker, bacground work
     *
     * This class have the role to enable dispatching messages to bacground_work class
     * that have it's own boost::asio::io_context.
     * This is to ensure that the front-end threads serving the web is not exhausted
     * doing long running operations.
     *
     * This class is used as parameter to the root of the session-handlers
     * so it is quite essential (but not complex) for the server-framework.
     *
     * Since we would like to use the same web-server framework for
     * different backend services, we keep the server-class implementation
     * as a template parameter.
     *
     * Typically the server class S, does stuff like this:
     *
     *  -# parse input (text/buffer from web)
     *  -# compute/do stuff based on parsed input parameters
     *  -# generate output/response(s) as multi_buffer/buffer
     *  -# then post/replie(s) back to front-end io-threads running the io-services.
     *
     *  @tparam S bacground server providing the do_the_work(msg)->buffer method
     */
    template <class S>
    struct bg_worker {
        S& server;///< not owning, lifetime exeeds this object
        io_context& ioc;///< post bg work to this ioc
    };



    /**  Return a reasonable mime type based on the extension of a file.*/
    extern
    string_view
    mime_type(string_view path);

    /**
     *Append an HTTP rel-path to a local filesystem path.
     * The returned path is normalized for the platform.
     */
    extern
    string
    path_cat( string_view base,  string_view path) ;

    // This function produces an HTTP response for the given
    // request. The type of the response object depends on the
    // contents of the request, so the interface requires the
    // caller to pass a generic lambda for receiving the response.
    template<
        class Body, class Allocator,
        class Send>
    void
    handle_request(
        string_view doc_root,
        http::request<Body, http::basic_fields<Allocator>>&& req,
        Send&& send) {
        // Returns a bad request response
        auto const bad_request =
        [&req](string_view why) {
            http::response<http::string_body> res{http::status::bad_request, req.version()};
            res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
            res.set(http::field::content_type, "text/html");
            res.keep_alive(req.keep_alive());
            res.body() = why.to_string();
            res.prepare_payload();
            return res;
        };

        // Returns a not found response
        auto const not_found =
        [&req](string_view target) {
            http::response<http::string_body> res{http::status::not_found, req.version()};
            res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
            res.set(http::field::content_type, "text/html");
            res.keep_alive(req.keep_alive());
            res.body() = "The resource '" + target.to_string() + "' was not found.";
            res.prepare_payload();
            return res;
        };

        // Returns a server error response
        auto const server_error =
        [&req](string_view what) {
            http::response<http::string_body> res{http::status::internal_server_error, req.version()};
            res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
            res.set(http::field::content_type, "text/html");
            res.keep_alive(req.keep_alive());
            res.body() = "An error occurred: '" + what.to_string() + "'";
            res.prepare_payload();
            return res;
        };

        // Make sure we can handle the method
        if( req.method() != http::verb::get &&
            req.method() != http::verb::head)
            return send(bad_request("Unknown HTTP-method"));

        // Request path must be absolute and not contain "..".
        if( req.target().empty() ||
            req.target()[0] != '/' ||
            req.target().find("..") != string_view::npos)
            return send(bad_request("Illegal request-target"));

        // Build the path to the requested file
        string path = path_cat(doc_root, req.target());
        if(req.target().back() == '/')
            path.append("index.html");

        // Attempt to open the file
        error_code ec;
        http::file_body::value_type body;
        body.open(path.c_str(), boost::beast::file_mode::scan, ec);

        // Handle the case where the file doesn't exist
        if(ec == boost::system::errc::no_such_file_or_directory)
            return send(not_found(req.target()));

        // Handle an unknown error
        if(ec)
            return send(server_error(ec.message()));

        // Cache the size since we need it after the move
        auto const size = body.size();

        // Respond to HEAD request
        if(req.method() == http::verb::head) {
            http::response<http::empty_body> res{http::status::ok, req.version()};
            res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
            res.set(http::field::content_type, mime_type(path));
            res.content_length(size);
            res.keep_alive(req.keep_alive());
            return send(std::move(res));
        }

        // Respond to GET request
        http::response<http::file_body> res{
            std::piecewise_construct,
            std::make_tuple(std::move(body)),
            std::make_tuple(http::status::ok, req.version())};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, mime_type(path));
        res.content_length(size);
        res.keep_alive(req.keep_alive());
        return send(std::move(res));
    }

    //------------------------------------------------------------------------------

    // Report a failure
    extern void fail(error_code ec, char const* what);


    //------------------------------------------------------------------------------

    /** web socket session handler
     *
     * This class is common base-class , so that plain ws, and ssl ws is derived from
     * this using CRTP pattern. Somewhat involved in our case, - but the boiler plate
     * from beast was using this, so we just extend it somewhat to allow to
     * dispatch work to the bacground worker io-service, that does
     * the service-kind of work.
     *
     * @tparam Derived<bg_work> a nested template parameter (a parameter that is it self a template)
     * @tparam bg_work the background worker class that keeps the io_context and the server type.
     *
     */
    template<class Derived,class bg_work>
    class websocket_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        Derived&
        derived() {
            return static_cast<Derived&>(*this);
        }

        boost::beast::multi_buffer buffer_;
        char ping_state_ = 0;// 0 initial, 1 sent request , 2 success sending

    protected:
        strand<io_context::executor_type> strand_;
        steady_timer timer_;
        bg_work& bgw_; ///< long running background work is forwarded here
    public:
        // Construct the session
        explicit
        websocket_session(io_context& ioc,bg_work& bgw)
            : strand_(ioc.get_executor())
            , timer_(ioc,(std::chrono::steady_clock::time_point::max)())
            , bgw_{bgw}{
        }

        // Start the asynchronous operation
        template<class Body, class Allocator>
        void
        do_accept(http::request<Body, http::basic_fields<Allocator>> req) {
            // Set the control callback. This will be called
            // on every incoming ping, pong, and close frame.
            derived().ws().control_callback(
                std::bind(
                    &websocket_session::on_control_callback,
                    this,
                    std::placeholders::_1,
                    std::placeholders::_2));

            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));

            // Accept the websocket handshake
            derived().ws().async_accept(
                req,
                bind_executor(
                    strand_,
                    std::bind(
                        &websocket_session::on_accept,
                        derived().shared_from_this(),
                        std::placeholders::_1)));
        }

        void
        on_accept(error_code ec) {
            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "accept");

            do_read();// Read a message
        }

        // Called when the timer expires.
        void
        on_timer(error_code ec) {
            if(ec && ec != boost::asio::error::operation_aborted)
                return fail(ec, "timer");

            // See if the timer really expired since the deadline may have moved.
            if(timer_.expiry() <= std::chrono::steady_clock::now()) {
                // If this is the first time the timer expired,
                // send a ping to see if the other end is there.
                if(derived().ws().is_open() && ping_state_ == 0) {
                    ping_state_ = 1;// Note that we are sending a ping
                    timer_.expires_after(std::chrono::seconds(15));// Set the timer
                    derived().ws().async_ping({},// Now send the ping
                        bind_executor(
                            strand_,
                            std::bind(
                                &websocket_session::on_ping,
                                derived().shared_from_this(),
                                std::placeholders::_1)));
                } else {
                    // The timer expired while trying to handshake,
                    // or we sent a ping and it never completed or
                    // we never got back a control frame, so close.

                    derived().do_timeout();
                    return;
                }
            }

            // Wait on the timer
            timer_.async_wait(
                bind_executor(
                    strand_,
                    std::bind(
                        &websocket_session::on_timer,
                        derived().shared_from_this(),
                        std::placeholders::_1)));
        }

        // Called to indicate activity from the remote peer
        void
        activity() {
            ping_state_ = 0;// Note that the connection is alive
            timer_.expires_after(std::chrono::seconds(15));// Set the timer TODO: use parameter timeout
        }

        // Called after a ping is sent.
        void
        on_ping(error_code ec) {
            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "ping");

            // Note that the ping was sent.
            if(ping_state_ == 1) {
                ping_state_ = 2;
            } else {
                // ping_state_ could have been set to 0
                // if an incoming control frame was received
                // at exactly the same time we sent a ping.
                BOOST_ASSERT(ping_state_ == 0);
            }
        }

        void
        on_control_callback(
            websocket::frame_type kind,
            string_view payload) {
            boost::ignore_unused(kind, payload);
            // Note that there is activity
            activity();
        }

        void
        do_read() {
            // Read a message into our buffer
            derived().ws().async_read(
                buffer_,
                bind_executor(
                    strand_,
                    std::bind(
                        &websocket_session::on_read,
                        derived().shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2)));
        }

        void
        on_read(
            error_code ec,
            std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);

            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            // This indicates that the websocket_session was closed
            if(ec == websocket::error::closed)
                return;

            if(ec)
                fail(ec, "read");

            // Note that there is activity
            activity();
            //---
            // here buffer_ is complete message
            // forward the text/message to the ws-handler
            auto req=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());// empty the buffer,

            bgw_.ioc.post([req,me=derived().shared_from_this()]{ // me=shared_from_this ensure the session stays around while doing back work.
                me->on_message_handled(error_code{},me->bgw_.server.do_the_work(req));// should result in a move..
            });
            //do_read();// continue read and do stuff while working in the background
        }

        /** respond back with message to client
         *
         * in nice weather, the ws is still working, >  no prob.
         * if it got closed in between, - then we end up here
         * trying to do async_write on a socket that is closed
         *
         */
        void on_message_handled(error_code ec,boost::beast::multi_buffer b) { //b arg is moved..
            if(! strand_.running_in_this_thread()) {
                return boost::asio::post(
                    bind_executor(
                        strand_,
                        std::bind(
                            &websocket_session::on_message_handled,
                            derived().shared_from_this(),
                            ec,
                            std::move(b)
                        )
                    )
                );
            }
            buffer_=std::move(b); //moving into buffer_, ensure that buffer_ outlives async_write..
            derived().ws().text(true);
            derived().ws().async_write(
                buffer_.data(),
                bind_executor(
                    strand_,
                    std::bind(
                        &websocket_session::on_write_response,
                        derived().shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2)));
        }
        void
        on_write_response(
            error_code ec,
            std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "write");
            buffer_.consume(buffer_.size());// Clear the buffer

            // we are done here
            do_read();// when done on bg. then do the next read..
        }
        void
        on_write(
            error_code ec,
            std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);

            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "write");

            buffer_.consume(buffer_.size());// Clear the buffer
            do_read();// Do another read
        }
    };

    /** @brief handles plain web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, ssl_websocket_session
    */
    template <class bg_work>
    class plain_websocket_session
        : public websocket_session<plain_websocket_session<bg_work>,bg_work>
        , public enable_shared_from_this<plain_websocket_session<bg_work>> {

        websocket::stream<tcp::socket> ws_;
        bool close_ = false;
    public:
        // penalty for using CRTP with multiple template args, need to reenable or quality
        // we try to reenable with using
        using base=websocket_session<plain_websocket_session<bg_work>,bg_work>;
        using base_shared=enable_shared_from_this<plain_websocket_session<bg_work>>;
        using base_shared::shared_from_this;
        using base::timer_,base::strand_;
        using base::on_timer,base::do_accept;

        // Create the session
        explicit
        plain_websocket_session(tcp::socket socket, bg_work& bgw)
            : base(socket.get_executor().context(),bgw)
            , ws_(std::move(socket)) {
        }

        // Called by the base class
        websocket::stream<tcp::socket>& ws() {return ws_;}

        /** Start the asynchronous operation
        */
        template<class Body, class Allocator>
        void
        run(http::request<Body, http::basic_fields<Allocator>> req) {
            // Run the timer. The timer is operated
            // continuously, this simplifies the code.
            on_timer({});
            do_accept(std::move(req));// Accept the WebSocket upgrade request
        }

        void
        do_timeout() {
            // This is so the close can have a timeout
            if(close_)
                return;
            close_ = true;

            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));

            // Close the WebSocket Connection
            ws_.async_close(
                websocket::close_code::normal,
                bind_executor(
                    strand_,
                    std::bind(
                        &plain_websocket_session::on_close,
                        shared_from_this(),
                        std::placeholders::_1)));
        }

        void
        on_close(error_code ec) {
            // Happens when close times out
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "close");

            // At this point the connection is gracefully closed
        }
    };

    /** @brief handles ssl web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, plain_websocket_session
    */
    template <class bg_work>
    class ssl_websocket_session
        : public websocket_session<ssl_websocket_session<bg_work>,bg_work>
        , public enable_shared_from_this<ssl_websocket_session<bg_work>> {
        websocket::stream<boost::beast::ssl_stream<tcp::socket>> ws_;
        bool eof_ = false;

    public:
        using base=websocket_session<ssl_websocket_session<bg_work>,bg_work>;
        using base_shared=enable_shared_from_this<ssl_websocket_session<bg_work>>;
        using base_shared::shared_from_this;
        using base::timer_,base::strand_;
        using base::on_timer,base::do_accept;

        // Create the http_session
        explicit
        ssl_websocket_session(boost::beast::ssl_stream<tcp::socket> stream,bg_work&bgw)
            : base(stream.get_executor().context(),bgw)
            , ws_(std::move(stream)) {
        }

        // Called by the base class
        websocket::stream<boost::beast::ssl_stream<tcp::socket>>&ws() { return ws_;}

        // Start the asynchronous operation
        template<class Body, class Allocator>
        void
        run(http::request<Body, http::basic_fields<Allocator>> req) {
            // Run the timer. The timer is operated
            // continuously, this simplifies the code.
            on_timer({});
            do_accept(std::move(req));// Accept the WebSocket upgrade request
        }

        void
        do_eof() {
            eof_ = true;
            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));
            // Perform the SSL shutdown
            ws_.next_layer().async_shutdown(
                bind_executor(
                    strand_,
                    std::bind(
                        &ssl_websocket_session::on_shutdown,
                        shared_from_this(),
                        std::placeholders::_1)));
        }

        void
        on_shutdown(error_code ec) {
            // Happens when the shutdown times out
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "shutdown");

            // At this point the connection is closed gracefully
        }

        void
        do_timeout() {
            // If this is true it means we timed out performing the shutdown
            if(eof_)
                return;

            // Start the timer again
            timer_.expires_at(
                (std::chrono::steady_clock::time_point::max)());
            on_timer({});
            do_eof();
        }
    };

    template<class Body, class Allocator,class bg_work>
    void
    make_websocket_session(
        tcp::socket socket,
        http::request<Body, http::basic_fields<Allocator>> req,
        bg_work& bgw
        ) {
        make_shared<plain_websocket_session<bg_work>>(std::move(socket),bgw)->run(std::move(req));
    }

    template<class Body, class Allocator, class bg_work>
    void
    make_websocket_session(
        boost::beast::ssl_stream<tcp::socket> stream,
        http::request<Body, http::basic_fields<Allocator>> req,
        bg_work& bgw
        ) {
        make_shared<ssl_websocket_session<bg_work>>(std::move(stream),bgw)->run(std::move(req));
    }

    //------------------------------------------------------------------------------

    /** @brief common base http-session handler  for both plain and ssl
     *
     * This class handles http-sessions, used as base-class for the plain and ssl based
     * web-handlers, so that we can keep common code in one place.
     * It's using CRTP, as the boost beast boiler-plate code did,
     * but extended with the bg_work class so that we *could* dispatch
     * time-consuming work to the background io_context and server.
     *
     * @tparam Derived<bg_work> A template parameter taking bg_work as parameter
     * @tparam bg_work the bacground worker containing io_context ioc and a server that deals with the requests
     *
     */
    template<class Derived,class bg_work>
    class http_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        using derived_bg_t =  Derived;
        derived_bg_t& derived() { return static_cast<derived_bg_t&>(*this);}

        /** This queue is used for HTTP pipelining.*/
        class queue {
            enum {
                limit = 8// Maximum number of responses we will queue
            };

            /** The type-erased, saved work item */
            struct work {
                virtual ~work() = default;
                virtual void operator()() = 0;
            };

            http_session& self_;
            std::vector<std::unique_ptr<work>> items_;

        public:
            explicit
            queue(http_session& self)
                : self_(self) {
                static_assert(limit > 0, "queue limit must be positive");
                items_.reserve(limit);
            }

            // Returns `true` if we have reached the queue limit
            bool
            is_full() const {
                return items_.size() >= limit;
            }

            // Called when a message finishes sending
            // Returns `true` if the caller should initiate a read

            bool on_write() {
                BOOST_ASSERT(! items_.empty());
                auto const was_full = is_full();
                items_.erase(items_.begin());
                if(! items_.empty())
                    (*items_.front())();
                return was_full;
            }

            // Called by the HTTP handler to send a response.
            template<bool isRequest, class Body, class Fields>
            void
            operator()(http::message<isRequest, Body, Fields>&& msg) {

                // This holds a work item
                struct work_impl : work {
                    http_session& self_;
                    http::message<isRequest, Body, Fields> msg_;

                    work_impl(
                        http_session& self,
                        http::message<isRequest, Body, Fields>&& msg)
                        : self_(self)
                        , msg_(std::move(msg)){
                    }

                    void
                    operator()() {
                        http::async_write(
                            self_.derived().stream(),
                            msg_,
                            bind_executor(
                                self_.strand_,
                                std::bind(
                                    &http_session::on_write,
                                    self_.derived().shared_from_this(),
                                    std::placeholders::_1,
                                    msg_.need_eof())));
                    }
                };

                // Allocate and store the work
                items_.push_back(boost::make_unique<work_impl>(self_, std::move(msg)));

                // If there was no previous work, start this one
                if(items_.size() == 1)
                    (*items_.front())();
            }
        };

        shared_ptr<string const> doc_root_;
        http::request<http::string_body> req_;
        queue queue_;

    protected:
        steady_timer timer_;
        strand<io_context::executor_type> strand_;
        flat_buffer buffer_;
        bg_work& bgw_;

    public:
        // Construct the session
        http_session(
            io_context& ioc,
            flat_buffer buffer,
            shared_ptr<string const> const& doc_root,
            bg_work& bgw )
            : doc_root_(doc_root)
            , queue_(*this)
            , timer_(ioc,std::chrono::steady_clock::time_point::max())
            , strand_(ioc.get_executor())
            , buffer_(std::move(buffer))
            , bgw_{bgw} {
        }

        void
        do_read() {
            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));

            // Make the request empty before reading,
            // otherwise the operation behavior is undefined.
            req_ = {};

            // Read a request
            http::async_read(
                derived().stream(),
                buffer_,
                req_,
                bind_executor(
                    strand_,
                    std::bind(
                        &http_session::on_read,
                        derived().shared_from_this(),
                        std::placeholders::_1)));
        }

        // Called when the timer expires.
        void
        on_timer(error_code ec) {
            if(ec && ec != boost::asio::error::operation_aborted)
                return fail(ec, "timer");

            // Check if this has been upgraded to Websocket
            if(timer_.expires_at() == (std::chrono::steady_clock::time_point::min)())
                return;

            // Verify that the timer really expired since the deadline may have moved.
            if(timer_.expiry() <= std::chrono::steady_clock::now())
                return derived().do_timeout();

            // Wait on the timer
            timer_.async_wait(
                bind_executor(
                    strand_,
                    std::bind(
                        &http_session::on_timer,
                        derived().shared_from_this(),
                        std::placeholders::_1)));
        }

        void
        on_read(error_code ec) {
            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            // This means they closed the connection
            if(ec == http::error::end_of_stream)
                return derived().do_eof();

            if(ec)
                return fail(ec, "read");

            // See if it is a WebSocket Upgrade
            if(websocket::is_upgrade(req_)) {
                // Make timer expire immediately, by setting expiry to time_point::min we can detect
                // the upgrade to websocket in the timer handler
                timer_.expires_at((std::chrono::steady_clock::time_point::min)());

                // Transfer the stream to a new WebSocket session
                return make_websocket_session(derived().release_stream(),std::move(req_),bgw_);
            }

            // Send the response
            handle_request(*doc_root_, std::move(req_), queue_);

            // If we aren't at the queue limit, try to pipeline another request
            if(! queue_.is_full())
                do_read();
        }

        void
        on_write(error_code ec, bool close) {
            // Happens when the timer closes the socket
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "write");

            if(close) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return derived().do_eof();
            }

            // Inform the queue that a write completed
            if(queue_.on_write()) {
                // Read another request
                do_read();
            }
        }
    };

    /** @brief plain http session handler
     *
     * This class uses the http_session as base class to implement
     * a plain http session. This class is only responsible for
     * keeping the tcp socket, timeout/termination. All real work
     * is done in the base class handler.
     *
     * @see http_session for detailed description
     * @tparam bg_work bacground worker type
     */
    template <class bg_work>
    class plain_http_session
        : public http_session<plain_http_session<bg_work>,bg_work>
        , public enable_shared_from_this<plain_http_session<bg_work>> {
        tcp::socket socket_;
        strand< io_context::executor_type> strand_;

    public:
        // CRTP needs reenabled base class access in our case
        using base=http_session<plain_http_session,bg_work>;
        using base_shared=enable_shared_from_this<plain_http_session<bg_work>>;
        using base_shared::shared_from_this;
        using base::on_timer,base::do_read,base::timer_,base::buffer_;

        // Create the http_session
        plain_http_session(
            tcp::socket socket,
            flat_buffer buffer,
            shared_ptr<string const> const& doc_root,
            bg_work& bgw)
            : base(
              socket.get_executor().context()
            , std::move(buffer)
            , doc_root,bgw)
            , socket_(std::move(socket))
            , strand_(socket_.get_executor()){
        }

        // Called by the base class
        tcp::socket& stream() { return socket_;}

        // Called by the base class, it's a take-over!
        tcp::socket release_stream() { return std::move(socket_);}

        // Start the asynchronous operation
        void
        run() {
            // Make sure we run on the strand
            if(! strand_.running_in_this_thread())
                return boost::asio::post(
                    bind_executor(
                        strand_,
                        std::bind(
                            &plain_http_session::run,
                            shared_from_this())));

            // Run the timer. The timer is operated
            // continuously, this simplifies the code.
            on_timer({});

            do_read();
        }

        void
        do_eof() {
            // Send a TCP shutdown
            error_code ec;
            socket_.shutdown(tcp::socket::shutdown_send, ec);

            // At this point the connection is closed gracefully
        }

        void
        do_timeout() {
            // Closing the socket cancels all outstanding operations. They
            // will complete with boost::asio::error::operation_aborted
            error_code ec;
            socket_.shutdown(tcp::socket::shutdown_both, ec);
            socket_.close(ec);
        }
    };

    /** @brief ssl http (https) session handler
     *
     * This class uses the http_session as base class to implement
     * a  https session. This class is only responsible for
     * keeping the tcp socket, timeout/termination. All real work
     * is done in the base class handler.
     *
     * @see http_session for detailed description
     * @tparam bg_work bacground worker type
     */
    template <class bg_work>
    class ssl_http_session
        : public http_session<ssl_http_session<bg_work>,bg_work>
        , public enable_shared_from_this<ssl_http_session<bg_work>> {
        boost::beast::ssl_stream<tcp::socket> stream_;
        strand< io_context::executor_type> strand_;
        bool eof_ = false;

    public:
        // CRTP needs reenabled base class access in our case
        using base=http_session<ssl_http_session<bg_work>,bg_work>;
        using base_shared=enable_shared_from_this<ssl_http_session<bg_work>>;
        using base_shared::shared_from_this;
        using base::on_timer,base::do_read,base::timer_,base::buffer_;

        // Create the http_session
        ssl_http_session(
            tcp::socket socket,
            ssl::context& ctx,
            flat_buffer buffer,
            shared_ptr<string const> const& doc_root,
            bg_work& bgw
            )
            : base(
                socket.get_executor().context(),
                std::move(buffer),
                doc_root,
                bgw)
            , stream_(std::move(socket), ctx)
            , strand_(stream_.get_executor()){
        }

        // Called by the base class
        boost::beast::ssl_stream<tcp::socket>& stream(){ return stream_;}

        // Called by the base class
        boost::beast::ssl_stream<tcp::socket> release_stream() {return std::move(stream_);}

        // Start the asynchronous operation
        void
        run() {
            // Make sure we run on the strand
            if(! strand_.running_in_this_thread())
                return boost::asio::post(
                    bind_executor(
                        strand_,
                        std::bind(
                            &ssl_http_session::run,
                            shared_from_this())));

            // Run the timer. The timer is operated
            // continuously, this simplifies the code.
            on_timer({});

            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));

            // Perform the SSL handshake
            // Note, this is the buffered version of the handshake.
            stream_.async_handshake(
                ssl::stream_base::server,
                buffer_.data(),
                bind_executor(
                    strand_,
                    std::bind(
                        &ssl_http_session::on_handshake,
                        shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2)));
        }
        void
        on_handshake(
            error_code ec,
            std::size_t bytes_used) {
            // Happens when the handshake times out
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "handshake");

            // Consume the portion of the buffer used by the handshake
            buffer_.consume(bytes_used);
            do_read();
        }

        void
        do_eof() {
            eof_ = true;

            // Set the timer
            timer_.expires_after(std::chrono::seconds(15));

            // Perform the SSL shutdown
            stream_.async_shutdown(
                bind_executor(
                    strand_,
                    std::bind(
                        &ssl_http_session::on_shutdown,
                        shared_from_this(),
                        std::placeholders::_1)));
        }

        void
        on_shutdown(error_code ec) {
            // Happens when the shutdown times out
            if(ec == boost::asio::error::operation_aborted)
                return;

            if(ec)
                return fail(ec, "shutdown");

            // At this point the connection is closed gracefully
        }

        void
        do_timeout() {
            // If this is true it means we timed out performing the shutdown
            if(eof_)
                return;

            // Start the timer again
            timer_.expires_at(std::chrono::steady_clock::time_point::max());
            on_timer({});
            do_eof();
        }
    };

    //------------------------------------------------------------------------------

    /** @brief detect session, and forward it
     *
     * Detects SSL handshakes
     * and launches either a http-plain or https session object that
     * handle the request(s), doing the real work.
     * at that stage the socket_ ,ctx_, doc_root_ and buffer_
     * is handed over to that new session.
     * the strand_ is local, not forwarded, as it's seem to be
     * just a short-hand for socket_.get_executor(), ref. constructor.
     *
     * @tparam bg_work the background worker type containing io_context and server to do long running work
     */
    template<class bg_work>
    class detect_session : public enable_shared_from_this<detect_session<bg_work>> {
        tcp::socket socket_;
        ssl::context& ctx_;
        strand<io_context::executor_type> strand_;
        shared_ptr<string const> doc_root_;
        flat_buffer buffer_;
        bg_work& bgw_;

    public:
        using base_shared=enable_shared_from_this<detect_session<bg_work>>;
        using base_shared::shared_from_this;

        explicit
        detect_session(
            tcp::socket socket,
            ssl::context& ctx,
            shared_ptr<string const> const& doc_root,
            bg_work& bgw
            )
            : socket_(std::move(socket))
            , ctx_(ctx)
            , strand_(socket_.get_executor())
            , doc_root_(doc_root)
            , bgw_{bgw} {
        }

        // Launch the detector, it's defined in the detect_ssl.h file
        void
        run() {
            async_detect_ssl(
                socket_,
                buffer_,
                bind_executor(
                    strand_,
                    std::bind(
                        &detect_session::on_detect,
                        shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2)));
        }

        /// We reach here if the async_detect_ssl is done, one way or the other
        void
        on_detect(error_code ec, boost::tribool result) {
            if(ec)
                return fail(ec, "detect");

            if(result) {
                // Launch SSL session
                make_shared<ssl_http_session<bg_work>>(
                    std::move(socket_),
                    ctx_,
                    std::move(buffer_),
                    doc_root_,
                    bgw_)->run();
                return;
            }

            // Launch plain session
            make_shared<plain_http_session<bg_work>>(
                std::move(socket_),
                std::move(buffer_),
                doc_root_,
                bgw_)->run();
        }
    };

    /** @brief the listener
     *
     *  Accepts incoming connections and launches the sessions.
     *  -That is, what it *really* does, is that it launces the
     *  detect_session that takes the work determining http or https
     *  before forwarding all the stash (socket_, ctx_ doc_root_)
     *  to either http or https handlers.
     *
     *  Since  work/sessions is originated from there, it needs to keep the
     *  bg_work class instance that is common for all work, so that it can
     *  forward that to the detectect-session (that forward it to ws-session or http-session when done)
     *
     *
     * @tparam bg_work the background worker class to do long running work, needed to create forwarders
     */
    template<class bg_work>
    class listener : public enable_shared_from_this<listener<bg_work>> {
        ssl::context& ctx_;
        tcp::acceptor acceptor_;
        tcp::socket socket_;
        shared_ptr<string const> doc_root_;
        bg_work &bgw_;
        using base_shared=enable_shared_from_this<listener<bg_work>>;
        using base_shared::shared_from_this;
    public:
        listener(
            io_context& ioc,
            ssl::context& ctx,
            tcp::endpoint endpoint,
            shared_ptr<string const> const& doc_root,
            bg_work&bgw)
            : ctx_(ctx)
            , acceptor_(ioc)
            , socket_(ioc)
            , doc_root_(doc_root)
            , bgw_{bgw} {
            error_code ec;
            // Open the acceptor
            acceptor_.open(endpoint.protocol(), ec);
            if(ec) {
                fail(ec, "open");
                return;
            }

            // Allow address reuse
            acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);
            if(ec) {
                fail(ec, "set_option");
                return;
            }

            // Bind to the server address
            acceptor_.bind(endpoint, ec);
            if(ec) {
                fail(ec, "bind");
                return;
            }

            // Start listening for connections
            acceptor_.listen( boost::asio::socket_base::max_listen_connections, ec);
            if(ec) {
                fail(ec, "listen");
                return;
            }
        }

        // Start accepting incoming connections
        void
        run() {
            if(! acceptor_.is_open())
                return;
            do_accept();
        }

        void
        do_accept() {
            acceptor_.async_accept(
                socket_,
                [me=shared_from_this()](error_code ec) {
                    me->on_accept(ec);
                }
            );
        }

        void
        on_accept(error_code ec) {
            if(ec){
                fail(ec, "accept");
            } else {
                // Create the detector http_session and run it
                make_shared<detect_session<bg_work>>(
                    std::move(socket_),
                    ctx_,
                    doc_root_,
                    bgw_)->run();
            }

            // Accept another connection
            do_accept();
        }
    };

    /** @brief run_web_server
     *
     * Start the beast flex advanced web-server with bacground request-handler for long running ops,
     * with specified document root and port parameters.
     *
     * To stop the server, use std::raise(SIGTERM) (from another thread)
     *
     * When signals are catched, the web-server will try to do graceful close, join the worker threads, and return 0.
     *
     * @tparam request_handler the type that is required to keep the bg-server, and providing the do_the_work(req)->buffer method
     *
     * @param bg_server of type request_handler, reference, we do not take a copy, just forward the reference, so it's life time should outperform this call
     * @param address_s the listening ip-port number, so that you can be selective regarding scope
     * @param port the ip port number to listen on
     * @param doc_root the document root where you can stash the serverside plain mime-type docs.
     * @param threads number of io-threads to serve the front-end
     * @param bg_threads number of io-threads to serve for long running requests
     */
    template <class request_handler>
    int run_web_server(request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads, int bg_threads) {
        auto const address = boost::asio::ip::make_address(address_s);
        // shorthand for now, using web-api request handler.
        using bg_work=bg_worker<request_handler>;
        // The io_context is required for all I/O
        io_context ioc{threads};
        io_context bg_ioc{bg_threads};
        optional<io_context::work> bg_work_lock;
        bg_work_lock.emplace(std::ref(bg_ioc)); // keep alive the bg_work io-service until we zero out the work flag
        bg_work bgw{bg_server,bg_ioc};// bacground worker consist of a server and an ioc that we can post on

        // The SSL context is required, and holds certificates
        ssl::context ctx{ssl::context::sslv23};

        // This holds the self-signed certificate used by the server
        load_server_certificate(ctx);

        // Create and launch a listening port
        // it needs to carry all the stuff
        // that needs to passed on to
        // the down-stream classes to handle
        // incoming connections, including the background worker
        make_shared<listener<bg_work>>(
            ioc,
            ctx,
            tcp::endpoint{address, port},
            doc_root,
            bgw)->run(); // will hopefully post work to the ioc, that will get alive later when we start the run.

        // Capture SIGINT and SIGTERM to perform a clean shutdown
        boost::asio::signal_set signals(ioc, SIGINT, SIGTERM);
        signals.async_wait(
            [&](error_code const&, int)
            {
                // Stop the `io_context`. This will cause `run()`
                // to return immediately, eventually destroying the
                // `io_context` and all of the sockets in it.
                ioc.stop();
                bgw.ioc.stop();//ok stopping this loop
            });

        // Run the I/O service on the requested number of threads
        std::vector<std::thread> v;
        v.reserve(threads - 1);
        for(auto i = threads - 1; i > 0; --i)
            v.emplace_back( [&ioc] { ioc.run(); });
        for(auto i = threads; i > 0; --i)
            v.emplace_back( [&bg_ioc] { bg_ioc.run(); });
        ioc.run();

        // (If we get here, it means we got a SIGINT or SIGTERM)
        bg_work_lock=std::nullopt;// remove the motivation for the bg.io_context to stay alive (but we did actually stop above..)
        // Block until all the threads exit
        for(auto& t : v)
            t.join();

        return 0;
    }


}
#endif
