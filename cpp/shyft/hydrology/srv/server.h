#pragma once


/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <regex>
#include <fstream>
#include <mutex>

#include <dlib/server.h>
#include <dlib/iosockstream.h>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/variant.hpp> // boost::variant supports serialization
#include <boost/serialization/variant.hpp>
#include <shyft/hydrology/srv/msg_defs.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/fs_compat.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/hydrology/geo_cell_data.h>
#include <shyft/hydrology/region_model.h>
// include model-stacks supported here

#include <shyft/hydrology/stacks/pt_gs_k.h>  
#include <shyft/hydrology/stacks/pt_ss_k.h>
#include <shyft/hydrology/stacks/pt_hs_k.h>
#include <shyft/hydrology/stacks/pt_hps_k.h>
#include <shyft/hydrology/stacks/r_pm_gs_k.h>

#include <shyft/hydrology/api/api.h>  // looking for region environment
#include <shyft/hydrology/api/api_state.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::hydrology::srv {

    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::string;
    using std::to_string;
    using std::ifstream;
    using std::ofstream;
    using std::runtime_error;
    using std::mutex;
    using std::unique_lock;
    using shyft::core::utctime;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    using shyft::core::region_model;
    using namespace shyft::core;
    using shyft::api::a_region_environment;
    
    using model_variant_t=boost::variant<
        shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>>,
        
        shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>>
        // hbv-stack not supported due to quality issues.
        
        >;
    using shyft::api::cell_state_with_id;
    
    /** wrap state into variant that support serialization and python exposure automagically */
    using state_variant_t=boost::variant<
        shared_ptr<vector<cell_state_with_id<pt_gs_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_ss_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_hs_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_hps_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state_t>>>
    >;

    /** wrap state into variant that support serialization and python exposure automagically */
    using parameter_variant_t=boost::variant<
        shared_ptr<pt_gs_k::parameter>,
        shared_ptr<pt_ss_k::parameter>,
        shared_ptr<pt_hs_k::parameter>,
        shared_ptr<pt_hps_k::parameter>,
        shared_ptr<r_pm_gs_k::parameter>
    >;
    
    /** enumerate the supported model types above (also exposed as constants to python)
     * The _opt enum should map to the discharge/snow only models used for speed/resource optimal templates.
     */
    enum struct rmodel_type:int8_t {
        pt_gs_k,pt_gs_k_opt,
        pt_ss_k,pt_ss_k_opt,
        pt_hs_k,pt_hs_k_opt,
        pt_hps_k,pt_hps_k_opt,
        r_pm_gs_k,r_pm_gs_k_opt
    };
    
    namespace detail {
        
        /** helper class to multiplex in cellstate to a model, using boost::apply_visitor  */
        struct set_state_visitor:boost::static_visitor<bool> {
            state_variant_t const& s;
            
            explicit set_state_visitor(state_variant_t const&s):s(s){}
            
            /** helper to do the apply state based on types.*/
            template <class S, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
            bool apply(M const& m) const {
                if(s.type()== typeid(shared_ptr<vector<cell_state_with_id<S>>>)) {
                    shyft::api::cids_t cids;
                    shyft::api::state_io_handler<typename M::element_type::cell_t> cs_handler(m->get_cells());
                    cs_handler.apply_state(boost::get< shared_ptr<vector<cell_state_with_id<S>>> >(s),cids);
                    return true;//TODO: possible to check results?
                }
                throw runtime_error("Illegal:missmatch between state type and model type. E.g.: pt_gs_k state must match pt_gs_k models");
                
            }
            
            //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
            
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::state_t>(m);}
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::state_t>(m);}
            
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::state_t>(m);}
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::state_t>(m);}

            bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::state_t>(m);}
            bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::state_t>(m);}

            bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::state_t>(m);}
            bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::state_t>(m);}

            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::state_t>(m);}
            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::state_t>(m);}
        };

        /** helper class to multiplex in parameter to a model, using boost::apply_visitor TODO: consider binary visitor instead! */
        struct set_region_model_parameter_visitor:boost::static_visitor<bool> {
            parameter_variant_t const& p;
            
            explicit set_region_model_parameter_visitor(parameter_variant_t const&p):p(p){}
            
            /** helper to do the apply state based on types.*/
            template <class P, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
            bool apply(M const& m) const {
                if(p.type()== typeid(shared_ptr<P>)) {
                    m->set_region_parameter(*boost::get< shared_ptr<P> >(p));
                    return true;//TODO: possible to check results?
                }
                throw runtime_error("Illegal:missmatch between region parameter type and model type. E.g.: pt_gs_k parameter must match pt_gs_k models");
            }
            
            //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
            
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
            
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
        };

        /** helper class to multiplex in parameter to a model, using boost::apply_visitor TODO: consider binary visitor instead! */
        struct set_catchment_model_parameter_visitor:boost::static_visitor<bool> {
            parameter_variant_t const& p;
            size_t cid;
            set_catchment_model_parameter_visitor(parameter_variant_t const&p,size_t cid):p{p},cid{cid}{}
            
            /** helper to do the apply state based on types.*/
            template <class P, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
            bool apply(M const& m) const {
                if(p.type()== typeid(shared_ptr<P>)) {
                    m->set_region_parameter(*boost::get< shared_ptr<P> >(p));
                    return true;//TODO: possible to check results?
                }
                throw runtime_error("Illegal:missmatch between region parameter type and model type. E.g.: pt_gs_k parameter must match pt_gs_k models");
            }
            
            //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
            
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
            
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}

            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
            bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
        };
        

    }
    
    /** @brief a server for serializable (parts of) hydrology forecasting models,
    *
    * Currently using dlib server_iostream, 
    */
    
    struct server : dlib::server_iostream {
        
        server() =default;    
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server() =default;
        
        std::mutex srv_mx;///< protect server context
        //TODO: make tuple<model_stack_map...>
        //      then do get<stack_type>(model_maps)...
        std::map<string,model_variant_t> model_map;///< key=mid, model.. shared_ptr
        std::map<string,shared_ptr<mutex>> mx_map;///< key=mid, value- shared mutext for model
        
        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server() {
            if(get_listening_port()==0) {
                start_async();
                while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                    std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
            } else {
                start_async();
            }
            return get_listening_port();
        }
        
        string do_get_version_info(){
            return "1.0";
        }
        
        /**  create a model based on geo_cell_data,  default parameter of specified type
         * TODO: consider using table-driven meta-programming approach to minimize maintenance
         */
        
        model_variant_t make_shared_model_of_type(rmodel_type &mtype,vector <core::geo_cell_data> const& gcd) {
            model_variant_t r;
            if(mtype == rmodel_type::pt_gs_k_opt) {
                pt_gs_k::parameter p;
                r= make_shared<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
            } else if ( mtype == rmodel_type::pt_gs_k) {
                pt_gs_k::parameter p;
                r= make_shared<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
            } else if(mtype == rmodel_type::pt_ss_k) {
                pt_ss_k::parameter p;
                r= make_shared<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>>(gcd,p);
            } else if ( mtype == rmodel_type::pt_ss_k_opt) {
                pt_ss_k::parameter p;
                r= make_shared<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
            } else if(mtype == rmodel_type::pt_hs_k) {
                pt_hs_k::parameter p;
                r= make_shared<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
            } else if ( mtype == rmodel_type::pt_hs_k_opt) {
                pt_hs_k::parameter p;
                r= make_shared<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
            } else if(mtype ==rmodel_type::pt_hps_k) {
                pt_hps_k::parameter p;
                r= make_shared<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>>(gcd,p);
            } else if ( mtype == rmodel_type::pt_hps_k_opt) {
                pt_hps_k::parameter p;
                r= make_shared<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
            } else if(mtype == rmodel_type::r_pm_gs_k) {
                r_pm_gs_k::parameter p;
                r= make_shared<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
            } else if ( mtype == rmodel_type::r_pm_gs_k_opt) {
                r_pm_gs_k::parameter p;
                r= make_shared<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
            } else {
                throw runtime_error("currently not supported model type");
            }
            
            return r;
        }
        
        /** @brief creates a new model, with model-id and specified type */
        bool do_create_model(string const& mid, 
                             rmodel_type mtype, 
                             vector <core::geo_cell_data> const& gcd){
            unique_lock<mutex> sl(srv_mx);
            auto i=model_map.find(mid);
            if(i!=model_map.end()) {
                throw runtime_error("drms:model with specified name'"+ mid+"' already exists, please remove it before (re)create");
            }

            model_map[mid] = make_shared_model_of_type(mtype,gcd);//<ptgsk_model_t>(gcd, rp);
            mx_map[mid]=make_shared<mutex>();// also create the mutex that we use to control one at a time access to the model
            return true;
        }

        /** @brief remove (free up mem etc) of region-model  model-id */
        bool do_remove_model(string const& mid){
            unique_lock<mutex> sl(srv_mx);
            auto i=model_map.find(mid);
            if(i==model_map.end()) {
                throw runtime_error("drms: remove, -  no model with specified name'"+ mid+ "'");
            }
            model_map.erase(mid);
            mx_map.erase(mid);
            return true;
        }
        
        /** @brief get models, returns a string list with model identifiers */
        vector<string> do_get_model_ids() {
            vector<string> r;
            unique_lock<mutex> sl(srv_mx);
            for(auto e=model_map.begin();e!=model_map.end();++e)
                r.push_back(e->first);
            return r;
        }

        
        /** @brief given model id, safely get a shared_ptr to that */
        model_variant_t get_model(string mid) {
            unique_lock<mutex> sl(srv_mx);
            auto i=model_map.find(mid);
            if(i!=model_map.end())
                return (*i).second;
            else
                throw runtime_error("drms: not able to find model "+ mid);
        }
        
        /** @brief returns a shared_ptr to the mutex for model id */
        shared_ptr<mutex> get_model_mx(string mid) {
            unique_lock<mutex> sl(srv_mx);
            auto i=mx_map.find(mid);
            if(i!=mx_map.end())
                return (*i).second;
            else
                throw runtime_error("drms:not able to find mx for model "+ mid);
        }
        
        /** @brief rename a model */
        bool do_rename_model(string old_mid, string new_mid){
            unique_lock<mutex> sl(srv_mx);
            auto i=model_map.find(new_mid);
            if(i!=model_map.end()) {
                throw runtime_error("drms:model with specified name'"+ new_mid+"' already exists");
            }
            i=model_map.find(old_mid);
            if(i==model_map.end())
                throw runtime_error("drms: not able to find model "+ old_mid);
            auto m_old = (*i).second;
            shared_ptr<mutex> mx = mx_map[old_mid];
            model_map.erase(old_mid);
            mx_map.erase(old_mid);
            model_map[new_mid] = m_old;
            mx_map[new_mid] = mx;
            return true;
        }
        
        bool do_set_state(string const& mid, state_variant_t const& csv){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor(detail::set_state_visitor{csv},mv);
        }
        
        bool do_set_region_parameter(string const&mid, parameter_variant_t const&p) {
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor(detail::set_region_model_parameter_visitor{p},mv);
        }

        bool do_set_catchment_parameter(string const&mid, parameter_variant_t const&p,size_t cid) {
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor(detail::set_catchment_model_parameter_visitor{p,cid},mv);
        }

        bool do_run_interpolation(string const& mid, 
                                       const shyft::core::interpolation_parameter& ip_parameter,
                                       const shyft::time_axis::generic_dt& ta,
                                       const shyft::api::a_region_environment& r_env,
                                       bool best_effort){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([&ip_parameter,&ta,&r_env,best_effort](const auto&m) { return m->run_interpolation_g(ip_parameter, ta, r_env, best_effort);},mv);
        }
        
        bool do_run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps){
            unique_lock sl(*get_model_mx(mid));
            auto mv =get_model(mid);
            return boost::apply_visitor([use_ncore,start_step,n_steps](auto const&m)->bool {m->run_cells(use_ncore,start_step,n_steps);return true;},mv);//TODO: consider other retval handling
        }
        
        q_adjust_result do_adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1) {
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);//size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1..)
            q_adjust_result q_result = boost::apply_visitor(
                [&indexes,wanted_q,start_step,scale_range,scale_eps,max_iter,n_steps]
                (auto const& m){
                    return m->adjust_state_to_target_flow(wanted_q,indexes,start_step,scale_range,scale_eps,max_iter,n_steps);
                },
                mv
            );
            return q_result;
        }
        
        apoint_ts do_get_discharge(string const& mid, 
                                   const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor(
                    [&cids,ix_type](auto const&m)->apoint_ts{
                        auto cells=m->get_cells();
                        auto stats = api::basic_cell_statistics(cells);
                        return stats.discharge(cids, ix_type);
                    }
                ,mv);
        }
        
        bool do_set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([&catchment_id_list](const auto&m) {m->set_catchment_calculation_filter(catchment_id_list); return true;},mv);
        }
        
        bool do_revert_state(string const& mid) {
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([](const auto&m) {m->revert_to_initial_state(); return true;},mv);
        }
        
        bool do_clone_model(string const& old_mid, string const& new_mid) {
            unique_lock<mutex> sl(srv_mx);
            auto i=model_map.find(new_mid);
            if(i!=model_map.end()) {
                throw runtime_error("drms:model with specified name'"+ new_mid+"' already exists");
            }
            i=model_map.find(old_mid);
            if(i==model_map.end())
                throw runtime_error("drms: not able to find model "+ old_mid);
            auto m_old = (*i).second;
            unique_lock<mutex> ml(*mx_map[old_mid]);
            return  boost::apply_visitor([&new_mid,this](const auto&m)->bool {
                model_variant_t model = m->clone_model();
                model_map[new_mid] = model;
                mx_map[new_mid] = make_shared<mutex>(); 
                return true;
            },m_old);
        }
        
        bool do_set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([&catchment_id, &on_or_off](const auto&m) {m->set_state_collection(catchment_id, on_or_off); return true;},mv);
        }
        
        bool do_set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([&catchment_id, &on_or_off](const auto&m) {m->set_snow_sca_swe_collection(catchment_id, on_or_off); return true;},mv);
        }
        
        bool do_is_cell_env_ts_ok(string const& mid){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([](const auto&m) {return m->is_cell_env_ts_ok();},mv);
        }
        
        bool do_is_calculated(string const& mid, size_t cid){
            unique_lock sl(*get_model_mx(mid));
            auto mv=get_model(mid);
            return boost::apply_visitor([&cid](const auto&m) {return m->is_calculated(cid);},mv);
        }

        /**@brief handle one client connection 
        *
        * Reads messages/requests from the clients,
        * - act and perform request,
        * - return response
        * for as long as the client keep the connection 
        * open.
        * 
        */
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & /*foreign_ip*/,
            const std::string & /*local_ip*/,
            unsigned short /*foreign_port*/,
            unsigned short /*local_port*/,
            dlib::uint64 /*connection_id*/
        ) {

            using shyft::core::core_iarchive;
            using shyft::core::core_oarchive;
            
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                    //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                    switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                        //TODO:: implement switch for each added message-type
                        // dispatch to this class do_method_impl(args)->return_type
                        
                        case message_type::VERSION_INFO: {
                            //core_iarchive ia(in,core_arch_flags);// create the stream
                            //int64_t mid;model_info mi;// decl. variables to read(args to do_fx-call)
                            //ia>>mid>>mi;
                            auto result=do_get_version_info();// get result
                            msg::write_type(message_type::VERSION_INFO,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::CREATE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; rmodel_type mtype; vector <core::geo_cell_data> gcd;// decl. variables to read(args to do_fx-call)
                            ia>>mid>>mtype>>gcd;
                            auto result=do_create_model(mid, mtype, gcd);// get result
                            msg::write_type(message_type::CREATE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::SET_STATE: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; state_variant_t csv;// decl. variables to read(args to do_fx-call)
                            ia>>mid>>csv;
                            auto result=do_set_state(mid, csv);// get result
                            msg::write_type(message_type::SET_STATE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::SET_REGION_PARAMETER: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; parameter_variant_t pv;// decl. variables to read(args to do_fx-call)
                            ia>>mid>>pv;
                            auto result=do_set_region_parameter(mid, pv);// get result
                            msg::write_type(message_type::SET_REGION_PARAMETER,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::SET_CATCHMENT_PARAMETER: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; parameter_variant_t pv;int64_t cid;// decl. variables to read(args to do_fx-call)
                            ia>>mid>>pv>>cid;
                            auto result=do_set_catchment_parameter(mid, pv, size_t(cid));// get result
                            msg::write_type(message_type::SET_CATCHMENT_PARAMETER,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::RUN_INTERPOLATION: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; shyft::time_axis::generic_dt ta; api::a_region_environment r_env; bool best_effort=true;// decl. variables to read(args to do_fx-call)
                            ia>>mid>>ta>>r_env>>best_effort;
                            shyft::core::interpolation_parameter ip_parameter;
                            auto result=do_run_interpolation(mid, ip_parameter, ta, r_env, best_effort);// get result
                            msg::write_type(message_type::RUN_INTERPOLATION,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::RUN_CELLS: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;// decl. variables to read(args to do_fx-call)
                            int64_t use_ncore,start_step, n_steps;
                            ia>>mid>>use_ncore>>start_step>>n_steps;
                            auto result=do_run_cells(mid,size_t(use_ncore),start_step,n_steps);// get result
                            msg::write_type(message_type::RUN_CELLS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::ADJUST_Q: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; shyft::api::cids_t indexes; double wanted_q;// decl. variables to read(args to do_fx-call)
                            int64_t start_step,max_iter,n_steps;
                            double scale_range,scale_eps;
                             //double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1
                            ia>>mid>>indexes>>wanted_q>>start_step>>scale_range>>scale_eps>>max_iter>>n_steps;
                            auto result=do_adjust_q(mid, indexes, wanted_q,size_t(start_step),scale_range,scale_eps,size_t(max_iter),size_t(n_steps));// get result
                            msg::write_type(message_type::ADJUST_Q,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::GET_DISCHARGE: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; shyft::api::cids_t indexes;// decl. variables to read(args to do_fx-call)
                            shyft::core::stat_scope ix_type;
                            ia>>mid>>indexes>>ix_type;
                            auto result=do_get_discharge(mid, indexes,ix_type);// get result
                            msg::write_type(message_type::GET_DISCHARGE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::SET_CATCHMENT_CALCULATION_FILTER: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            std::vector<int64_t> catchment_id_list;
                            ia>>mid>>catchment_id_list;
                            auto result=do_set_catchment_calculation_filter(mid, catchment_id_list);// get result
                            msg::write_type(message_type::SET_CATCHMENT_CALCULATION_FILTER,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::REMOVE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto result=do_remove_model(mid);// get result
                            msg::write_type(message_type::REMOVE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::GET_MODEL_IDS: {
                            auto result=do_get_model_ids();// get result
                            msg::write_type(message_type::GET_MODEL_IDS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::RENAME_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid; 
                            ia>>old_mid>>new_mid;
                            auto result=do_rename_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::RENAME_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::CLONE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid; 
                            ia>>old_mid>>new_mid;
                            auto result=do_clone_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::CLONE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::REVERT_TO_INITIAL_STATE: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto result=do_revert_state(mid);// get result
                            msg::write_type(message_type::REVERT_TO_INITIAL_STATE,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::SET_STATE_COLLECTION: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; int64_t catchment_id; bool on_or_off;
                            ia>>mid>>catchment_id>>on_or_off;
                            auto result=do_set_state_collection(mid, catchment_id, on_or_off);// get result
                            msg::write_type(message_type::SET_STATE_COLLECTION,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::SET_SNOW_SCA_SWE_COLLECTION: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; int64_t catchment_id; bool on_or_off;
                            ia>>mid>>catchment_id>>on_or_off;
                            auto result=do_set_snow_sca_swe_collection(mid, catchment_id, on_or_off);// get result
                            msg::write_type(message_type::SET_SNOW_SCA_SWE_COLLECTION,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::IS_CELL_ENV_TS_OK: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto result=do_is_cell_env_ts_ok(mid);// get result
                            msg::write_type(message_type::IS_CELL_ENV_TS_OK,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        case message_type::IS_CALCULATED: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; size_t cid;
                            ia>>mid>>cid;
                            auto result=do_is_calculated(mid, cid);// get result
                            msg::write_type(message_type::IS_CALCULATED,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;
                        
                        // other 
                        default:
                            throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string((int)msg_type));
                    }
                } catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        }
    };
    
}
