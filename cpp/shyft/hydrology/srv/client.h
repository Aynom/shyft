/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <cstdint>
#include <exception>
#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <thread>

#include <shyft/hydrology/srv/server.h> // looking for header files, plus common defs of variant.

namespace shyft::hydrology::srv {
    using std::vector;
    using std::string;
    using std::string_view;
    using std::to_string;
    using std::runtime_error;
    using std::exception;
    using std::chrono::seconds;
    using std::chrono::milliseconds;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using std::unique_ptr;
    using std::shared_ptr;
    using std::make_unique;
    using std::max;
    using std::this_thread::sleep_for;
    using shyft::core::srv_connection;
    using shyft::core::scoped_connect;
    using shyft::core::do_io_with_repair_and_retry;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    

    /** @brief a client
    *
    * 
    * This class take care of message exchange to the remote server,
    * using the supplied connection parameters.
    * 
    * It implements the message protocol of the server, sending
    * message-prefix, arguments, waiting for response
    * deserialize the response and handle it back to the user.
    * 
    * @see server
    * 
    * 
    */
    struct client {
        srv_connection c;
        //client()=delete;
        client(string host_port,int timeout_ms=1000):c{host_port,timeout_ms}{}
        
        //TODO: implement server_version_info()
        
        /** @brief typical client pattern
        *
        * @param mid the model-id to which we update the model-info on
        * @return true if succeeded (model exists, and was removed)
        * 
        */
        string version_info() {
            scoped_connect sc(c);
            string r{};
            do_io_with_repair_and_retry(c,[this,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::VERSION_INFO,io);
                core_oarchive oa(io, core_arch_flags);
                //oa << mid<<mi;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::VERSION_INFO) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool create_model(string const& mid, 
                          rmodel_type mtype, 
                          vector <core::geo_cell_data> const& gcd){
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, mtype, &gcd](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::CREATE_MODEL,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<mtype<<gcd;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::CREATE_MODEL) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool set_state(string const& mid, state_variant_t const& csv){
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &csv](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_STATE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<csv;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_STATE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool set_region_parameter(string const&mid,parameter_variant_t const& pv) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &pv](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_REGION_PARAMETER,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<pv;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_REGION_PARAMETER) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool set_catchment_parameter(string const&mid,parameter_variant_t const& pv,size_t cid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &pv,cid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_CATCHMENT_PARAMETER,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<pv<<int64_t(cid);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_CATCHMENT_PARAMETER) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }

        bool remove_model(string const&mid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::REMOVE_MODEL,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REMOVE_MODEL) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool rename_model(string const& old_mid, string const& new_mid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &old_mid, &new_mid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::RENAME_MODEL,io);
                core_oarchive oa(io, core_arch_flags);
                oa << old_mid << new_mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::RENAME_MODEL) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool clone_model(string const& old_mid, string const& new_mid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &old_mid, &new_mid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::CLONE_MODEL,io);
                core_oarchive oa(io, core_arch_flags);
                oa << old_mid << new_mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::CLONE_MODEL) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        vector<string> get_model_ids() {
            scoped_connect sc(c);
            vector<string> r;
            do_io_with_repair_and_retry(c,[this,&r](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::GET_MODEL_IDS,io);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::GET_MODEL_IDS) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
            
        }
        
        bool run_interpolation(string const& mid, const shyft::time_axis::generic_dt& ta, const shyft::api::a_region_environment& r_env, bool best_effort) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &r_env, &ta, &best_effort](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::RUN_INTERPOLATION,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<ta<<r_env<<best_effort;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::RUN_INTERPOLATION) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool run_cells(string const& mid,size_t use_ncore=0,int start_step=0,int n_steps=0) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid,use_ncore,start_step,n_steps](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::RUN_CELLS,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<int64_t(use_ncore)<<int64_t(start_step)<<int64_t(n_steps);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::RUN_CELLS) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        q_adjust_result adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1) {
            scoped_connect sc(c);
            q_adjust_result q_result;
            do_io_with_repair_and_retry(c,[this,&q_result, &mid, &wanted_q, &indexes,start_step,scale_range,scale_eps,max_iter,n_steps](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::ADJUST_Q,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<indexes<<wanted_q<<int64_t(start_step)<<scale_range<<scale_eps<<int64_t(max_iter)<<int64_t(n_steps);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::ADJUST_Q) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> q_result;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return q_result;
        }
        
        apoint_ts get_discharge(string const& mid, 
                                const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
            scoped_connect sc(c);
            apoint_ts ts;
            do_io_with_repair_and_retry(c,[this,&ts, &mid, &indexes,ix_type](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::GET_DISCHARGE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid << indexes<<ix_type;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::GET_DISCHARGE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> ts;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return ts;
        }
        
        bool set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &catchment_id_list](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_CATCHMENT_CALCULATION_FILTER,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid<<catchment_id_list;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_CATCHMENT_CALCULATION_FILTER) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool revert_to_initial_state(string const& mid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::REVERT_TO_INITIAL_STATE,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REVERT_TO_INITIAL_STATE) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &catchment_id, &on_or_off](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_STATE_COLLECTION,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid << catchment_id << on_or_off;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_STATE_COLLECTION) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &catchment_id, &on_or_off](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::SET_SNOW_SCA_SWE_COLLECTION,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid << catchment_id << on_or_off;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SET_SNOW_SCA_SWE_COLLECTION) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool is_cell_env_ts_ok(string const& mid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::IS_CELL_ENV_TS_OK,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::IS_CELL_ENV_TS_OK) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        bool is_calculated(string const& mid, size_t cid) {
            scoped_connect sc(c);
            bool r{false};
            do_io_with_repair_and_retry(c,[this,&r, &mid, &cid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                msg::write_type(message_type::IS_CALCULATED,io);
                core_oarchive oa(io, core_arch_flags);
                oa << mid << cid;
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::IS_CALCULATED) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
            });
            return r;
        }
        
        /** @brief close, until needed again, the server connection
        *
        */
        void close() {
            c.close();//just close-down connection, it will auto-open if needed
        }
    };
}
