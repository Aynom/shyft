/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/model.h>


namespace shyft::energy_market::market {

    power_line::power_line(shared_ptr<model>const&m,shared_ptr<model_area>& a1, shared_ptr<model_area>& a2, int id, const string& name,const string& json)
        :id_base{id,name,json},mdl(m),  area_1(a1), area_2(a2) {
        if (a1 == a2 || a1 == nullptr || a2 == nullptr)
            throw runtime_error("A power line must connect two different ModelAreas");
    }

    bool power_line::operator==(const power_line &o) const {
        return id_base::operator==(o);
            //TODO: Chekc area_1 and area_2?!
            //&& area_1.lock() == o.area_1.lock()
            //&& area_2.lock() == o.area_2.lock();
    }


}
