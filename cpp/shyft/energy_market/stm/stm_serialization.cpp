
#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>

#include <shyft/energy_market/dataset.h>

#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/water_route.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

// stuff needed to ensure vector<T>, map<K,V> etc. are automagically serializable
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>

#include <sstream>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>


/** from  google how to serialize tuple using a straight forward tuple expansion */
namespace boost::serialization {

    /** generic recursive template version of tuple_serialize */
    template<int N>
    struct tuple_serialize {
        template<class Archive, typename ...tuple_types>
        static void serialize(Archive& ar, std::tuple<tuple_types...>& t, const unsigned version) {
            ar & make_nvp("tpl_m", std::get<N - 1>(t));
            tuple_serialize<N - 1>::serialize(ar, t, version); // recursive expand/iterate over members
        }
    };
    /** specialize recurisive template expansion termination at 0 args */
    template<>
    struct tuple_serialize<0> {
        template<class Archive, typename ...tuple_types>
        static void serialize(Archive&, std::tuple<tuple_types...>&, const unsigned) {
            ;// zero elements to serialize, noop, btw: should we instead stop at <1>??
        }
    };

    template<class Archive, typename ...tuple_types>
    void serialize(Archive& ar, std::tuple<tuple_types...>& t, const unsigned version) {
        tuple_serialize<sizeof ...(tuple_types)>::serialize(ar, t, version);
    }

}

/** provide serialization as free function for the enum */
namespace boost::serialization {
    
template <class Archive,typename A>
void serialize(Archive& ar, shyft::energy_market::core::ds_ref<A>&r, const unsigned int /*version*/) {
    ar
    &make_nvp("id",r.id)
    &make_nvp("aid",r.attr_id)
    ;
}

}

// instantiate serialization templates here, ref. shyft expression-serialization for the ids_c  type
using namespace boost::serialization;
using core_iarchive=boost::archive::binary_iarchive;
using core_oarchive=boost::archive::binary_oarchive;


template <typename I>
template <class Archive>
void shyft::energy_market::core::dataset<I>::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("name",name)
    & make_nvp("elements",elements)
    ;
}

template < class ...ds_types>
template <class Archive>
void shyft::energy_market::core::ds_collection<ds_types...>::serialize(Archive& ar, const unsigned int /*version*/) {
    ar 
    & make_nvp("all_id",all_id)
    ;
}

template<class Archive>
void shyft::energy_market::stm::reservoir::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::unit::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& 	make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::power_plant::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& 	make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::catchment::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& make_nvp("super",	base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::waterway::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	&  make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::gate::serialize(Archive & ar, const unsigned int /*version*/) {
	ar
	&  make_nvp("super", base_object<super>(*this))
	;
}

template<class Archive>
void shyft::energy_market::stm::stm_hps::serialize(Archive & ar, const unsigned int /*version*/) {
    ar
    & make_nvp("super",base_object<super>(*this)) // core hydro power goes here(core model,topology)
    & make_nvp("ids",ids) //  and here is the corresponding proxy-attributes 
    & make_nvp("rds",rds) //  results
    ;
}

template<class Archive>
void shyft::energy_market::stm::stm_system::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("super",base_object<super>(*this))
    & make_nvp("hps",hps)
    & make_nvp("market",market)
    & make_nvp("ids",ids)
    & make_nvp("rds",rds)
    ;
}

template<class Archive>
void shyft::energy_market::stm::hps_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("rsv",rsv)
    & make_nvp("wtr",wtr)
    & make_nvp("pwr_stn", pwr_plant)
    & make_nvp("agg",unitds)
    & make_nvp("catchment", ctchm)
	& make_nvp("gt",gt)
    ;
}

template<class Archive>
void shyft::energy_market::stm::market_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("ema",ema)
    ;
}

template<class Archive>
void shyft::energy_market::stm::energy_market_area::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("super",base_object<super>(*this))
    & make_nvp("sys",sys)//weak_ptr Hmm. did that really work ..
    ;
}

//
// 4. Then include the archive supported
//

// repeat template instance for each archive class
#define xxx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)

xxx_arch(shyft::energy_market::stm::unit);
xxx_arch(shyft::energy_market::stm::power_plant);
xxx_arch(shyft::energy_market::stm::reservoir);
xxx_arch(shyft::energy_market::stm::catchment);
xxx_arch(shyft::energy_market::stm::waterway);
xxx_arch(shyft::energy_market::stm::gate);
xxx_arch(shyft::energy_market::stm::stm_hps);
xxx_arch(shyft::energy_market::stm::stm_system);
xxx_arch(shyft::energy_market::stm::energy_market_area);


namespace shyft::energy_market::stm {

/** polymorphic types needed some pre-registration to stream
 * If you get 'unknown class' error while doing serialization,
 * this is the place.
 */
template <class Archive>
void register_types(Archive& a) {
    a.template register_type<reservoir>();
    a.template register_type<waterway>();
    a.template register_type<unit>();
	a.template register_type<gate>();
	a.template register_type<power_plant>();
    a.template register_type<catchment>();
    a.template register_type<stm_hps>();
    a.template register_type<stm_system>();
    a.template register_type<energy_market_area>();
}


/**fx_to_blob simply serializes an object to a blob */
template <class T>
static string fx_to_blob(const shared_ptr<T>&s) {
    using namespace std;
    std::ostringstream xmls;
    {
        core_oarchive oa(xmls,core_arch_flags);
        register_types(oa);
        oa << boost::serialization::make_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
}

/** fx_from_blob de-serializes a blob to a fully working object*/
template<class T>
static shared_ptr<T> fx_from_blob(const string &xmls) {
    shared_ptr<T> s;
    std::istringstream xmli(xmls); {
        //boost::archive::xml_iarchive ia(xmli);
        core_iarchive ia(xmli,core_arch_flags);
        register_types(ia);
        ia >> boost::serialization::make_nvp("hps", s);
	}
    return s;
}

// implementation for main classes, using the templates above

string  stm_hps::to_blob(const shared_ptr<stm_hps>&s) {return fx_to_blob<stm_hps>(s);}
shared_ptr<stm_hps> stm_hps::from_blob(const string &xmls) {return fx_from_blob<stm_hps>(xmls);}
string  stm_system::to_blob(const shared_ptr<stm_system>&s) { return fx_to_blob<stm_system>(s);}
shared_ptr<stm_system> stm_system::from_blob(const string &xmls) { return fx_from_blob<stm_system>(xmls);}

    
}
