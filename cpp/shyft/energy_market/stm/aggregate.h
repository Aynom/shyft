#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
	using std::map;
	using shyft::core::utctime;
	using shyft::time_series::dd::apoint_ts;
	using core::proxy_attr;

	struct unit : hydro_power::unit {
		using super = hydro_power::unit;
		using ds_collection_t =unit_ds;
		using ids = hps_ids<unit>;
		using rds = hps_rds<unit>;
        using e_attr=unit_attr; // enum attr type
        using e_attr_seq_t=unit_attr_seq_t;// the full sequence of attr type

		unit(int id, const string& name, const string&json, const stm_hps_ &hps);
		unit()=default;

		proxy_attr<unit, t_double_, unit_attr, unit_attr::production_min_static, ids> production_min_static{*this};
		proxy_attr<unit, t_double_, unit_attr, unit_attr::production_max_static, ids> production_max_static{*this};
		proxy_attr<unit, t_xy_, unit_attr, unit_attr::generator_efficiency, ids> generator_efficiency{ *this };
		proxy_attr<unit, t_turbine_description_, unit_attr,unit_attr::turbine_description, ids> turbine_description{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::unavailability, ids> unavailability{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::production_min, ids> production_min{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::production_max, ids> production_max{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::production_schedule, ids> production_schedule{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::discharge_min, ids> discharge_min{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::discharge_max, ids> discharge_max{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::discharge_schedule, ids> discharge_schedule{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::cost_start, ids> cost_start{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::cost_stop, ids> cost_stop{ *this };
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::production, rds> production{*this};
		proxy_attr<unit, apoint_ts, unit_attr, unit_attr::discharge, rds> discharge{*this};
        
        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=unit;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(unit_attr,production_min_static)
            def_proxy_map(unit_attr,production_max_static)
            def_proxy_map(unit_attr,generator_efficiency)
            def_proxy_map(unit_attr,turbine_description)
            def_proxy_map(unit_attr,unavailability)
            def_proxy_map(unit_attr,production_min)
            def_proxy_map(unit_attr,production_max)
            def_proxy_map(unit_attr,production_schedule)
            def_proxy_map(unit_attr,discharge_min)
            def_proxy_map(unit_attr,discharge_max)
            def_proxy_map(unit_attr,discharge_schedule)
            def_proxy_map(unit_attr,cost_start)
            def_proxy_map(unit_attr,cost_stop)
            def_proxy_map(unit_attr,production)
            def_proxy_map(unit_attr,discharge)
        };
        
		x_serialize_decl();
	};
	using unit_ = shared_ptr<unit>;

}
//x_serialize_export_key(shyft::energy_market::stm::unit);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::stm::unit, BOOST_PP_STRINGIZE(shyft::energy_market::stm::aggregate));

