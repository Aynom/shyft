/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>

namespace shyft::energy_market::hydro_power {

    using std::dynamic_pointer_cast;
            

    const reservoir_& reservoir::input_from(reservoir_ const& me,waterway_ const& w) { 
        connect(w,me); 
        return me; 
    }
    const reservoir_& reservoir::output_to(reservoir_ const& me,waterway_ const& w, connection_role role) {
        connect(me, role, w);
        return me;
    }

    /// <summary>
    /// note: it will not find through junctions (yet)!
    /// </summary>
    vector<unit_> reservoir::downstream_powerstations() const {
        vector<unit_> r;
        for(const auto& ds:downstreams) {
            auto ds_ps = downstream_powerstation_closure(dynamic_pointer_cast<waterway>(ds.target_()));
            if (ds_ps != nullptr)
                r.push_back(ds_ps);
        }
        // maybe:
        // copy_if(downstreams.cbegin(),downstreams.cend(),
        //         back_inserter(r), .. but this needs type dynamic_pointer_cast
        //        [](const auto&w)->aggregate_ {..}
        // );
        return r;
    }
            
    vector<unit_> reservoir::upstream_powerstations() const {
        vector<unit_> r;
        for(const auto &us:upstreams) 
            upstream_powerstation_closure(dynamic_pointer_cast<waterway>(us.target_()), r);
        return r;
    }
            
    vector<unit_> reservoir::upstream_powerstation_closure(const waterway_c& w, vector<unit_>& r) const {
        if (w == nullptr || w->upstreams.size() == 0)
            return r;
        for(const auto& us: w->upstreams) {
            if (dynamic_pointer_cast<unit>(us.target_()))
                r.push_back(dynamic_pointer_cast<unit>(us.target_()));
            else if (dynamic_pointer_cast<waterway const>(us.target_()))
                upstream_powerstation_closure(dynamic_pointer_cast<waterway const >(us.target_()), r);
        }
        return r;
    }

    unit_  reservoir::downstream_powerstation_closure(const waterway_c& w) const {
        if (w == nullptr || w->downstream() == nullptr || dynamic_pointer_cast<reservoir>(w->downstream()))
            return nullptr;
        if (dynamic_pointer_cast<unit>(w->downstream()))
            return dynamic_pointer_cast<unit>(w->downstream());
        return downstream_powerstation_closure(dynamic_pointer_cast<const waterway>(w->downstream() ));
    }

    bool reservoir::operator==(const reservoir&o)const {
        return id_base::operator==(o);
    }

}
