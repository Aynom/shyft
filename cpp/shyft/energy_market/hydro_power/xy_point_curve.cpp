/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <stdexcept>
#include <boost/format.hpp>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>

namespace shyft::energy_market::hydro_power {

    using std::sort;
    using std::runtime_error;
            
    xy_point_curve::xy_point_curve( vector<point> const& points_a) :points(points_a) {
        sort(points.begin(), points.end());
    }
    xy_point_curve::xy_point_curve(const vector<double>&x, const vector<double>& y) {
        if (x.size() != y.size())
            throw runtime_error((boost::format("x points size %1% and y points size %2% lists must have same dimension") % x.size() % y.size()).str());
        points.reserve(x.size());
        for (size_t i = 0; i < x.size(); ++i)
            points.emplace_back(x[i], y[i]);
        sort(points.begin(), points.end());
    }

    bool xy_point_curve::is_xy_mono_increasing() const {
        if (points.size() < 2)
            return false;
        for (size_t i = 1; i < points.size(); i++) {
            if (points[i - 1].x >= points[i].x || points[i - 1].y >= points[i].y)
                return false;
        }
        return true;
    }

    double xy_point_curve::calculate_y(double from_x) const {
        if (points.size() == 0)
            return std::numeric_limits<double>::quiet_NaN();
        if (points.size() == 1)
            return points[0].y;
        auto r = lower_bound(points.cbegin(), points.cend(), from_x, []( const point& pt, double x)->bool { return pt.x < x; });
        int ix = static_cast<int>(r - points.cbegin()) - 1;
        if (ix < 0) { //below lowest point ?
            ix = 0;// extrapolate downwards
        }
        if (ix+1 == points.size()) {// above highest point
            ix = ix - 1;//extrapolate upwards.
        }
        // here we got two points on a linear curve we are going to use:
        auto a = (points[ix + 1].y - points[ix].y) / (points[ix + 1].x - points[ix].x);
        auto b = points[ix].y - a * points[ix].x;
        return a * from_x + b;
    }

    double xy_point_curve::calculate_x(double from_y) const {
        if (points.size() == 0)
            return std::numeric_limits<double>::quiet_NaN();
        if (points.size() == 1)
            return points[0].x;
        auto r = lower_bound(points.cbegin(), points.cend(), from_y, [](const point& pt,double y)->bool { return pt.y < y; });
        int ix = static_cast<int>(r - points.cbegin()) - 1;
        if (ix < 0) { //below lowest point ?
            ix = 0;// extrapolate downwards
        }
        if (ix +1== points.size() ) {// above highest point
            ix = ix - 1;//extrapolate upwards.
        }
        // here we got two points on a linear curve we are going to use:
        auto a = (points[ix + 1].x - points[ix].x) / (points[ix + 1].y - points[ix].y);
        auto b = points[ix].x - a * points[ix].y;
        return a * from_y + b;
    }

}
