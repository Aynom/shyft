/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <map>
#include <string>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <shyft/core/core_serialization.h>

namespace shyft::energy_market::core {
    
using std::map;
using std::string;
using std::vector;
using std::tuple;
using std::runtime_error;
using std::to_string;

/** @brief a reference to object.attr by object-id and enum for attr. 
* 
* The attribute could/should be scoped to the type, so separate enums for each object type.
* 
* It is used as a building block, persistable/serializable adress element for input-data-sets.
* 
*/
template<typename A>
struct ds_ref {
    int64_t id; ///< the object id, unique pr.type, set/determined by users, eg. reservoir blåsjø =16606
    A attr_id;///< the attribute enum type, as determined/set/requested by expert users, e.g. .hrv, .min_restriction
    // maybe: size_t ix; to be able to adress a single element out of a scenario-collection.
    
    bool operator < (const ds_ref &o) const noexcept {return id<o.id || (id==o.id && attr_id<o.attr_id);}
    bool operator==(const ds_ref &o) const noexcept {return id==o.id && attr_id==o.attr_id;}
};

/** @brief A input data set,  dataset for short
* 
* Contains a name, describing the dataset (for users to keep track of stuff)
* and a typed unique map driven by ds_ref<A> as key and value-type V as value.
* 
* 
* @tparam V the value type
* @tparam A the enum attribute type
*/
/// helper class to make pairs of enum attribute and value-type, e.g object-type.attribute of type V
template <typename V,typename A>
struct ds_t {
    using value_type=V;
    using attribute_type=A;
};

template < typename  I >
struct dataset {
    using value_type=typename I::value_type;
    using attribute_type=typename I::attribute_type;
    string name;///< some descriptive name for the dataset, could be a .rst formattet string for all we know
    map<ds_ref<attribute_type>,value_type> elements;///< the elements, map from ds_ref<A> to value_type V
    x_serialize_decl();
};

/** @brief A simple ds_collection template class
*
*  Can be parameterized with all legal/ combinations of value-type, attribute-type
*
* Provides get_attr,set_attr, and has_attr,
*
* @tparam ds_types a parameter pack of type ds_t<value_type,enum_attr_type>
*
*/
template <class ...ds_types> // a list of valid dataset types, e.g. ds_t<double, rsv_attr>, ds_t<time_series,rsv_attr>
struct ds_collection {
    tuple< dataset<ds_types>...> all_id;// expand template arg.pack into a tuple, .. dataset<value_type,xx> 

    /** @brief Get the specified attribute for the specified object
    *
    * @note succeeds or throws if not found in the dataset
    *
    * @tparam T the value type  to retrieve, could be double, time_series, map<utctime,xy_curve>..
    * @tparam A the attribute , enum, and we keep one for each object type, e.g. enum rsv_attr, or aggr_attr etc.
    *
    * @param id the object id as persisted in the dataset, like id=16606 for blåsjø
    * @param a the specific attribute to retrieve. e.g. hrv, lrv etc.
    *
    * @return the specified value
    */
    template <typename T, typename A>
    T get_attr(int id, A a) const {
        const auto &m = std::get< dataset<ds_t<T, A>> >(all_id).elements;
        const auto & f = m.find(ds_ref<A>{id, a});
        if (f != m.end())
            return (*f).second;
        throw runtime_error(string("Attemt to read not-yet-set attribute for object: id= ") + to_string(id) + string(", a_id=") + to_string(int(a)));
    }

    /** @brief Set the specified attribute for the specified object to a value
    *
    * @tparam T the value type  to retrieve, could be double, time_series, map<utctime,xy_curve>..
    * @tparam A the attribute , enum, and we keep one for each object type, e.g. enum rsv_attr, or aggr_attr etc.
    *
    * @param id the object id as persisted in the dataset, like id=16606 for blåsjø
    * @param a the specific attribute to retrieve. e.g. hrv, lrv etc.
    * @param v
    *
    */
    template <typename T, typename A>
    void set_attr(int id, A a, const T&v) {
        auto & i = std::get< dataset<ds_t<T, A>> >(all_id);
        i.elements[ds_ref<A>{id, a}] = v;
    }

    /** @brief Check if the specified attribute for the specified object exists
    *
    *
    * @tparam T the value type  to retrieve, could be double, time_series, map<utctime,xy_curve>..
    * @tparam A the attribute , enum, and we keep one for each object type, e.g. enum rsv_attr, or aggr_attr etc.
    *
    * @param id the object id as persisted in the dataset, like id=16606 for blåsjø
    * @param a the specific attribute to retrieve. e.g. hrv, lrv etc.
    *
    * @return true if the value is found
    */
    template <typename T, typename A>
    bool has_attr(int id, A a)  const {
        const auto & i = std::get< dataset<ds_t<T, A>> >(all_id);
        return i.elements.find(ds_ref<A>{id, a}) != i.elements.end();
    }

    template <typename T, typename A>
    bool remove_attr(int id, A a) {
        auto & i = std::get< dataset<ds_t<T, A>> >(all_id);
        return i.elements.erase(ds_ref<A>{id, a})>0;
    }
    x_serialize_decl();
};
    
}
//x_serialize_export_key_nt(shyft::energy_market::core::ds_ref);
//x_serialize_export_key(shyft::energy_market::core::dataset);
//x_serialize_export_key(shyft::energy_market::core::ids_collection_);
