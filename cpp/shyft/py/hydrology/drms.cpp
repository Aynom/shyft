/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <future>
#include <mutex>
#include <shyft/hydrology/srv/server.h>
#include <shyft/hydrology/srv/client.h>
#include <shyft/py/scoped_gil.h>

namespace expose {
    using std::mutex;
    using std::unique_lock;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::hydrology::srv::client;
    using shyft::hydrology::srv::server;
    using shyft::hydrology::srv::rmodel_type;
    using shyft::hydrology::srv::parameter_variant_t;
    using shyft::hydrology::srv::state_variant_t;
    namespace py=boost::python;
    using namespace shyft::py;
    using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    

    /** @brief A  client 
     *
     * This class takes care of  python gil and mutex, ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     * 
     * 
     */
    struct py_client {
        using client_t=client;
        mutex mx; ///< to enforce just one thread active on this client object at a time
        client_t impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {}
        ~py_client() { }

        py_client()=delete;
        py_client(py_client const&) = delete;
        py_client(py_client &&) = delete;
        py_client& operator=(py_client const&o) = delete;

        void close(int timeout_ms=1000) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close();
        }
        
        string get_server_version() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.version_info();
        }
        
        vector<string> get_model_ids() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_ids();
        }
        
        bool remove_model(string const& mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model(mid);
        }
        
        bool rename_model(string const& mid, string const&new_mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.rename_model(mid,new_mid);
        }
        
        bool create_model(string const& mid, 
                          rmodel_type mtype, 
                          vector <shyft::core::geo_cell_data> const& gcd){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.create_model(mid, mtype, gcd);
        }
        
        bool set_state_variant(string const& mid, state_variant_t sv){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_state(mid, sv);
        }
        
        template<class S>
        bool set_state(string const& mid, S s) { return set_state_variant(mid,state_variant_t{s}); }


        bool set_region_parameter_variant(string const& mid, 
                       parameter_variant_t pv){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_region_parameter(mid, pv);
        }
        
        bool set_catchment_parameter_variant(string const& mid, 
                       parameter_variant_t pv,size_t cid){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_catchment_parameter(mid, pv,cid);
        }
        
        template<class P>
        bool set_region_parameter(string const& mid, P p) { return set_region_parameter_variant(mid,parameter_variant_t{p}); }

        template<class P>
        bool set_catchment_parameter(string const& mid, P p, size_t cid) { return set_catchment_parameter_variant(mid,parameter_variant_t{p},cid); }
        
        bool run_interpolation(string const& mid, 
                         const shyft::time_axis::generic_dt& ta, 
                         const shyft::api::a_region_environment& r_env, 
                         bool best_effort){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.run_interpolation(mid, ta, r_env, best_effort);
        }
        
        bool run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.run_cells(mid,use_ncore,start_step,n_steps);
        }
        
        q_adjust_result adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.adjust_q(mid, indexes, wanted_q,start_step,scale_range,scale_eps,max_iter,n_steps);
        }
        
        apoint_ts get_discharge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_discharge(mid, indexes,ix_type);
        }
        
        bool set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_catchment_calculation_filter(mid, catchment_id_list);
        }
        
        bool revert_to_initial_state(string const& mid){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.revert_to_initial_state(mid);
        }
        
        bool clone_model(string const& old_mid, string const& new_mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.clone_model(old_mid, new_mid);
        }
        
        bool set_state_collection(string const& mid, int64_t catchment_id, bool on_or_off) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_state_collection(mid, catchment_id, on_or_off);
        }
        
        bool set_snow_sca_swe_collection(string const& mid, int64_t catchment_id, bool on_or_off) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.set_snow_sca_swe_collection(mid, catchment_id, on_or_off);
        }
        
        bool is_cell_env_ts_ok(string const& mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.is_cell_env_ts_ok(mid);
        }
        
        bool is_calculated(string const& mid, size_t cid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.is_calculated(mid, cid);
        }
        
        void close_conn() {//weird, close is not a name we can use here..
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close();
        }
    };

    /** @brief The server side component for a model repository
     *
     * 
     * This class wraps/provides the server-side 
     * suitable for exposure to python.
     * 
     */
    
    struct py_server  {
         server impl;
        py_server() {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();// ensure threads-is enabled
            }
        }
        
        void set_listening_port(int port) {impl.set_listening_port(port);}
        int  get_listening_port() { return impl.get_listening_port();}
        
        void set_max_connections(int  n) {impl.set_max_connections(size_t(n));}
        int  get_max_connections() { return int(impl.get_max_connections());}
        
        void set_listening_ip(string const& ip) {impl.set_listening_ip(ip);}
        string  get_listening_ip() {return impl.get_listening_ip();}
        
        int start_server() {return impl.start_server(); }
        void stop_server(int timeout_ms) { 
            impl.set_graceful_close_timeout(timeout_ms);
            impl.clear(); 
        }
        
        string get_version_info() {return impl.do_get_version_info();}
        
        bool is_running() { return impl.is_running(); }
        
        ~py_server() { }
        //-- get rid of stuff that would not work
         py_server(py_server const&)=delete;
         py_server(py_server&&)=delete;
         py_server& operator=(py_server const &)=delete;
         py_server& operator=(py_server&&)=delete;
    };

    void expose_common() {
        py::enum_<rmodel_type> ("RegionModelType","Ref to DrmClient, used do specify what remote region-model type to create")
        .value("PT_GS_K",rmodel_type::pt_gs_k)
        .value("PT_GS_K_OPT",rmodel_type::pt_gs_k_opt)
        .value("PT_SS_K",rmodel_type::pt_ss_k)
        .value("PT_SS_K_OPT",rmodel_type::pt_ss_k_opt)
        .value("PT_HS_K",rmodel_type::pt_hs_k)
        .value("PT_HS_K_OPT",rmodel_type::pt_hs_k_opt)
        .value("PT_HPS_K",rmodel_type::pt_hps_k)
        .value("PT_HPS_K_OPT",rmodel_type::pt_hps_k_opt)
        .value("R_PM_GS_K",rmodel_type::r_pm_gs_k)
        .value("R_PM_GS_K_OPT",rmodel_type::r_pm_gs_k_opt)
        .export_values();
    }
    
    
    /** @brief Expose to python the client side api for a model-repository of type M
     * 
     * @tparam M the model type, same requirements as for shyft::energy_market::srv::client
     */
    
    void expose_client() {
        using cm=py_client;
        using shyft::api::cell_state_with_id;
        using namespace shyft::core;
        
        py::class_<cm,boost::noncopyable>(
            "DrmClient",
            doc_intro(
                "Distributed region model client provides all needed functionality to transfer\n"
                "Shyft region-models, and then run simulations/optimizations"
            )
             ,py::no_init)
           .def(
                py::init<string const&,int>(
                    (py::arg("self"),py::arg("host_port"),py::arg("timeout_ms")),
                    "TODO"
                )
            )
            .add_property("server_version",&cm::get_server_version,
                 
                doc_intro("returns the server version")
                doc_returns("version","str","Server version string")
            )
            
            .def("close",&cm::close_conn,
                (py::arg("self")),
                doc_intro("Close the connection, it will auto-open if ever needed")
            )
            
            .def("create_model",&cm::create_model,
                (py::arg("self"), py::arg("mid"), py::arg("mtype"), py::arg("gcd")),
                doc_intro("create the model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("mtype", "enum rmodel_type", "rmodel_type.pt_gs_k rmodel_type.pt_gs_k_opt or similar model type")
                doc_parameter("gcd", "GeoCellDataVector", "strongly typed list of GeoCellData")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("remove_model",&cm::remove_model,
                (py::arg("self"), py::arg("mid")),
                doc_intro("remove the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("rename_model",&cm::rename_model,
                (py::arg("self"), py::arg("mid"),py::arg("new_mid")),
                doc_intro("rename the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("new_mid","str", "the new wanted model-id for the model")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("clone_model",&cm::clone_model,
                (py::arg("self"), py::arg("mid"),py::arg("new_mid")),
                doc_intro("clone the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "original model")
                doc_parameter("new_mid","str", "the model-id for the cloned model")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("get_model_ids",&cm::get_model_ids,
                (py::arg("self")),
                doc_intro("returns a list of model ids (mids) that is alive and known at the remote server")
                doc_returns("", "StringList", "Strongly typed list of strings naming the available models on the drms")
            )
            //-- overloading all stack states goes here (alternative: register implicit convert from T -> variant<T> for state etc)
            .def<bool (cm::*)(string const&,shared_ptr<vector<cell_state_with_id<pt_gs_k::state>>>)>("set_state",&cm::template set_state<shared_ptr<vector<cell_state_with_id<pt_gs_k::state>>> >,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            .def<bool (cm::*)(string const&,shared_ptr<vector<cell_state_with_id<pt_ss_k::state>>>)>("set_state",&cm::template set_state<shared_ptr<vector<cell_state_with_id<pt_ss_k::state>>> >,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            .def<bool (cm::*)(string const&,shared_ptr<vector<cell_state_with_id<pt_hs_k::state>>>)>("set_state",&cm::template set_state<shared_ptr<vector<cell_state_with_id<pt_hs_k::state>>> >,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            .def<bool (cm::*)(string const&,shared_ptr<vector<cell_state_with_id<pt_hps_k::state>>>)>("set_state",&cm::template set_state<shared_ptr<vector<cell_state_with_id<pt_hps_k::state>>> >,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            .def<bool (cm::*)(string const&,shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state>>>)>("set_state",&cm::template set_state<shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state>>> >,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            //-- overloading all stack parameters goes here (see alt. using impl. type conversion registration for state)
            .def<bool (cm::*)(string const&,shared_ptr<pt_gs_k::parameter>) >("set_region_parameter",&cm::template set_region_parameter<shared_ptr<pt_gs_k::parameter>>,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )
             .def<bool (cm::*)(string const&,shared_ptr<pt_hs_k::parameter>) >("set_region_parameter",&cm::template set_region_parameter<shared_ptr<pt_hs_k::parameter>>,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def<bool (cm::*)(string const&,shared_ptr<pt_ss_k::parameter>) >("set_region_parameter",&cm::template set_region_parameter<shared_ptr<pt_ss_k::parameter>>,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def<bool (cm::*)(string const&,shared_ptr<pt_hps_k::parameter>) >("set_region_parameter",&cm::template set_region_parameter<shared_ptr<pt_hps_k::parameter>>,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def<bool (cm::*)(string const&,shared_ptr<r_pm_gs_k::parameter>) >("set_region_parameter",&cm::template set_region_parameter<shared_ptr<r_pm_gs_k::parameter>>,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )

            //--
            .def("run_interpolation",&cm::run_interpolation,
                (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("r_env"), py::arg("best_effort")),
                doc_intro("initializes the cell environment and project region environment time_series to cells.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("ta", "TimeAxis", "time axis")
                doc_parameter("r_env", "ARegionEnvironment", "region environment containing all geo-located sources")
                doc_parameter("best_effort", "bool", "best effort type interpolation")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("run_cells",&cm::run_cells,
                (py::arg("self"), py::arg("mid"),py::arg("use_ncore")=0,py::arg("start_step")=0,py::arg("n_steps")=0
                ),
                doc_intro("Run calculation over the specified time axis. Requires that cells and environment have been initialized and interpolated.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("use_ncore","int","default 0, means autodetect core, otherwise you can specify 1..n")
                doc_parameter("start_step","int","default 0, from start step of time-axis")
                doc_parameter("n_steps","int","default 0, means run entire time-axis, 1..n means number of steps from start_step")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("adjust_q",&cm::adjust_q,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"), py::arg("wanted_q"),
                    py::arg("start_step")=0,py::arg("scale_range")=3.0,py::arg("scale_eps")=1e-3,py::arg("max_iter")=300,py::arg("n_steps")=1
                ),
                doc_intro("State adjustment to achieve wanted/observed flow")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "cell ids that should be adjusted")
                doc_parameter("wanted_q", "float", "the average flow first time-step we want to achieve, in m**3/s")
                doc_parameter("start_step","int","time-axis start step index, default=0")
                doc_parameter("scale_range","float","default=3, min,max= scale0/scale_range.. scale0*scale_range")
                doc_parameter("scale_eps","float","default 1e-3, stop iteration when scale-change is less than this")
                doc_parameter("max_iter","int","default=300, stop searching for solution after this limit is reached")
                doc_parameter("n_steps","int","default=1, number of steps on time-axis to average to match wanted flow")
                doc_returns("", "q_adjust_result", "obtained flow in m**3/s. This can deviate from wanted flow due to model and state constraints. q_r is optimized q and q_0 is unadjusted.")
            )
            
            .def("get_discharge",&cm::get_discharge,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the discharge")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("set_catchment_calculation_filter", &cm::set_catchment_calculation_filter,
                 (py::arg("self"), py::arg("mid"),py::arg("catchment_id_list")),
                 doc_intro("set catchment calculation filter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id_list", "List[int]", "list of catchment indexes")
                 doc_returns("", "bool", "true if succeeded")
             )
            
            .def("revert_to_initial_state", &cm::revert_to_initial_state,
                 (py::arg("self"), py::arg("mid")),
                 doc_intro("revert model to initial state")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def("set_state_collection", &cm::set_state_collection,
                 (py::arg("self"), py::arg("mid"), py::arg("catchment_id"), py::arg("on_or_off")),
                 doc_intro("enable state collection for specified or all cells")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id", "int", "to enable state collection for, -1 means turn on/off for all")
                 doc_parameter("on_or_off", "bool", "on_or_off true|or false")
                 doc_returns("", "bool", "true if succeeded")
             )
            
            .def("set_snow_sca_swe_collection", &cm::set_snow_sca_swe_collection,
                 (py::arg("self"), py::arg("mid"), py::arg("catchment_id"), py::arg("on_or_off")),
                 doc_intro("enable/disable collection of snow sca|sca for calibration purposes")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id", "int", "to enable snow calibration for, -1 means turn on/off for all")
                 doc_parameter("on_or_off", "bool", "on_or_off true|or false")
                 doc_returns("", "bool", "true if succeeded")
             )
            
            .def("is_cell_env_ts_ok",&cm::is_cell_env_ts_ok,
                (py::arg("self"), py::arg("mid")),
                doc_intro("check if all values in cell.env_ts for selected calculation-filter is non-nan, i.e. valid numbers")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "bool", "true if ok, false if not ok")
            )
            
            .def("is_calculated",&cm::is_calculated,
                (py::arg("self"), py::arg("mid"), py::arg("catchment_id")),
                doc_intro("using the catchment_calculation_filter to decide if discharge etc. are calculated.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("cid", "int", "catchment id")
                doc_returns("", "bool", "true if catchment id is calculated during runs")
            )
        ;
        
    }
    
    /** @brief a function to expose a server
     * 
     * 
     * This class simply expose the methods of the above py_server
     * 
     */
    
    void expose_server() {
        
        using srv=py_server;
            py::class_<srv, boost::noncopyable >("DrmServer",
                doc_intro(""),
                py::init<>((py::arg("self")),
                        doc_intro("Creates a server object .")
                )
            )
            .def("set_listening_port", &srv::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("set_listening_ip", &srv::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None","")
            )
            .def("start_server",&srv::start_server,(py::arg("self")),
                doc_intro("start server listening in background, and processing messages")
                doc_see_also("set_listening_port(port_no),is_running")
                doc_returns("port_no","in","the port used for listening operations, either the value as by set_listening_port, or if it was unspecified, a new available port")
            )
            .def("set_max_connections",&srv::set_max_connections,(py::arg("self"),py::arg("max_connect")),
                doc_intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
                doc_parameters()
                doc_parameter("max_connect","int","maximum number of connections before denying more connections")
                doc_see_also("get_max_connections()")
            )
            .def("get_max_connections",&srv::get_max_connections, (py::arg("self")),
                doc_intro("returns the maximum number of connections to be served concurrently"))
            .def("stop_server",&srv::stop_server, (py::arg("self")),
                doc_intro("stop serving connections, gracefully.")
                doc_see_also("start_server()")
            )
            .def("is_running",&srv::is_running, (py::arg("self")),
                doc_intro("true if server is listening and running")
                doc_see_also("start_server()")
            )
            .def("get_listening_port",&srv::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            ;
    }
    void drms() {
        expose_common();
        expose_client();
        expose_server();
    }
}
