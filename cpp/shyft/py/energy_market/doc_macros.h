#pragma once
//
// Python google-style multi-line docstring macro helpers.
//
#define doc_intro(intro) intro  "\n"
#define doc_details(dt) "\n"  dt  "\n"
#define doc_attributes() "\nAttributes:"
#define doc_attribute(name_str,type_str,descr_str) "\n    " name_str  " ( "  type_str  "):  "  descr_str  "\n"
#define doc_parameters() "\nArgs:"
#define doc_parameter(name_str,type_str,descr_str) "\n    " name_str  " ( "  type_str  "):  "  descr_str  "\n"
#define doc_paramcont(doc_str) "    "  doc_str  "\n"
#define doc_returns(name_str,type_str,descr_str) "\nReturns:\n    "  type_str  ": " name_str "." descr_str "\n"
#define doc_notes() "\nNotes:"
#define doc_note(note_str) "\n    " note_str  "\n"
#define doc_see_also(ref) "\n.. _see also:\n    " ref  "\n"
#define doc_reference(ref_name,ref) "\n.. _" ref_name ":\n    " ref "\n"
#define doc_ind(doc_str) "    "  doc_str
#define doc_raises() "\nRaises:"
#define doc_raise(type_str,descr_str) "\n    " type_str ": " descr_str  
#define doc_retcont(descr_str)  descr_str "\n"
