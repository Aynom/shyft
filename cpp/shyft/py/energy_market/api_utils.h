#pragma once
#if defined(_WINDOWS)
#pragma warning (disable : 4267)
#pragma warning (disable : 4244)
#pragma warning (disable : 4503)
#if !defined(HAVE_SNPRINTF)
// Python include (via Python.h to pyerrors.h) define macro snprintf to _snprintf on Windows,
// but then Boost (filesystem) gets into trouble; '_snprintf': is not a member of 'std'.
#include <cstdio>
#define HAVE_SNPRINTF
using std::snprintf;
#endif
#endif


#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/args.hpp>
#include <boost/python/class.hpp>
#include <boost/python/scope.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/return_internal_reference.hpp>
#include <boost/python/return_arg.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/handle.hpp>

#include <boost/python/tuple.hpp>
#include <boost/python/enum.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/operators.hpp>

#include <ostream>
#include <sstream>

#include <shyft/py/energy_market/doc_macros.h>


namespace expose{
    namespace py = boost::python;
    
    template <class T>
    struct py_object_ext{
        static py::object get_obj(T const& o){
            auto pyo = static_cast<py::object*>(o.h.obj);
            if(pyo){
                return *pyo;
            }else{
                return py::object{};
            }
        }
        
        static void set_obj(T &o, py::object& h){
            auto pyo = static_cast<py::object*>(o.h.obj);
            if(pyo){
                *pyo = h;
            }else{
                pyo = new py::object(h);
                o.h.obj = static_cast<void *>(pyo);
            }
        }
    };
    
}
