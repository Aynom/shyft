#include <string>
#include <memory>
#include <vector>
#include <shyft/py/energy_market/api_utils.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/api/py_convertible.h>
#include <boost/python/docstring_options.hpp>
#include <shyft/time/utctime_utilities.h>

#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market_ds.h>
#include <shyft/energy_market/srv/db.h>

namespace py=boost::python;
using std::shared_ptr;
using std::string;

namespace expose {

using namespace shyft::energy_market;
using std::string;
using std::to_string;
using std::make_shared;

using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::utcperiod;
using shyft::core::calendar;
using shyft::time_axis::generic_dt;
using shyft::time_series::dd::apoint_ts;

///
//  SECTION illustrating one simple way of generating python str and repr functions
//
template <class PT> // as in proxy_attr
string oid_attr_str(PT const& a,const char *type_name,const char*attr_name) {
    return string(type_name)+"("+to_string(a.o->id) + ",'"+a.o->name+"')."+string(attr_name)+": ";
}


template <class PT> // as in proxy_attr
string py_simple_str(PT const&a,const string& prefix) {
    if(!a.exists()) 
        return prefix+string("Empty");
    return prefix+to_string(a.get());
}

string py_string_of_value_type(const stm::t_double_::element_type& m) {
    string r("{");
    calendar utc;
    for(const auto&i:m) {
        r+= "\n\t"+utc.to_string(i.first) + ": "+to_string(i.second);
    }
    return r+(r.size()>1?"\n}":"}");
}

string py_string_of_value_type(const stm::t_xy_::element_type& m) {
    string r("{");
    calendar utc;
    for(const auto&i:m) {
        r+= "\n\t"+utc.to_string(i.first) + ": ";
        if(i.second->points.size()>0) {
            r+="[";
            for(const auto&p:i.second->points)
                r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
            r+="],";
        } else {
            r+="[],";
        }
    }
    return r+(r.size()>1?"\n}":"}");
}

string py_string_of_value_type(const stm::t_xyz_::element_type& m) {
    string r("{");
    calendar utc;
    for(const auto&i:m) {
        r+= "\n\t"+utc.to_string(i.first) + ": z"+to_string(i.second->z);
        if(i.second->xy_curve.points.size()>0) {
            r+="[";
            for(const auto&p:i.second->xy_curve.points)
                r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
            r+="],";
        } else {
            r+="[],";
        }
    }
    return r+(r.size()>1?"\n}":"}");
}

string py_string_of_value_type(const stm::t_xyz_list_::element_type& m) {
    string r("{");
    calendar utc;
    for(const auto&i:m) {
        r += "\n\t" + utc.to_string(i.first) + ": {";
        auto s = r.size();
        for(const auto&j:*i.second) {
            r += "\n\t\tz@"+to_string(j.z) + ": ";
            if (j.xy_curve.points.size()>0) {
                r += "[";
                for(const auto&p:j.xy_curve.points)
                    r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                r += "],";
            } else {
                r += "[],";
            }
        }
        r += r.size() > s ? "\n\t}" : "}";
    }
    return r+(r.size()>1?"\n}":"}");
}

string py_string_of_value_type(const stm::t_turbine_description_::element_type& m) {
    string r("{");
    calendar utc;
    for (const auto& i : m) {
        r += "\n\t" + utc.to_string(i.first) + ": {";
        auto s = r.size();
        if(i.second->efficiencies.size()>1) {
            for (int j=0;j<i.second->efficiencies.size();++j) {
                const auto& eff = i.second->efficiencies[j];
                r += "\n\t\t" + to_string(j+1) + ": {";
                r += "\n\t\t\tmin@" + to_string(eff.production_min) + ",";
                r += "\n\t\t\tmax@" + to_string(eff.production_max) + ",";
                for(const auto&ec:eff.efficiency_curves) {
                    r += "\n\t\t\tz@"+to_string(ec.z) + ": ";
                    if (ec.xy_curve.points.size()>0) {
                        r += "[";
                        for(const auto&p:ec.xy_curve.points)
                            r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                        r += "],";
                    } else {
                        r += "[],";
                    }
                }
                r += "\n\t\t},";
            }
        } else if(i.second->efficiencies.size()>0) {
            const auto& eff = i.second->efficiencies.front();
            for(const auto&ec:eff.efficiency_curves) {
                r += "\n\t\tz@"+to_string(ec.z) + ": ";
                if (ec.xy_curve.points.size()>0) {
                    r += "[";
                    for(const auto&p:ec.xy_curve.points)
                        r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                    r += "],";
                } else {
                    r += "[],";
                }
            }
        }
        r += r.size() > s ? "\n\t}" : "}";
    }
    return r + (r.size() > 1 ? "\n}" : "}");
}

// ** map shared_pointers to references.
template <class T>
string py_string_of_value_type(const shared_ptr<T>&m) {
    return py_string_of_value_type(*m);
}

string py_string_of_value_type(const apoint_ts& ts) {
    return ts.id().size()?string("TimeSeries('"+ts.id()+"')"):string("TimeSeries(with-values-and-time-axis)");
}

template <class PT> // as in proxy_attr
string py_default_complex_str(PT const &a, const string& prefix) {
    if(!a.exists()) 
        return prefix+string("Empty");
    return prefix + py_string_of_value_type(a.get());
}

string py_str(decltype(stm::reservoir::lrl) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "lowest regulated water level [masl]")); }
string py_str(decltype(stm::reservoir::hrl) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "highest regulated water level [masl]")); }
string py_str(decltype(stm::reservoir::volume_max_static) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "static maximum volume [Mm3]")); }
string py_str(decltype(stm::reservoir::volume_descr) const& a) {return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "volume description curve (t,masl)[m3/s]"));}
string py_str(decltype(stm::reservoir::spill_descr) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "spill description curve (t,masl)[Mm3]")); }
string py_str(decltype(stm::reservoir::endpoint_desc) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "endpoint description [Currency/MWh]")); }
string py_str(decltype(stm::reservoir::inflow) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "inflow [m3/s]")); }
string py_str(decltype(stm::reservoir::level_historic) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "historical level [masl]")); }
string py_str(decltype(stm::reservoir::level_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "planned level [masl]")); }
string py_str(decltype(stm::reservoir::level_min) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "min level restriction [masl]")); }
string py_str(decltype(stm::reservoir::level_max) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "max level restriction [masl]")); }
string py_str(decltype(stm::reservoir::ramping_level_down_d)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "max ramping level down restriction [masl/d]")); }
string py_str(decltype(stm::reservoir::ramping_level_down_h)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "max ramping level down restriction [masl/h]")); }
string py_str(decltype(stm::reservoir::ramping_level_up_d)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "max ramping level up restriction [masl/d]")); }
string py_str(decltype(stm::reservoir::ramping_level_up_h)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "max ramping level up restriction [masl/h]")); }
string py_str(decltype(stm::reservoir::volume)const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "volume [Mm3]")); }
string py_str(decltype(stm::reservoir::level) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Reservoir", "level [masl]")); }

string py_str(decltype(stm::unit::generator_efficiency) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "generator efficiency curve")); }
string py_str(decltype(stm::unit::turbine_description) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "turbine description")); }
string py_str(decltype(stm::unit::production_min_static) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "static minimum production [MW]")); }
string py_str(decltype(stm::unit::production_max_static) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "static maximum production [MW]")); }
string py_str(decltype(stm::unit::unavailability)const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "Unavailability [0/1(unavailable)]")); }
string py_str(decltype(stm::unit::production_min)const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "minimum production restriction [MW]")); }
string py_str(decltype(stm::unit::production_max)const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "maximum production restriction [MW]")); }
string py_str(decltype(stm::unit::production_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "schedule production [MW]")); }
string py_str(decltype(stm::unit::discharge_min) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "minimum discharge [m3/s]")); }
string py_str(decltype(stm::unit::discharge_max) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "maximum discharge [m3/s]")); }
string py_str(decltype(stm::unit::discharge_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "schedule discharge [m3/s]")); }
string py_str(decltype(stm::unit::production) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "production [MW]")); }
string py_str(decltype(stm::unit::discharge) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "discharge [m3/s]")); }
string py_str(decltype(stm::unit::cost_stop)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "stop cost [currency]")); }
string py_str(decltype(stm::unit::cost_start)const&  a) { return py_default_complex_str(a, oid_attr_str(a, "Aggregate", "start cost [currency]")); }

string py_str(decltype(stm::power_plant::outlet_level) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "outlet level [masl]")); }
string py_str(decltype(stm::power_plant::mip) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "Mip [0/1]")); }
string py_str(decltype(stm::power_plant::unavailability) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "Unavailability [0/1(unavailable)]")); }
string py_str(decltype(stm::power_plant::production_min) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "minimum production restriction [MW]")); }
string py_str(decltype(stm::power_plant::production_max) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "maximum production restriction [MW]")); }
string py_str(decltype(stm::power_plant::production_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "schedule [MW]")); }
string py_str(decltype(stm::power_plant::discharge_min) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "minimum discharge [m3/s]")); }
string py_str(decltype(stm::power_plant::discharge_max) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "maximum discharge [m3/s]")); }
string py_str(decltype(stm::power_plant::discharge_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "PowerStation", "schedule [m3/s]")); }

string py_str(decltype(stm::waterway::discharge_max_static) const& a) { return py_default_complex_str(a, oid_attr_str(a, "WaterRoute", "static maximum discharge [m3/s]")); }
string py_str(decltype(stm::waterway::head_loss_coeff) const& a) { return py_default_complex_str(a, oid_attr_str(a, "WaterRoute", "head loss factor")); }
string py_str(decltype(stm::waterway::head_loss_func) const& a) { return py_default_complex_str(a, oid_attr_str(a, "WaterRoute", "head loss function")); }
string py_str(decltype(stm::waterway::discharge) const& a) { return py_default_complex_str(a, oid_attr_str(a, "WaterRoute", "discharge [m3/s]")); }

string py_str(decltype(stm::gate::opening_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Gate", "this is the gate opening schedule, range 0..1")); }
string py_str(decltype(stm::gate::discharge_schedule) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Gate", "this is the gate flow schedule in m3/s units")); }
string py_str(decltype(stm::gate::discharge) const& a) { return py_default_complex_str(a, oid_attr_str(a, "Gate", "discharge [m3/s]")); }

string py_str(decltype(stm::energy_market_area::load)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "load requirement in MW")); }
string py_str(decltype(stm::energy_market_area::price)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "price EUR/MW")); }
string py_str(decltype(stm::energy_market_area::max_buy)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "max buy MW")); }
string py_str(decltype(stm::energy_market_area::max_sale)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "max sale MW")); }
string py_str(decltype(stm::energy_market_area::buy)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "buy MW")); }
string py_str(decltype(stm::energy_market_area::sale)const& a) { return py_default_complex_str(a, oid_attr_str(a, "MarketArea", "sale MW")); }

template <class T>
void py_attr(const char *py_cls) {
    using py_a=T;//core::proxy_attr<T,V,A,a>;
    string (*str_f)(py_a const&)=&py_str;

    py::class_<py_a>(py_cls,
        doc_intro("Attribute of domain object.")
        doc_details(
            "This object represent an *attribute*, like 'volume_max_static',\n"
            "of a specific *value type*, like 'float' or time-dependent float,\n" 
            "that is an optional part of a *domain object*, like Reservoir, Waterway,PowerPlant, Unit.\n\n"
            "The class provides properties to check for existence, set or get the value,\n"
            "as well as remove a value, so that .exists property returns False\n"
            "Attribute can be compared for value equality using:\n"
            ">>> a==b\n"
            ">>> a!=b")
        doc_notes()
        doc_note("This class is only provided as part of domain-objects")
        ,py::no_init)
        .add_property("exists",&py_a::exists,
            doc_intro("Check if attribute is available/filled in.")
            doc_returns("attribute_exists", "bool", "True if the attribute is available/filled in.")
        )
        .add_property("value",&py_a::get,&py_a::set,
            doc_intro("Access value to get or set it.")
            doc_raises()
            doc_raise("runtime_error", "If .exists == False when you try to read it.")
        )
        .def("remove",&py_a::remove,(py::arg("self")),
            doc_intro("Remove the attribute.")
            doc_details("After calling this the .exists returns False.")
            doc_returns("removed_item","bool","True if removed.")
            doc_retcont("False if it was already away when invoking the method.")
        )
        .def("__str__",str_f,
             "Provide easy to read string representation of the object."
        )
        .def("__repr__",str_f,
             "Provide easy to read string representation of the object."
        )
        .def(py::self == py::self) //when value type supports equality..
        .def(py::self != py::self)
    ;
};

template <class T>
void py_t_value(const char *tp_name) {
    string (*str_f)(typename T::element_type const&)=&py_string_of_value_type;
    py::class_<typename T::element_type,py::bases<>,T>(tp_name,
        doc_intro("Time variable value-type.")
        doc_details(
            "Implemented as a sorted map of tuple (time,value) items.\n"
            "\n"
            "You can assign/replace a new item using:\n"
            ">>> m = t_double_() # create a time-dependent map-type\n"
            ">>> m[time('2018-01-01T00:00:00Z')] = 3.23\n"
            "\n"
            "And iterate over the tuple like this:\n"
            ">>> for i in m:\n"
            ">>>    print(i.key(),i.data())\n"
            "\n"
            "To make a copy of a time-dependent variable, use:\n"
            ">>> m_clone=t_double_(m) # pass in the object to clone in the constructor")
        )
        .def(py::map_indexing_suite<typename T::element_type,true>())
        .def(py::init<const typename T::element_type&>((py::arg("clone")),"create a copy of the object to clone"))
        .def("__str__",str_f)
        .def("__repr__",str_f)
    ;
    // this is what we want:
    // user can create a t_value as t_value({})
    //py_api::iterable_converter().from_python<T>();
}

void create_ids(const shared_ptr<stm::stm_hps>& hps) {
    hps->ids= make_shared<stm::hps_ds>();
}

using shyft::energy_market::stm::stm_hps_;
using shyft::energy_market::stm::reservoir_;
using shyft::energy_market::stm::unit_;
using shyft::energy_market::stm::power_plant_;
using shyft::energy_market::stm::waterway_;
using shyft::energy_market::stm::catchment_;
using shyft::energy_market::stm::stm_hps_builder;
using shyft::energy_market::stm::energy_market_area;
using shyft::energy_market::stm::stm_system_;
using shyft::energy_market::stm::gate_;

/** extensions to ease py expose */
struct hps_ext {
    static std::vector<char> to_blob(const  stm_hps_& m) {
        auto s=shyft::energy_market::stm::stm_hps::to_blob(m);
        return std::vector<char>(s.begin(),s.end());
    }
    static stm_hps_ from_blob(std::vector<char>&blob) {
        std::string s(blob.begin(),blob.end());
        return shyft::energy_market::stm::stm_hps::from_blob(s);
    }
    // wrap all create calls via the stm_hps_builder to enforce build-rules
    static catchment_ create_catchment(stm_hps_&s,int id,const string&name,const string &json) {return stm_hps_builder(s).create_catchment(id,name,json);}
    static reservoir_ create_reservoir(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_reservoir(id,name,json);}
    static unit_ create_unit(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_unit(id,name,json);}
    static waterway_ create_waterway(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_waterway(id,name,json);}
    static power_plant_ create_power_plant(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_power_station(id,name,json);}
    static waterway_ create_tunnel(stm_hps_&s,int id,const string&name,const string &json) {return create_waterway(s,id,name,json);}
    static waterway_ create_river(stm_hps_&s,int id,const string&name,const string &json) {return create_waterway(s,id,name,json);}
};

/** extensions to ease py expose */
struct stm_sys_ext {
    static std::vector<char> to_blob(const  stm_system_& m) {
        auto s=shyft::energy_market::stm::stm_system::to_blob(m);
        return std::vector<char>(s.begin(),s.end());
    }
    static stm_system_ from_blob(std::vector<char>&blob) {
        std::string s(blob.begin(),blob.end());
        return shyft::energy_market::stm::stm_system::from_blob(s);
    }
};

#if 0
//TODO: Later
using shyft::energy_market::stm::stm_rule_exception;
void rule_translator(const stm_rule_exception&e) {
    PyErr_SetString(PyExc_UserWarning,e.what());
}
#endif

void stm() {
    //TODO: Later py::register_exception_translator<stm_rule_exception>(rule_translator);
    
    py::class_<
        stm::stm_hps,
        py::bases<hydro_power::hydro_power_system>,
        shared_ptr<stm::stm_hps>,
        boost::noncopyable
    >("HydroPowerSystem",
        doc_intro("A hydro power system, with indataset.")
        doc_details(
            "The hydro power system consists of reservoirs, waterway (river/tunnel)\n"
            "and units. In addition, the power plant has the role of keeping\n"
            "related units together into a group that resembles what most people\n"
            "would think is a power plant in this context. The power plant has just\n"
            "references to the units (generator/turbine parts), but can keep\n"
            "sum-requirements/schedules and computations valid at power plant level.")
        ,py::no_init)
        .def(py::init<int, string>((py::arg("uid"), py::arg("name")),
            "Create hydro power system with unique uid.")
        )
        .def("create_ids",create_ids,(py::arg("self")),
            "Create an initial empty indataset."
        )
        .def("create_reservoir", &hps_ext::create_reservoir,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm reservoir with unique uid.")
            doc_returns("reservoir","Reservoir","The new reservoir.")
        )
        .def("create_unit", &hps_ext::create_unit,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm unit.")
            doc_returns("unit","Unit","The new unit.")
        )
        .def("create_power_plant", &hps_ext::create_power_plant,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm power plant that keeps units.")
            doc_returns("power_plant","PowerPlant","The new PowerPlant.")
        )
        .def("create_waterway", &hps_ext::create_waterway,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm waterway (a tunnel or river).")
            doc_returns("waterway","Waterway","The new waterway.")
        )
        .def("create_tunnel", &hps_ext::create_waterway,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm waterway (a tunnel or river).")
            doc_returns("waterway","Waterway","The new waterway.")
        )
        .def("create_river", &hps_ext::create_waterway,
            (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
            doc_intro("Create stm waterway (a tunnel or river).")
            doc_returns("waterway","Waterway","The new waterway.")
        )
        .def("to_blob",&hps_ext::to_blob,(py::arg("self")),
            doc_intro("Serialize the model to a blob.")
            doc_returns("blob","ByteVector","Blob form of the model.")
        )
        .def("from_blob",&hps_ext::from_blob,(py::arg("blob")),
            doc_intro("Re-create a stm hps from a previously create blob.")
            doc_returns("hps","HydroPowerSystem","A stm hydro-power-system including it's attributes in the ids.")
        ).staticmethod("from_blob")
    ;

    typedef std::vector<stm::stm_hps_> HpsList;
    py::class_<HpsList>("HydroPowerSystemList", "A strongly typed list of HydroPowerSystem.")
        .def(py::vector_indexing_suite<HpsList, true>())
    ;

    py::class_<
        stm::reservoir,
        py::bases<hydro_power::reservoir>,
        shared_ptr<stm::reservoir>, 
        boost::noncopyable
    >("Reservoir","Stm reservoir.",py::no_init)
        .def(py::init<int, const string&,const string&, stm::stm_hps_ &>(
            (py::arg("uid"),py::arg("name"),py::arg("json"),py::arg("hps")),
            "Create reservoir with unique id and name for a hydro power system."))

        .def_readwrite("lrl", &stm::reservoir::lrl, "Lowest regulated water level [masl], time-dependent attribute.")
        .def_readwrite("hrl", &stm::reservoir::hrl, "Highest regulated water level [masl], time-dependent attribute.")
        .def_readwrite("volume_max_static", &stm::reservoir::volume_max_static, "Maximum regulated volume [Mm3], time-dependent attribute.")
        .def_readwrite("spill_descr",&stm::reservoir::spill_descr,"Spill description, time-dependent attribute.")
        .def_readwrite("volume_descr",&stm::reservoir::volume_descr,"Volume description, time-dependent attribute.")
        
        .def_readwrite("endpoint_desc", &stm::reservoir::endpoint_desc, "Endpoint description [currency/MWH], time-dependent attribute.")
        .def_readwrite("inflow", &stm::reservoir::inflow, "Inflow [m3/s], time series.")
        .def_readwrite("level_historic", &stm::reservoir::level_historic, "Historical level, time-dependent attribute.")
        .def_readwrite("level_schedule",&stm::reservoir::level_schedule,"Level schedule, time series.")
        .def_readwrite("level_min",&stm::reservoir::level_min,"Level minimum, time series.")
        .def_readwrite("level_max",&stm::reservoir::level_max,"Level maximum, time series.")
        .def_readwrite("ramping_level_down_d", &stm::reservoir::ramping_level_down_d, "Max level change down per day, time series.")
        .def_readwrite("ramping_level_down_h", &stm::reservoir::ramping_level_down_h, "Max level change down per hour, time series.")
        .def_readwrite("ramping_level_up_d", &stm::reservoir::ramping_level_up_d, "Max level change up per day, time series.")
        .def_readwrite("ramping_level_up_h", &stm::reservoir::ramping_level_up_h, "Max level change up per hour, time series.")

        .def_readwrite("volume", &stm::reservoir::volume, "Volume results, time series.")
        .def_readwrite("level", &stm::reservoir::level, "Level results, time series.")
    ;
    
    py::class_<
        stm::unit,
        py::bases<hydro_power::unit>,
        shared_ptr<stm::unit>,
        boost::noncopyable
    >("Unit", "Stm unit(turbine and generator assembly).", py::no_init)
        .def(
            py::init<int, const string&, const string&, stm::stm_hps_ &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create unit with unique id and name for a hydro power system."))

        .def_readwrite("production_min_static", &stm::unit::production_min_static, "Production minimum, time-dependent attribute.")
        .def_readwrite("production_max_static", &stm::unit::production_max_static, "Production maximum, time-dependent attribute.")
        .def_readwrite("generator_efficiency", &stm::unit::generator_efficiency, "Generator efficiency curve, time-dependent attribute.")
        .def_readwrite("turbine_description", &stm::unit::turbine_description, "Turbine description, time-dependent attribute.")

        .def_readwrite("unavailability", &stm::unit::unavailability, "Unavailability, time series.")
        .def_readwrite("production_min", &stm::unit::production_min, "Production minimum restriction, time series.")
        .def_readwrite("production_max", &stm::unit::production_max, "Production maximum restriction, time series.")
        .def_readwrite("production_schedule", &stm::unit::production_schedule, "Production schedule, time series.")
        .def_readwrite("discharge_min", &stm::unit::discharge_min, "Discharge minimum restriction, time series.")
        .def_readwrite("discharge_max", &stm::unit::discharge_max, "Discharge maximum restriction, time series.")
        .def_readwrite("discharge_schedule", &stm::unit::discharge_schedule, "Discharge schedule, time series.")
        .def_readwrite("cost_stop", &stm::unit::cost_stop, "Stop cost, time series.")
        .def_readwrite("cost_start", &stm::unit::cost_start, "Start cost, time series.")

        .def_readwrite("production", &stm::unit::production, "Production result, time series.")
        .def_readwrite("discharge", &stm::unit::discharge, "Discharge result, time series.")
    ;

    typedef std::vector<stm::unit_> UnitList;
    py::class_<UnitList>("UnitList", "A strongly typed list of Units.")
        .def(py::vector_indexing_suite<UnitList, true>())
    ;

    py::class_<
        stm::power_plant,
        py::bases<hydro_power::power_plant>,
        shared_ptr<stm::power_plant>,
        boost::noncopyable
    >("PowerPlant", 
        doc_intro("A power plant keeping stm units.")
        doc_details("After creating the units, create the power plant,\n"
                    "and add the units to the power plant.")
        , py::no_init)
        .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create power plant with unique id and name for a hydro power system."))

        .def_readwrite("outlet_level", &stm::power_plant::outlet_level, "Outlet level, time-dependent attribute.")

        .def_readwrite("mip", &stm::power_plant::mip, "Mip, time series.")
        .def_readwrite("unavailability", &stm::power_plant::unavailability, "Unavailability, time series.")
        .def_readwrite("production_min", &stm::power_plant::production_min, "Production minimum restriction, time series.")
        .def_readwrite("production_max", &stm::power_plant::production_max, "Production maximum restriction, time series.")
        .def_readwrite("production_schedule", &stm::power_plant::production_schedule, "Production schedule, time series.")
        .def_readwrite("discharge_min", &stm::power_plant::discharge_min, "Discharge minimum restriction, time series.")
        .def_readwrite("discharge_max", &stm::power_plant::discharge_max, "Discharge maximum restriction, time series.")
        .def_readwrite("discharge_schedule", &stm::power_plant::discharge_schedule, "Discharge schedule, time series.")

        .def_readwrite("production_schedule", &stm::power_plant::production_schedule, "Production schedule, time series.")
        .def_readwrite("discharge_schedule", &stm::power_plant::discharge_schedule, "Discharge schedule, time series.")
    ;

    py::class_<
        stm::waterway,
        py::bases<hydro_power::waterway>,
        shared_ptr<stm::waterway>,
        boost::noncopyable
    >("Waterway", "Stm waterway.", py::no_init)
        .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
            "Create waterway with unique id and name for a hydro power system."))

        .def_readwrite("discharge_max_static", &stm::waterway::discharge_max_static, "Maximum discharge, time-dependent attribute.")
        .def_readwrite("head_loss_coeff", &stm::waterway::head_loss_coeff, "Loss factor, time-dependent attribute.")
        .def_readwrite("head_loss_func", &stm::waterway::head_loss_func, "Loss function, time-dependent attribute.")
        .def_readwrite("discharge", &stm::waterway::discharge, "Discharge result, time series.")

//        .def("add_gate",&stm::water_route::add_gate,(py::arg("self"),py::arg("gate")),"Add gate to control the water flow.")
        .def("add_gate",&stm::waterway::add_gate,(py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")),
             "Create and add a new gate to the waterway.")//,py::return_value_policy<py::return_by_value>())
//        .def("remove_gate",&stm::water_route::remove_gate,(py::arg("self"),py::arg("gate")),"Remove an existing gate.")
    ;

    py::class_<
        stm::gate,
        py::bases<hydro_power::gate>,
        shared_ptr<stm::gate>,
        boost::noncopyable
    >("Gate", "Stm gate.", py::no_init)
        //.def(py::init<int, const string&, const string&>(
        //    (py::arg("uid"), py::arg("name"), py::arg("json")),
        //    "Create gate for a hydro power system, with unique id and name."))
        .def_readwrite("opening_schedule", &stm::gate::opening_schedule, "Opening schedule, value between 0.0 and 1.0, time series.")
        .def_readwrite("discharge_schedule", &stm::gate::discharge_schedule, "Discharge schedule restriction, time series.")
        .def_readwrite("discharge", &stm::gate::discharge, "Discharge result, time series.")
    ;

    py_attr<decltype(stm::reservoir::lrl)>("rsv_lrl");
    py_attr<decltype(stm::reservoir::hrl)>("rsv_hrl");
    py_attr<decltype(stm::reservoir::volume_max_static)>("rsv_volume_max_static");
    py_attr<decltype(stm::reservoir::volume_descr)>("rsv_volume_descr");
    py_attr<decltype(stm::reservoir::spill_descr)>("rsv_spill_descr");
    py_attr<decltype(stm::reservoir::endpoint_desc)>("rsv_endpoint_desc_currency_mwh");
    py_attr<decltype(stm::reservoir::inflow)>("rsv_inflow");
    py_attr<decltype(stm::reservoir::level_historic)>("rsv_level_historic");
    py_attr<decltype(stm::reservoir::level_schedule)>("rsv_level_schedule");
    py_attr<decltype(stm::reservoir::level_min)>("rsv_level_min");
    py_attr<decltype(stm::reservoir::level_max)>("rsv_level_max");
    py_attr<decltype(stm::reservoir::ramping_level_down_d)>("rsv_ramping_level_down_d");
    py_attr<decltype(stm::reservoir::ramping_level_down_h)>("rsv_ramping_level_down_h");
    py_attr<decltype(stm::reservoir::ramping_level_up_d)>("rsv_ramping_level_up_d");
    py_attr<decltype(stm::reservoir::ramping_level_up_h)>("rsv_ramping_level_up_h");
    py_attr<decltype(stm::reservoir::volume)>("rsv_volume");
    py_attr<decltype(stm::reservoir::level)>("rsv_level");

    py_attr<decltype(stm::unit::production_min_static)>("agg_pmin_static");
    py_attr<decltype(stm::unit::production_max_static)>("agg_pmax_static");
    py_attr<decltype(stm::unit::generator_efficiency)>("agg_generator_efficiency");
    py_attr<decltype(stm::unit::turbine_description)>("agg_turbine_description");
    py_attr<decltype(stm::unit::unavailability)>("agg_unavailability");
    py_attr<decltype(stm::unit::production_min)>("agg_production_min");
    py_attr<decltype(stm::unit::production_max)>("agg_production_max");
    py_attr<decltype(stm::unit::production_schedule)>("agg_production_schedule");
    py_attr<decltype(stm::unit::discharge_min)>("agg_discharge_min");
    py_attr<decltype(stm::unit::discharge_max)>("agg_discharge_max");
    py_attr<decltype(stm::unit::discharge_schedule)>("agg_discharge_schedule");
    py_attr<decltype(stm::unit::cost_stop)>("agg_cost_stop");
    py_attr<decltype(stm::unit::cost_start)>("agg_cost_start");

    py_attr<decltype(stm::unit::production)>("agg_production");
    py_attr<decltype(stm::unit::discharge)>("agg_discharge");

    py_attr<decltype(stm::power_plant::outlet_level)>("pwr_stn_outlet_level");
    py_attr<decltype(stm::power_plant::mip)>("pwr_mip");
    py_attr<decltype(stm::power_plant::unavailability)>("pwr_stn_unavailability");
    py_attr<decltype(stm::power_plant::production_min)>("pwr_stn_production_min");
    py_attr<decltype(stm::power_plant::production_max)>("pwr_stn_production_max");
    py_attr<decltype(stm::power_plant::production_schedule)>("pwr_stn_production_schedule");
    py_attr<decltype(stm::power_plant::discharge_min)>("pwr_stn_discharge_min");
    py_attr<decltype(stm::power_plant::discharge_max)>("pwr_stn_discharge_max");
    py_attr<decltype(stm::power_plant::discharge_schedule)>("pwr_stn_discharge_schedule");

    py_attr<decltype(stm::waterway::discharge_max_static)>("wtr_qmax_static");
    py_attr<decltype(stm::waterway::head_loss_coeff)>("wtr_head_loss_coeff");
    py_attr<decltype(stm::waterway::head_loss_func)>("wtr_head_loss_func");
    py_attr<decltype(stm::waterway::discharge)>("wtr_discharge");

    py_attr<decltype(stm::gate::opening_schedule)>("gate_opening_schedule");
    py_attr<decltype(stm::gate::discharge_schedule)>("gate_discharge_schedule");
    py_attr<decltype(stm::gate::discharge)>("gate_discharge");

    py_t_value<stm::t_double_>("t_double");
    py_t_value<stm::t_xy_>("t_xy");
    py_t_value<stm::t_xyz_>("t_xyz");
    py_t_value<stm::t_xyz_list_>("t_xyz_list");
    py_t_value<stm::t_turbine_description_>("t_turbine_description");
}


/**Expose stm system, that contains market(with its dataset), 
 *as well as a number of stm hydro power-systems
 */
void stm_system() {
    py::class_<
        stm::energy_market_area,
        py::bases<>,
        shared_ptr<stm::energy_market_area>,
        boost::noncopyable
    >("MarketArea", 
        doc_intro("A market area, with load/price and other properties.")
        doc_details("Within the market area, there are zero or more energy-\n"
                    "producing/consuming units.")
        , py::no_init)
        .def(py::init<int, const string&, const string&, stm::stm_system_ &>(
            (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("stm_sys")),
            "Create market area for a stm system."))
        .def_readwrite("id",&stm::energy_market_area::id,"Unique id for this object.")
        .def_readwrite("name",&stm::energy_market_area::name,"Name for this object.")
        .def_readwrite("json",&stm::energy_market_area::json,"Json keeping any extra data for this object.")
        .def_readwrite("load", &stm::energy_market_area::load, "Load, time-series.")
        .def_readwrite("price", &stm::energy_market_area::price, "Price, time series.")
        .def_readwrite("max_buy", &stm::energy_market_area::max_buy, "Maximum buy, time series.")
        .def_readwrite("max_sale", &stm::energy_market_area::max_sale, "Maximum sale, time series.")
        .def_readwrite("buy_mw", &stm::energy_market_area::buy, "Buy result, time-series.")
        .def_readwrite("sale_mw", &stm::energy_market_area::sale, "Sale result, time series.")
    ;

    typedef std::vector<stm::energy_market_area_> MarketAreaList;
    py::class_<MarketAreaList>("MarketAreaList", "A strongly typed list of MarketArea.")
        .def(py::vector_indexing_suite<MarketAreaList, true>())
    ;

    py_attr<decltype(stm::energy_market_area::load)>("ema_load_mw");
    py_attr<decltype(stm::energy_market_area::price)>("ema_price_eur_mw");
    py_attr<decltype(stm::energy_market_area::max_buy)>("ema_max_buy_mw");
    py_attr<decltype(stm::energy_market_area::max_sale)>("ema_max_sale_mw");
    py_attr<decltype(stm::energy_market_area::buy)>("ema_buy_mw");
    py_attr<decltype(stm::energy_market_area::sale)>("ema_sale_mw");

    py::class_<
        stm::stm_system,
        py::bases<>,
        shared_ptr<stm::stm_system>,
        boost::noncopyable
    >("StmSystem", "A complete stm system, with market areas, and hydro power systems.", py::no_init)
        .def(py::init<int, const string&, const string&>(
            (py::arg("uid"), py::arg("name"), py::arg("json")), "Create stm system."))
        .def_readwrite("id",&stm::stm_system::id,"Unique id for this object.")
        .def_readwrite("name",&stm::stm_system::name,"Name for this object.")
        .def_readwrite("json",&stm::stm_system::json,"Json keeping any extra data for this object.")
        .def_readonly("market_areas",&stm::stm_system::market,"List of market areas.")
        .def_readonly("hydro_power_systems",&stm::stm_system::hps,"List of hydro power systems.")
        .def("to_blob",&stm_sys_ext::to_blob,(py::arg("self")),
             doc_intro("Serialize the model to a blob.")
             doc_returns("blob","ByteVector","Blob form of the model.")
        )
        .def("from_blob",&stm_sys_ext::from_blob,(py::arg("blob")),
             doc_intro("Re-create a stm system from a previously create blob.")
             doc_returns("stm_sys","StmSystem","A stm system including hydro-power-systems and markets.")
        ).staticmethod("from_blob")
    ;
#if 0
    using StmSystemList=std::vector<shared_ptr<stm::stm_system>>;
    py::class_<StmSystemList>("StmSystemList")
        .def(py::vector_indexing_suite<StmSystemList, true>())
    ;

    py::class_<
        stm::shop::shop_system,
        py::bases<>,
        shared_ptr<stm::shop::shop_system>,
        boost::noncopyable
    >("ShopSystem", "A shop system, managing a session to the shop core.", py::no_init)
        .def(py::init<utcperiod, utctimespan>(
            (py::arg("time_period"), py::arg("time_step")),
            "Create shop system, initialize with fixed time resolution."))
        .def(py::init<const generic_dt&>(
            (py::arg("time_axis")),
            "Create shop system, initialize with time axis."))
        .def_readonly("commander",&stm::shop::shop_system::commander,
            doc_intro("Get shop commander object.")
            doc_details("Use it to send individual commands to the shop core."))
        .def("set_silent_mode",&stm::shop::shop_system::set_silent_mode,(py::arg("self"),
            py::arg("on")),
            doc_intro("Turn on and turn off messages from the shop core.")
            doc_details("By default messages from shop core will be shown on console,\n"
                        "as well as written to the main shop log file (shop_messages.log).\n"
                        "Turning silent mode on will suppress these messages, but\n"
                        "important messages from the shop api will still appear."))
        .def("emit",&stm::shop::shop_system::emit,(py::arg("self"),
            py::arg("stm_system")),
            "Emit a stm system into the shop core.")
        .def("command",&stm::shop::shop_system::command,(py::arg("self"),
            py::arg("commands")),
            "Send a set of commands to the shop core.")
        .def("collect",&stm::shop::shop_system::collect,(py::arg("self"),
            py::arg("stm_system")),
            doc_intro("Collect results from shop core into stm system.")
            doc_details("The stm system should be the one previously emitted."))
        .def<string(stm::shop::shop_system::*)()>("export_topology",&stm::shop::shop_system::export_topology,(py::arg("self")),
            doc_intro("Export emitted topology as a DOT formatted string.")
            doc_details("You can use Graphviz to generate picture file from it."))
        .def<string(stm::shop::shop_system::*)()>("export_data",&stm::shop::shop_system::export_data,(py::arg("self")),
            "Export emitted data objects and attributes as a JSON formatted string.")
        .def<void(*)(stm_system_,utctime,utctime,utctimespan,const std::vector<shyft::energy_market::stm::shop::shop_command>&)>("optimize",&stm::shop::shop_system::optimize,
            (py::arg("stm_system"),py::arg("time_begin"),py::arg("time_end"),py::arg("time_step"),py::arg("commands")),
            doc_intro("Run one-step optimization with fixed time resolution.")
            doc_details("This will initialize a shop system with fixed time resolution,\n"
                        "emit stm system, send commands and collect results."))//.staticmethod("optimize")
        .def<void(*)(stm_system_,const generic_dt&,const std::vector<shyft::energy_market::stm::shop::shop_command>&)>("optimize", &stm::shop::shop_system::optimize,
            (py::arg("stm_system"),py::arg("time_axis"),py::arg("commands")),
            doc_intro("Run one-step optimization with time axis.")
            doc_details("This will initialize a shop system with time axis,\n"
                        "emit stm system, send commands and collect results.")).staticmethod("optimize")
    ;

    // Load Python exposure of shop_command and shop_commander classes from separate headers
    shyft::py::energy_market::stm::expose_shop_command();
    shyft::py::energy_market::stm::expose_shop_commander();
#endif
}

void hps_client_server() {
    using model=shyft::energy_market::stm::stm_hps;

    shyft::py::energy_market::expose_client<model>("HpsClient",
       "The client api for the hydro-power-system repostory server."
    );
    shyft::py::energy_market::expose_server<shyft::energy_market::srv::db<model>>("HpsServer",
        "The server-side component for the hydro-power-system model repository."
    );
}
void stm_client_server() {
    using model=shyft::energy_market::stm::stm_system;

    shyft::py::energy_market::expose_client<model>("StmClient",
        "The client api for the stm repostory server."
    );
    shyft::py::energy_market::expose_server<shyft::energy_market::srv::db<model>>("StmServer",
        "The server-side component for the stm energy_market model repository."
    );

}

}

BOOST_PYTHON_MODULE(_stm) {
    py::scope().attr("__doc__") = "Statkraft Energy Market short term model";
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    expose::stm();
    expose::stm_system();
    expose::hps_client_server();
    expose::stm_client_server();
}
