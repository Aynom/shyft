#include <shyft/py/energy_market/api_utils.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/energy_market/time_series_util.h>

// TODO: relocate compress stuff to time-series core

namespace expose {

static size_t compressed_size_float(std::vector<float> const& v,float accuracy) {
    return shyft::time_series::ts_compress_size(v, accuracy);
}

static size_t compressed_size_double(std::vector<double> const& v, double accuracy) {
    return shyft::time_series::ts_compress_size(v, accuracy);
}
#if 0
static shyft::energy_market::core::time_series_data<float> ts_compress_float(shyft::energy_market::core::time_series_data<float> const &ts, float const accuracy) {
    return shyft::energy_market::core::ts_compress(ts, accuracy);
}

static shyft::energy_market::core::time_series_data<double> ts_compress_double(shyft::energy_market::core::time_series_data<double> const &ts, double const accuracy) {
    return shyft::energy_market::core::ts_compress(ts, accuracy);
}

static shyft::energy_market::core::time_series_data<float> ts_decompress_float(shyft::energy_market::core::time_series_data<float> const &ts, shyft::energy_market::core::utctimespan dt) {
    return shyft::energy_market::core::ts_decompress(ts, dt);
}

static shyft::energy_market::core::time_series_data<double> ts_decompress_double(shyft::energy_market::core::time_series_data<double> const &ts, shyft::energy_market::core::utctimespan dt) {
    return shyft::energy_market::core::ts_decompress(ts, dt);
}
#endif
void e_utcperiod() {
#if 0
    using namespace boost::python;
    using namespace shyft::energy_market::core;
    typedef utcperiod UtcPeriod;
    class_<UtcPeriod>("UtcPeriod", "defined time period by [start..end> ")
        .def(init<utctime, utctime>(args("start", "end"), "construct a period"))
        .def_readonly("start", &UtcPeriod::start)
        .def_readonly("end", &UtcPeriod::end)
        .def("is_valid", &UtcPeriod::valid)
        .def("duration", &UtcPeriod::timespan, "returns end-start")
        .def(self == self)
        .def(self != self)
        ;
    def("deltahours", &shyft::energy_market::core::deltahours, "n_hours", "returns n_hours expressed as seconds");
#endif
}
#if 0
void e_time_axis() {
    using namespace boost::python;
    using namespace shyft::energy_market::core;
    typedef time_axis::generic TimeAxis;

    class_<TimeAxis>("TimeAxis", "doc tbd")
        .def(init<utctime,utctimespan,size_t>(args("start","dt","n"),""))
        .def(init<std::vector<utctime> const&>(args("time_points"), ""))
        .def("size",&TimeAxis::size,"number of periods")
        .def("period",&TimeAxis::period,args("i"),"returns i't period")
        .def_readonly("time_points",&TimeAxis::t,"if not is_fixed, t contains n+1 points, the n't points is the end of the last period")
        .def_readwrite("start",&TimeAxis::start, "if is_fixed, the start of time-axis")
        .def_readwrite("delta_t",&TimeAxis::delta_t,"if is_fixed, delta t of each period")
        .def_readwrite("n",&TimeAxis::n,"if is_fixed, number of periods")
        .add_property("is_fixed",&TimeAxis::is_fixed,"True if this is a fixed interval time-axis")
        .def(self == self)
        .def(self != self)
    ;
}

template <class T>
void e_ts_data(char const*cls_name) {
    using namespace boost::python;
    using namespace shyft::energy_market::core;
    typedef time_series_data<T> TsData;
    class_<TsData>(cls_name, "")
        .def(init<utctime,utctimespan,size_t>(args("start","dt","n"), "tbd"))
        .def(init<utctime,utctimespan,std::vector<T>const&>(args("start","dt","values"), "tbd"))
        .def(init<std::vector<utctime>const&,std::vector<T>const&>(args("time_points","values"), "tbd"))
        .def_readwrite("values",&TsData::values,"doc tbd")
        .def("size",&TsData::size,"doc tbd")
        .def_readonly("time_axis",&TsData::time_axis,"tbd doc")
        ;
}
#endif
void all_time_series_support() {
    using namespace boost::python;
    def("compressed_size", compressed_size_double,args("double_vector","accuracy"));
    def("compressed_size", compressed_size_float, args("float_vector", "accuracy"));
#if 0
    def("ts_compress", ts_compress_float, args("ts_data_float", "accuracy"),
        doc_intro("return a compressed time-series")
    );
    def("ts_compress", ts_compress_double, args("ts_data_double", "accuracy"),
        doc_intro("return a compressed time-series")
    );
    def("ts_decompress", ts_decompress_float, args("ts_data_float", "delta_t"),
        doc_intro("return a compressed time-series")
    );
    def("ts_decompress", ts_decompress_double, args("ts_data_double", "delta_t"),
        doc_intro("return a compressed time-series")
    );

    e_utcperiod();
    e_time_axis();
    e_ts_data<float>("TsDataFloat");
    e_ts_data<double>("TsDataDouble");
#endif
}

}
