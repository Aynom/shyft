# Compilation step for Python extensions

include_directories(
  ${CMAKE_SOURCE_DIR}/cpp
  ${SHYFT_DEPENDENCIES}/include
  ${python_include} 
  ${python_numpy_include}
)

set(py_core  "core" )
set(cpps 
	api.cpp
	api_hydro_power_system.cpp
	api_time_series_support.cpp
	api_server.cpp
)

add_library(${py_core} SHARED  ${cpps} )
    set_target_properties(${py_core} PROPERTIES OUTPUT_NAME ${py_core})
    set_target_properties(${py_core} PROPERTIES PREFIX "_" INSTALL_RPATH "$ORIGIN/../../lib") # Python extensions do not use the 'lib' prefix, and rpath ref
    if(MSVC)
        set_target_properties(${py_core} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    endif()
    target_link_libraries(${py_core} em_model_core  ${boost_py_link_libraries}  )

install(TARGETS ${py_core} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/energy_market/core)

