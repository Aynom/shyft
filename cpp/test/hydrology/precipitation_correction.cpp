#include "test_pch.h"
#include <shyft/hydrology/methods/precipitation_correction.h>

using namespace std;
using namespace shyft::core::precipitation_correction;
using namespace shyft::core;

TEST_SUITE("precipitation_correction") {
TEST_CASE("test_pc_equal_operator") {
	parameter pc1;
	parameter pc2{99};
	
	TS_ASSERT(pc1 != pc2);
	
	pc2.scale_factor = 1.0;
		
	TS_ASSERT(pc1 == pc2);
}
}

