# CMake configuration for tests

set(test_sources 
    runner.cpp 
    mocks.cpp 
    hydrology/actual_evapotranspiration.cpp 
    hydrology/api.cpp 
    hydrology/calibration.cpp 
    hydrology/cell_builder.cpp 
    hydrology/gamma_snow.cpp 
    hydrology/glacier_melt.cpp 
    hydrology/gridpp.cpp 
    hydrology/hbv_actual_evapotranspiration.cpp 
    hydrology/hbv_snow.cpp 
    hydrology/hbv_soil.cpp 
    hydrology/hbv_stack.cpp 
    hydrology/hbv_tank.cpp 
    hydrology/inverse_distance.cpp 
    hydrology/kalman.cpp 
    hydrology/kirchner.cpp 
    hydrology/kriging.cpp  
    hydrology/priestley_taylor.cpp 
    hydrology/pt_gs_k.cpp 
    hydrology/pt_hs_k.cpp 
    hydrology/pt_ss_k.cpp 
    hydrology/region_model.cpp 
    hydrology/routing.cpp 
    hydrology/skaugen.cpp
    hydrology/hbv_physical_snow.cpp
    hydrology/pt_hps_k.cpp 
    hydrology/radiation.cpp 
    hydrology/radiation_ts.cpp 
    hydrology/penman_monteith.cpp 
    hydrology/r_pm_gs_k.cpp 
    hydrology/experimental.cpp 
    hydrology/sceua.cpp 
    hydrology/geo.cpp 
    hydrology/drms.cpp
	hydrology/precipitation_correction.cpp
	hydrology/mstack_param.cpp
    time_series/max_abs_average_accessor.cpp
    time_series/predictor.cpp 
    time_series/merge.cpp 
    time_series/qm.cpp
    time_series/serialization.cpp  
    time_series/time_axis.cpp 
    time_series/average.cpp 
    time_series/fixup.cpp 
    time_series/ts_basics.cpp 
    time_series/utctime_utilities.cpp 
    time_series/ice_packing.cpp 
    time_series/decode.cpp 
    time_series/derivative.cpp
    time_series/qac.cpp 
    time_series/use_time_axis.cpp
    time_series/clip_to_period.cpp 
    time_series/accumulate.cpp 
    time_series/bucket.cpp 
    time_series/eval.cpp
    time_series/extend.cpp 
    time_series/repeat.cpp
    dtss/dtss_basics.cpp
    dtss/stress.cpp 
    dtss/mutex.cpp 
    dtss/db.cpp 
    dtss/url.cpp 
    dtss/krls_pred_db.cpp 
    web_api/web_server.cpp 
    web_api/grammar.cpp 
    web_api/generator.cpp 
    web_api/web_manual.cpp
    web_api/em_grammar.cpp
    web_api/em_generator.cpp
)

set(target "test_shyft")
    add_definitions("-D__UNIT_TEST__ -DVERBOSE=0 ")
    add_executable(${target} ${test_sources})
    target_include_directories(${target} PRIVATE ".")
    target_link_libraries(${target} shyft_core shyft_api em_model_core stm_core  ${boost_link_libraries} dlib::dlib ${arma_libs})
    #set_target_properties(${target} PROPERTIES BUILD_WITH_INSTALL_RPATH TRUE)
    set_target_properties(${target} PROPERTIES INSTALL_RPATH "$ORIGIN/../../shyft/lib")
    install(TARGETS ${target} CONFIGURATIONS Release DESTINATION ${CMAKE_SOURCE_DIR}/bin/Release)
    install(TARGETS ${target} CONFIGURATIONS Debug DESTINATION ${CMAKE_SOURCE_DIR}/bin/Debug)
    if(BUILD_COVERAGE)
        include(${PROJECT_SOURCE_DIR}/build_support/CodeCoverage.cmake)
        APPEND_COVERAGE_COMPILER_FLAGS()
        set(COVERAGE_EXCLUDES '/usr/include/*' '*/boost/*' '*/armadillo*' '*/dlib/*' '*/doctest/*' '${SHYFT_DEPENDENCY_DIR}/include/*' )
        SETUP_TARGET_FOR_COVERAGE(
            NAME ${target}_coverage
            EXECUTABLE ${target}
            DEPENDENCIES ${target}
        )
    endif()

# This can be made more specific, but we would need the list of tests.
#add_test(${target} ${target})
#The current list:
    set(all_tests "actual_evapotranspiration"  "time_series" "ts_merge" "time_axis" "time_series_extend"
        "utctime_utilities" "api"  "dtss_db" "dtss_krls_pred_db"
        "dtss" "dtss_url" "gamma_snow" "geo_cell_data" "glacier_melt" "gridpp" "hbv_actual_evapotranspiration"
        "hbv_physical_snow" "hbv_snow" "hbv_soil" "hbv_stack" "hbv_tank" "inverse_distance" "dtss_json"
        "kalman" "kirchner" "kriging" "mstack_param" "penman_monteith" "precipitation_correction" "predictors" "priestley_taylor"
        "pt_gs_k" "pt_hps_k" "pt_hs_k" "pt_ss_k" "qm" "r_pm_gs_k" "radiation" "region_model" "routing"
        "sceua" "serialization" "skaugen"
        "web_api_generator" "web_api_grammar" "calibration" "cell_builder"
    )
    foreach(shyft_ts ${all_tests} )
        add_test(${shyft_ts} ${target} -nv --test-suite=${shyft_ts})
    endforeach(shyft_ts)



