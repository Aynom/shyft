#include <doctest/doctest.h>
#include <shyft/energy_market/srv/db.h>
#include <shyft/energy_market/srv/server.h>
#include <shyft/energy_market/srv/client.h>
#include <build_test_model.h>

namespace fs=boost::filesystem;
using std::to_string;
using std::string;
using std::vector;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;

struct dir_cleanup {
    fs::path d;
    ~dir_cleanup() {
        if(fs::exists(d)){
            #ifdef _WIN32
            for (int i = 0; i<10; ++i) {
                std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(1000));
                try {
                    fs::remove_all(d);
                    break;
                }
                catch (...) {
                    std::cout <<"Try #"<<i+1<< ":Failed to remove " << d << "\n";
                }
            }
            #else
            fs::remove_all(d);
            #endif
        }
    }
};

TEST_SUITE("server") {
    //-- this is what we use and test:
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::srv::db;
    using shyft::energy_market::srv::server;
    using shyft::energy_market::srv::client;
    using shyft::energy_market::market::model;
    
    //-- keep test directory, unique, and with auto-cleanup.
    auto dirname = "srv.db.test." +to_string(to_seconds64(utctime_now()) );
    auto tmpdir = (fs::temp_directory_path()/fs::path(dirname));
    dir_cleanup wipe{tmpdir};
    
    //-- now ready for testing:
    TEST_CASE("serverdb") {
        //    
        // 0. test we can create a db
            db<model> dbm{tmpdir.generic_string()};
            vector<int64_t> mids;
            FAST_CHECK_EQ(0u,dbm.get_model_infos(mids).size());
        //
        // 1. test we can store one model into db
            auto m=test::build_model();
            m->id=0;// create new model, require 0 here
            model_info mi{0,"name",utctime_now(),"{a:'key',b:1.23}"};
            auto mid=dbm.store_model(m,mi);
            FAST_CHECK_GE(1u,mid);
            // update mi&m to new id: ref todo in db.h, we might change the db-layer to mutable mi and m, to avoid this
            mi.id=mid;
            m->id=mid;
            // verify model-info is there
            auto mis=dbm.get_model_infos(mids);
            FAST_CHECK_EQ(1u,mis.size());
            FAST_CHECK_EQ(mi,mis[0]);
            vector<int64_t> midv{mid};
            
            mis=dbm.get_model_infos(midv);
            FAST_CHECK_EQ(1u,mis.size());
            FAST_CHECK_EQ(mi,mis[0]);
            FAST_CHECK_EQ(1u,dbm.find_max_model_id());// verify we correctly can find max model-id
            // and that we can update it:
            mi.json=string("{Updated info}");
            dbm.update_model_info(mid,mi);
            
            mis=dbm.get_model_infos(midv);
            FAST_CHECK_EQ(mi,mis[0]);// verify we now are aligned with the new stored info
            
            // and that we can get back the model
            auto mr = dbm.read_model(mid);
            FAST_CHECK_UNARY(mr!=nullptr);
            FAST_CHECK_UNARY(*mr == *m);
            // 
            // store another model, and check it can handle more than one:
            auto m2=test::build_model();
            m2->id=0;
            model_info mi2={0,"name2",utctime_now(),"{b:'vv',b:3.21}"};
            auto mid2=dbm.store_model(m2,mi2);
            // verify model-infos and models are available
            mi2.id=mid2;
            m2->id=mid2;
            FAST_CHECK_EQ(2u,dbm.find_max_model_id());// verify we correctly can find max model-id

            mis=dbm.get_model_infos(mids);
            FAST_CHECK_EQ(2u,mis.size());
            auto ix2= mis[0].id==mid2?0:1;
            FAST_CHECK_EQ(mi2,mis[ix2]);
            
            auto mr2=dbm.read_model(mid2);
            FAST_CHECK_NE(*mr2,*mr);
            FAST_CHECK_EQ(*mr2,*m2);
            
            //
            // remove model
            dbm.remove_model(mid);

            // verify we got just one model left
            mis=dbm.get_model_infos(mids);
            FAST_REQUIRE_EQ(1u,mis.size());
            FAST_CHECK_EQ(mi2,mis[0]);// and that we got the right model left
            FAST_CHECK_EQ(2u,dbm.find_max_model_id());// verify we correctly can find max model-id, it's still 2 since we removed 1'
            
            // remove last model..
            dbm.remove_model(mid2);
            mis=dbm.get_model_infos(mids);
            FAST_REQUIRE_EQ(0u,mis.size());
            FAST_CHECK_EQ(0u,dbm.find_max_model_id());// verify we correctly can find max model-id, it's still 2 since we removed 1'
    }
    TEST_CASE("serverio") {
        using db_t=db<model>;
        using srv_t=server<db_t>;
        using client_t=client<model>;
        //
        // 0. arrange with a server and a client running on localhost, tempdir.
        //
        srv_t s0(tmpdir.generic_string());
        string host_ip="127.0.0.1";
        s0.set_listening_ip(host_ip);
        auto s_port=s0.start_server();
        FAST_CHECK_GE(s_port,1024);
        client_t c{host_ip +":"+to_string(s_port)};
        vector<int64_t> mids;
        auto mis=c.get_model_infos(mids);
        FAST_CHECK_EQ(mis.size(),0);
        //
        // 1.  arrange with one model
        //
        auto m = test::build_model();
        model_info mi {0,"name2",utctime_now(),"{b:'vv',b:3.21}"};
        m->id=0;
        
        auto mid=c.store_model(m,mi);
        FAST_CHECK_EQ(mid,1u);
        //
        // 2. with one model, verify minfo and m is ok
        //
        m->id=mid;
        mi.id=mid;
        auto mr=c.read_model(mid);
        FAST_CHECK_EQ(*mr,*m);
        mis=c.get_model_infos(mids);
        FAST_CHECK_EQ(mis.size(),1u);
        FAST_CHECK_EQ(mi,mis[0]);
        c.close();//just to force auto-open on next call
        //
        // 3. check we can update minfo of existing model
        //
        mi.json=string("{something:else}");
        c.update_model_info(mi.id,mi);
        mis=c.get_model_infos(mids);
        FAST_CHECK_EQ(mis.size(),1u);
        FAST_CHECK_EQ(mi,mis[0]);
        
        //
        // 4. finally, remove the model, and verify we are back to zero
        //
        c.remove_model(mid);
        mis=c.get_model_infos(mids);
        FAST_CHECK_EQ(mis.size(),0);
        c.close();// should work here
    }
}
