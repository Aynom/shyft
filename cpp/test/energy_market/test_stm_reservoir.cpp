#include <string>
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/stm_system.h>

using namespace std;
using namespace shyft::energy_market::stm;

TEST_SUITE("stm_reservoir"){
	TEST_CASE("reservoir basics") {
        const auto name{ "res_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto r = b.create_reservoir(1, name,json_string);
        FAST_CHECK_EQ(r->id, 1);
        FAST_CHECK_EQ(r->name, name);
        FAST_CHECK_EQ(r->json,json_string);
        CHECK_THROWS_AS(b.create_reservoir(1,name+"a",""),stm_rule_exception);
        CHECK_THROWS_AS(b.create_reservoir(2,name,""),stm_rule_exception);
	}

}
