#include "test_pch.h"
#include <shyft/web_api/web_api_generator.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

#include "test_parser.h"

// play-ground for stuff that will be promoted once it's ok'

using std::vector;
using std::string;
using boost::spirit::karma::generate;

using namespace shyft::core;

using shyft::time_series::ts_point_fx;
using shyft::time_series::point;

using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::gta_t;


namespace shyft::web_api::generator {
    
    inline namespace v1 {
        namespace ka=boost::spirit::karma;
        namespace phx=boost::phoenix;
        using xy_point=shyft::energy_market::hydro_power::point;
        using shyft::energy_market::hydro_power::xy_point_curve;
        using shyft::energy_market::hydro_power::xy_point_curve_with_z;
        using shyft::energy_market::hydro_power::turbine_efficiency;
        using shyft::energy_market::hydro_power::turbine_description;
        using shyft::energy_market::stm::xyz_point_curve_list;
        /** @brief grammer for emitting a x,y point
        *
        */
        template<class OutputIterator>
        struct xy_generator:ka::grammar<OutputIterator,xy_point()> {
            xy_generator():xy_generator::base_type(pg) {
                using ka::double_;
                using ka::_1;
                using ka::_val;
                pg = '['<< double_[_1=phx::bind(&xy_point::x,_val)]<<','<<double_[_1=phx::bind(&xy_point::y,_val)]<< ']' ;
                pg.name("xy-point");
            }
            ka::rule<OutputIterator,xy_point()> pg;
        };
        template<class OutputIterator>
        struct xy_point_curve_generator:ka::grammar<OutputIterator,xy_point_curve()> {
           xy_point_curve_generator():xy_point_curve_generator::base_type(pg) {
               pg = ('['<< -(xy_%',') << ']')[ka::_1=phx::bind(&xy_point_curve::points,ka::_val)];
               pg.name("xy_point_curve");
           }
           ka::rule<OutputIterator,xy_point_curve()> pg;
           xy_generator<OutputIterator> xy_;
        };

        template<class OutputIterator>
        struct xy_point_curve_with_z_generator:ka::grammar<OutputIterator,xy_point_curve_with_z()> {
           xy_point_curve_with_z_generator():xy_point_curve_with_z_generator::base_type(pg) {
               pg = ka::lit("{\"z\":")<<double_[ka::_1=phx::bind(&xy_point_curve_with_z::z,ka::_val)]<<
                    ka::lit(",\"points\":")<< xy_points_[ka::_1=phx::bind(&xy_point_curve_with_z::xy_curve,ka::_val)]<<'}';
               pg.name("xy_point_curve_with_z");
           }
           ka::rule<OutputIterator,xy_point_curve_with_z()> pg;
           xy_point_curve_generator<OutputIterator> xy_points_;
        };


        template<class OutputIterator>
        struct xyz_point_curve_list_generator:ka::grammar<OutputIterator,xyz_point_curve_list()> {
           xyz_point_curve_list_generator():xyz_point_curve_list_generator::base_type(pg) {
               pg = ('['<< -(xyz_points_%',') << ']');
               pg.name("xyz_point_curve_list");
           }
           ka::rule<OutputIterator,xyz_point_curve_list()> pg;
           xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
        };

        template<class OutputIterator>
        struct turbine_efficiency_generator:ka::grammar<OutputIterator,turbine_efficiency()> {
           turbine_efficiency_generator():turbine_efficiency_generator::base_type(pg) {
               pg = ka::lit("{\"prod_min\":")<<double_[ka::_1=phx::bind(&turbine_efficiency::production_min,ka::_val)]<<
                    ka::lit(",\"prod_max\":")<<double_[ka::_1=phx::bind(&turbine_efficiency::production_max,ka::_val)]<<
                    ka::lit(",\"eff_curves\":[")<<(-(xyz_points_ %',')) [ka::_1=phx::bind(&turbine_efficiency::efficiency_curves,ka::_val)]<<ka::lit("]}");
               pg.name("turbine_effiency");
           }
           ka::rule<OutputIterator,turbine_efficiency()> pg;
           xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
        };

        template<class OutputIterator>
        struct turbine_description_generator:ka::grammar<OutputIterator,turbine_description()> {
           turbine_description_generator():turbine_description_generator::base_type(pg) {
               pg =
                    ka::lit("{\"effiencies\":[")<<(-(t_eff_ %',')) [ka::_1=phx::bind(&turbine_description::efficiencies,ka::_val)]<<ka::lit("]}");
               pg.name("turbine_description");
           }
           ka::rule<OutputIterator,turbine_description()> pg;
           turbine_efficiency_generator<OutputIterator> t_eff_;
        };

    }
}

namespace shyft::web_api::generator {
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::hydro_power::hydro_connection;
    using shyft::energy_market::hydro_power::connection_role;
    using shyft::energy_market::hydro_power::hydro_component;
    using shyft::energy_market::hydro_power::reservoir;
    using shyft::energy_market::hydro_power::unit;
    using shyft::energy_market::hydro_power::waterway;
    using shyft::energy_market::hydro_power::gate;
    using shyft::energy_market::hydro_power::power_plant;
    using shyft::energy_market::hydro_power::hydro_power_system;
    using std::string_view;
    using ka::int_;
    using ka::double_;
    namespace stm=shyft::energy_market::stm;
    constexpr char quote='"';
    constexpr char colon=':';
    constexpr char obj_begin='{';
    constexpr char obj_end='}';
    constexpr char arr_begin='[';
    constexpr char arr_end=']';
    constexpr char comma=',';

/** Workbench notes:
 * What we try to achieve
 *
 * 1. speed/efficiency:
 *     hpc          : re-using boost::spirit::karma generators for time,period,time-series etc. since they do carry the
 *                    volume/performance critical parts
 *     zero-copy    : do not copy strings,etc, to emit (consider friend
 *
 * 2. ease of use/extension
 *     few lines    : we would like to have like one line code for each attribute exposed (for a given context).
 *     composition  : compose aggregates
 *     easy reading : direct code,
 *     parameterize : send parameters to emitter to control form/amount of emitted code.
 *     testing      : by composition
 *     !intrusive   : should be possible to use existing classes with no mods (maybe a friend class access, is allowed in case we start with private members).
 *
 * 3. what we do not yet need
 *     general framework : unless it satisfies the above requirements, including reusing boost::spirit::karma for the hpc parts.
 *
 */

    //--
    template <class OutputIterator> void emit_null(OutputIterator& oi) {*oi++='n';*oi++='u';*oi++='l';*oi++='l';}
    template <class OutputIterator> void emit(OutputIterator&oi,double  value) { generate(oi,double_,value);}
    template <class OutputIterator> void emit(OutputIterator&oi,int64_t value) { generate(oi,int_,value);}
    template <class OutputIterator> void emit(OutputIterator&oi,utctime value) {static  utctime_generator<OutputIterator> t_; generate(oi,t_,value);}
    template <class OutputIterator> void emit(OutputIterator&oi,utcperiod value) {static  utcperiod_generator<OutputIterator> p_; generate(oi,p_,value);}
    //-- xy-type curve-descriptions, karma-generators,  considered basic types in this context:
    template <class OutputIterator> void emit(OutputIterator&oi,xy_point_curve const&value) {static xy_point_curve_generator<OutputIterator> g_;generate(oi,g_,value);}
    template <class OutputIterator> void emit(OutputIterator&oi,xy_point_curve_with_z const&value) {static xy_point_curve_with_z_generator<OutputIterator> g_;generate(oi,g_,value);}
    template <class OutputIterator> void emit(OutputIterator&oi,turbine_description const&value) {static turbine_description_generator<OutputIterator> g_;generate(oi,g_,value);}

    //template <class OutputIterator> void emit(OutputIterator&oi,std::string const& value) {*oi++=quote;copy(begin(value),end(value),oi);*oi++=quote;}
    //TODO: for the string handling, ensure we deal with proper quoting/encoding
    template <class OutputIterator> void emit(OutputIterator&oi,char const * value) {*oi++=quote;while(value && *value) *oi++=*value++;*oi++=quote;}
    template <class OutputIterator> void emit(OutputIterator&oi,string_view value) {*oi++=quote;copy(begin(value),end(value),oi);*oi++=quote;}

    //-- emit null or object, needs fwd. decl here, then _after_ all T are know,
    //   do the def. of the template (annoying, due to lack of namespace/ADL for this approach)

    template <class OutputIterator,class T>
    void emit_shared_ptr(OutputIterator &oi,std::shared_ptr<T> const &o) ;

    template <class OutputIterator,class Value>
    void emit(OutputIterator& oi,std::vector<Value>const& value);
    /** json object emitter
     *
     *  needs context to fix the comma sep
    */
    template <class OutputIterator>
    struct emit_object {
        OutputIterator&oi;
        bool first{true};
        emit_object(OutputIterator&oi):oi{oi}{*oi++=obj_begin;}
        ~emit_object() {
            *oi++=obj_end;
        }

        void sep() {if(first) first=false;else *oi++=comma;}

        /** emit name-value pair
         *
         * note that we have to defer the trivial def of it until _after_
         * the vector-def. is seen, so that compiler succeed resolving
         * emit( vector<T>..)
         *
         * TO CONSIDER: Do better decomposition of the template library tools
         *
         */
        template <class Value>
        emit_object& def(std::string_view name,Value const& value);


        template <class Fx>
        emit_object& def_fx(std::string_view name,Fx&& fx) {
            sep();
            emit(oi,name);
            *oi++ = colon;
            fx(oi);
            return *this;
        }

        emit_object(const emit_object&)=delete;
        emit_object(emit_object&&)=delete;
        emit_object & operator=(emit_object const&) =delete;
        emit_object & operator=(emit_object &&) =delete;
    };

    /** x_emit_shared_ptr
     *
     *  utility macro to create null or object emitter,
     *  we use a lot of shared-ptr, so we would like null or object allmost everywhere
     */
    #define x_emit_shared_ptr(T) template <class OutputIterator> void emit(OutputIterator &oi,std::shared_ptr<T> const &o) {emit_shared_ptr(oi,o);}

    //-----------------
    // here we place the emitters for our classes
    // like one line each attribute.
    //
    template <class OutputIterator>
    void emit(OutputIterator &oi,model_info const &o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("id",o.id)
        .def("name",o.name)
        .def("created",o.created)
        .def("json",o.json)
        ;
    }



    template <class OutputIterator>
    void emit(OutputIterator &oi,hydro_connection const &o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def_fx("role",[r=o.role](OutputIterator&oi){
            switch(r) {
                case connection_role::main:return emit(oi,"main");
                case connection_role::bypass:return emit(oi,"bypass");
                case connection_role::flood:return emit(oi,"flood");
                case connection_role::input:return emit(oi,"input");
            }
        })
        .def_fx("target",[r=o.target.get()](OutputIterator&oi){
            if(r) {
                *oi++=quote;
                if(dynamic_cast<waterway*>(r)) *oi++='W';
                else if(dynamic_cast<unit*>(r)) *oi++='A';
                else if(dynamic_cast<reservoir*>(r)) *oi++='R';
                else *oi++='?'; //TODO: consider throw runtime_error()
                emit(oi,r->id);
                *oi++=quote;
            } else {
                emit_null(oi);
            }
        }
        )
        ;
    }

    template <class OutputIterator>
    void emit(OutputIterator &oi,hydro_component const &o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("id",o.id)
        .def("name",o.name)
        .def("json",o.json)
        .def("upstreams",o.upstreams)
        .def("downstreams",o.downstreams)
        ;
    }



    x_emit_shared_ptr(hydro_component)

    template <class OutputIterator>
    void emit(OutputIterator &oi,power_plant const &o) {
        emit_object<OutputIterator> oo(oi);
        std::vector<int64_t> a_ids;for(auto const&a:o.units) if(a) a_ids.push_back(a->id); // output by ref, id only
        oo
        .def("id",o.id)
        .def("name",o.name)
        .def("json",o.json)
        .def("units",a_ids)
        ;
    }

    x_emit_shared_ptr( power_plant )

    template <class OutputIterator>
    void emit(OutputIterator&oi, gate const&o) {
        emit_object<OutputIterator> oo(oi);
        oo.def("id",o.id).def("name",o.name).def("json",o.json);
    }
    x_emit_shared_ptr(gate)

    template<class OutputIterator>
    void emit(OutputIterator&oi, waterway const& o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("type","W")
        .def("hydro_component",static_cast<const hydro_component&>(o))
        .def("gates",o.gates)
        ;
    }
    x_emit_shared_ptr( waterway )

    //-- just for testing, a complete emitter for hydro-power system
    template<class OutputIterator,class HC>
    void emit_hc(OutputIterator&oi,const char*type_code, HC const& o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("type",type_code)
        .def("hydro_component",static_cast<const hydro_component&>(o))
        ;
    }

    template<class OutputIterator>
    void emit(OutputIterator&oi,unit const&o) {emit_hc(oi,"A",o);}


    template<class OutputIterator>
    void emit(OutputIterator&oi,reservoir const&o) {emit_hc(oi,"R",o);}

    x_emit_shared_ptr(unit)
    x_emit_shared_ptr(reservoir)

    template<class OutputIterator>
    void emit(OutputIterator&oi, hydro_power_system const& o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("id",o.id)
        .def("name",o.name)
        .def("created",o.created)
        .def("units",o.units )
        .def("waterways",o.waterways)
        .def("reservoirs",o.reservoirs)
        .def("power_plants",o.power_plants )
        ;
    }
    x_emit_shared_ptr(hydro_power_system)
    
    template<typename OutputIterator>
    void emit(OutputIterator&oi,apoint_ts const&ts) {
        static apoint_ts_generator<OutputIterator> ts_;
        generate(oi,ts_,ts);
    }
    //-- working with the stm reseroir as example,
    //   now we have to deal with properties (proxy_attr)
    //   that can be 'null', or have a value
    //    - note: most value-types are shared_ptr, so they might be null as well
    //
    //   json: matches to null or object
    //         and boils down to abilitiy to emit
    //         any of the basic proxy_attr value types as
    //         defined in stm/attribute_types.h
    //         we can represent map<key,value>
    //              as
    //                 [ [key,value],...]
    //        the basic value-types are mostly from hydro_power/xy_point_curve.h
    //           that deals with point(x,y) derived descriptions of volume-,gate-,turbine-,generator
    //           descriptions.
    //
    //   t_double_: (to be deprecated, replaced by apoint_ts type)
    //           null | [ [<time>, double_<precision>],.. ]
    //
    //   t_xy_ : (typical reservoir volume-description, Mm3 vs masl etc.)
    //           null | [ [<time>, <xy>],...]
    //    <xy> : (null|[ [double_,double_],... ])]
    //
    //   t_xyz_: (typical gate-opening-flow descr: given gate opening z, water-level x, then flow is y)
    //          null  | [ [<time>, <xyz>],...]
    //    <xyz>: {'z': double_, 'xy':<xy> }
    //
    // t_xyz_list_:
    //         null | [ [<time>], <xyz_list>],...]
    //
    // <xyz_list> :
    //           [ <xyz>,...]
    //
    // t_turbine_description_ :
    //

    x_emit_shared_ptr(xy_point_curve)
    x_emit_shared_ptr(xy_point_curve_with_z) // alias xyz_point_curve
    x_emit_shared_ptr(xyz_point_curve_list)
    x_emit_shared_ptr(turbine_description)

    
    /** emit a map<utctime,T> value
     *
     * This resolves typical time-map (time-dependent attributes required in the stm9
     *
     * as [ [utctime,value],...] sequence
     * assuming that emit(oi,utctime) and emit(oi,T) exists.
     */
    template<class OutputIterator,class T>
    void emit_t_map(OutputIterator&oi,std::map<utctime,T> const &value) {
        *oi++=arr_begin;
        bool first=true;
        for(auto const&kv:value) {
            if(!first) *oi++=comma; else first=false;
            *oi++=arr_begin;
            emit(oi,kv.first);
            *oi++=comma;
            emit(oi,kv.second);
            *oi++=arr_end;
        }
        *oi++=arr_end;
    }

    using shyft::energy_market::stm::t_double_;//to be replaced with apoint_ts later.
    using shyft::energy_market::stm::t_xy_;
    using shyft::energy_market::stm::t_xyz_;
    using shyft::energy_market::stm::t_xyz_list_;
    using shyft::energy_market::stm::t_turbine_description_;

    template<class OutputIterator,class T_MAP>
    void emit_ptr_t_map(OutputIterator&oi,T_MAP const &value) {
        if(value==nullptr) {
            emit_null(oi);
        } else {
            emit_t_map(oi,*value);
        }
    }

    // save typing, macro go generate the templates.
    #define x_emit_ptr_t_map(PTR_T) template<class OutputIterator> void emit(OutputIterator&oi,PTR_T const&v) {emit_ptr_t_map(oi,v);}

    x_emit_ptr_t_map(t_double_)
    x_emit_ptr_t_map(t_xy_)
    x_emit_ptr_t_map(t_xyz_)
    x_emit_ptr_t_map(t_xyz_list_)
    x_emit_ptr_t_map(t_turbine_description_)
    #undef x_emit_ptr_t_map
    /* -- now stm::reservoir, with proxy attributes
     *
     * Given that we only provide raw-topology for the topology response,
     * how to we provide a best possible web_api to retrieve/set the values?
     *
     * From the user-interface side, what we would like is to make request that
     * fills up the component-model for visualization
     *
     * like:
     *  request{request_id:888,hps_id:3,reservoir_ids:[1,3,4],attr_ids:[lrl,volume_descr],t=now}
     *
     * then get back a response(s) from the server-socket like:
     *
     * make_response(object,attr-list[lrl,volume_descr,..],valid_at=t)
     * ->[object.id,object.lrl(t),object.volume_descr(t),]
     *
     * e.g.     lrl(t)   volume_descr(t)
     *     id   lrl    Mm3 masl   Mm3   masl
     *   [123,433.2,[[0.0,433.2],[400.0,500.0]]]
     *
     * Q1: So what is the perfect attr-list ?
     *  A1:the int-version of the rsv_attr enum.
     *  why ?
     *   1. because on server-side it resolves at zero overhead to its value using
     *    proxy_attr<reservoir,t_double,rsv_attr,attr_id,reservoir::ids> a(object);
     *    then a.exists() -> true if it is set/exists
     *     and a.get() -> a_v, that is a value of value-type, given true above
     *     and valid_at(a_v,t) -> a_v_t, value valid at time t.
     *       the type of a_v_t above would be double in this example
     *       that we feed into the boost::spirit::karma double_ emitter.
     *   !!! ease mapping work:
     *    let reservoir provide the methods:
     *       has_attr<value_type>(rsv_attr|int,valid_at:utctime) -> value_type
     *       get_attr<value_type>(rsv_attr|int,valid_at:utctime) -> value_type
     *       set_attr(rsv_attr|int,valid_at:utctime,value_type val)->ok
     *    and for time-series, similar, but adapted:
     *       get_ts(rsv_attr|int, transform_spec, period_spec..)->time-series
     *     .. the above require you to compile-time know the value-type of rsv_attr..
     *         like value_type<rsv_attr,..> ::type
     *
     *    2. minimal consistent repr.
     * Q2, given A1,how to provide the enum to type-script ?
     *   A2: several options,
     *       as checked in library (generated from c++)
     *       as separate service call (would that be useful ?)
     *
     * Similar for setting the values (e.g. lrl):
     *
     *  request  {request_id:111,reservoir_ids:[1,2,3],attr_ids:[lrl],values:[100,200,300]}
     *
     * response: {request_id:111, result: "ok" }
     *
     */


    //-- end our classes emitters

    //----------------------------------
    // vectors of object (given object is defined)
    //
    /** emit a vector of values
    *   Could be upgraded to emit a range/sequence instead
    */
    template <class OutputIterator,class Value>
    void emit(OutputIterator& oi,std::vector<Value>const& value) {
        *oi++=arr_begin;
        bool first=true;
        for(auto const&v:value) {
            if(!first) *oi++=comma; else first=false;
            emit(oi,v);
        }
        *oi++=arr_end;
    }

    template <class OutputIterator,class T>
    void emit_shared_ptr(OutputIterator &oi,std::shared_ptr<T> const &o) {
        if(o==nullptr) emit_null(oi);
        else emit(oi,*o);
    }
    /** the implementation template needs to appear here, after the vector emitter above */
    template<class OutputIterator>
    template <class Value>
    emit_object<OutputIterator>& emit_object<OutputIterator>::def(std::string_view name,Value const& value) {
        sep();
        emit(oi,name);
        *oi++ = colon;
        emit(oi,value); // value here could be a vector<T>, so with the current approach it must have been seen at the point of template def (no ADL/scope that can help us.).
        return *this;
    }



}
namespace test::web_api {
    using namespace shyft::energy_market::hydro_power;
    using std::make_shared;
    using std::make_unique;

    inline hydro_power_system_ build_hps(string name) {
        auto hpsm = make_shared<hydro_power_system>(name);
        auto hps = make_unique<hydro_power_system_builder>(hpsm);
        auto r1 = hps->create_reservoir(1,"r1", R"(xxxr)");
        auto r2 = hps->create_reservoir(2,"r2", R"(reservoir_data(100, 110, 10))");
        auto r3 = hps->create_reservoir(3,"r3", R"(reservoir_data(100, 110, 10))");
        auto o = hps->create_reservoir(4,"ocean",R"( reservoir_data(0, 0.01, 1e100))");
        auto p1 = hps->create_unit (1,"the one",R"(power_station_data...)");
        auto p2 = hps->create_unit (2,"the other", R"(power_station_data()");
        auto p = hps->create_power_plant (3,"pp","json{}");
        power_plant::add_unit(p,p1);
        power_plant::add_unit(p,p2);

        auto t1 = hps->create_tunnel(1, "r1-p1", "");
        waterway::add_gate(t1, make_shared<gate>(1, "2", "json{3:'x'}"));
        connect(t1).input_from(r1).output_to(p1);
        connect(hps->create_river(2,"r2-r1 river","")).input_from(r2).output_to(r1);
        connect(hps->create_river(3,"p to ocean", "")).input_from(p1).output_to(o);
        connect(hps->create_tunnel(4,"r3-p2", "")).input_from(r3).output_to(p2);
        connect(hps->create_river(5,"p2 to ocean", "")).input_from(p2).output_to(o);
        connect(hps->create_river(6,"r3-bypass ocean", "")).input_from(r3, connection_role::bypass).output_to(o);
        connect(hps->create_river(7,"r2-flood ocean", "")).input_from(r2, connection_role::flood).output_to(o);

        auto wms1 = hps->create_catchment(1,"9471-U", "some jason metadata");
        return hpsm;
    }
}

using shyft::energy_market::srv::model_info;
using shyft::energy_market::hydro_power::power_plant;
using shyft::energy_market::hydro_power::hydro_connection;
using shyft::energy_market::hydro_power::waterway;
using shyft::energy_market::hydro_power::reservoir;
using shyft::energy_market::hydro_power::unit;
using shyft::energy_market::hydro_power::connection_role;
using shyft::energy_market::hydro_power::hydro_component;

using std::string;using std::vector;using std::make_shared;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::stm::t_xy_;
using namespace shyft::web_api::generator;

TEST_SUITE("em_web_api_generator") {
TEST_CASE("em_xy_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    xy_point a{1.0,2.0};
    auto ok=generate(sink,xy_generator<decltype(sink)>(),a);
    FAST_REQUIRE_EQ(ok,true);
    FAST_CHECK_EQ(R"_([1.0,2.0])_",ps);

    vector<xy_point> aa;
    ps.clear();
    FAST_REQUIRE_EQ(true,generate(sink,xy_point_curve_generator<decltype(sink)>(),aa));
    FAST_CHECK_EQ(R"_([])_",ps);
    //FAST_REQUIRE_EQ(true,generate(sink,xyv_generator<decltype(sink)>(),aa));
    //FAST_CHECK_EQ(R"_([[1.0,2.0],[3.0,4.0]])_",ps);
    ps.clear();
    aa.push_back(a);aa.push_back(xy_point{3.0,4.0});
    xy_point_curve aaa;aaa.points=aa;
    FAST_REQUIRE_EQ(true,generate(sink,xy_point_curve_generator<decltype(sink)>(),aa));
    FAST_CHECK_EQ(R"_([[1.0,2.0],[3.0,4.0]])_",ps);
}
TEST_CASE("em_xyz_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    xy_point_curve_with_z  a;
    FAST_REQUIRE_EQ(true,generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"z":0.0,"points":[]})_",ps);
    ps.clear();
    a.z=1.0;
    a.xy_curve.points.push_back(xy_point{3.0,4.0});
    FAST_REQUIRE_EQ(true,generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"z":1.0,"points":[[3.0,4.0]]})_",ps);
    // step up to list
    xyz_point_curve_list aa;
    ps.clear();
    FAST_REQUIRE_EQ(true,generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa));
    FAST_CHECK_EQ(R"_([])_",ps);
    aa.push_back(a);
    ps.clear();
    FAST_REQUIRE_EQ(true,generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa));
    FAST_CHECK_EQ(R"_([{"z":1.0,"points":[[3.0,4.0]]}])_",ps);

}
TEST_CASE("em_turbine_eff_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    turbine_efficiency  a;
    FAST_REQUIRE_EQ(true,generate(sink,turbine_efficiency_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"prod_min":0.0,"prod_max":0.0,"eff_curves":[]})_",ps);
    ps.clear();
    a.production_max=100.0;
    a.production_min=9.0;
    a.efficiency_curves.push_back(xy_point_curve_with_z{});
    FAST_REQUIRE_EQ(true,generate(sink,turbine_efficiency_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"prod_min":9.0,"prod_max":100.0,"eff_curves":[{"z":0.0,"points":[]}]})_",ps);
}

TEST_CASE("em_turbine_description_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    turbine_description  a;
    FAST_REQUIRE_EQ(true,generate(sink,turbine_description_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"effiencies":[]})_",ps);
    ps.clear();
    a.efficiencies.push_back(turbine_efficiency{});
    FAST_REQUIRE_EQ(true,generate(sink,turbine_description_generator<decltype(sink)>(),a));
    FAST_CHECK_EQ(R"_({"effiencies":[{"prod_min":0.0,"prod_max":0.0,"eff_curves":[]}]})_",ps);
}


TEST_CASE("model_info_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    model_info a{
        1,
        string("aname"),
        utc.time(2018,1,2,3,4,5),
        string("somejson")
    };
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ(R"_({"id":1,"name":"aname","created":1514862245.0,"json":"somejson"})_",ps);
}
TEST_CASE("em_t_xy_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xy_ a;
    emit(sink,a);
    FAST_CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xy_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{1.0,2.0}});
    (*a)[utc.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{3.0,4.0}});
    emit(sink,a);
    FAST_CHECK_EQ(R"_([[3.0,[[1.0,2.0]]],[4.0,[[3.0,4.0]]]])_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_ a;
    emit(sink,a);
    FAST_CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,6)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},1.0);
    (*a)[utc.time(1970,1,1,0,0,7)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{3.0,4.0}}},2.0);
    emit(sink,a);
    FAST_CHECK_EQ(R"_([[6.0,{"z":1.0,"points":[[1.0,2.0]]}],[7.0,{"z":2.0,"points":[[3.0,4.0]]}]])_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_list_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_list_ a;
    emit(sink,a);
    FAST_CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_list_::element_type>();
    auto c1=make_shared<xyz_point_curve_list>();
    xy_point_curve_with_z e1{xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},3.0};
    c1->push_back(e1);
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    FAST_CHECK_EQ(R"_([[6.0,[{"z":3.0,"points":[[1.0,2.0]]}]]])_",ps);
}

TEST_CASE("em_t_turbine_description_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_turbine_description_ a;
    emit(sink,a);
    FAST_CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_turbine_description_::element_type>();
    auto c1=make_shared<turbine_description>();
    c1->efficiencies.push_back(turbine_efficiency{});
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    FAST_CHECK_EQ(R"_([[6.0,{"effiencies":[{"prod_min":0.0,"prod_max":0.0,"eff_curves":[]}]}]])_",ps);
}
TEST_CASE("em_t_double_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_double_ a;
    emit(sink,a);
    FAST_CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_double_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,6)]=1.0;
    (*a)[utc.time(1970,1,1,0,0,7)]=2.5;
    emit(sink,a);
    FAST_CHECK_EQ(R"_([[6.0,1.0],[7.0,2.5]])_",ps);
}


TEST_CASE("model_info_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    std::vector<model_info> a{
        model_info{
            1,
            string("aname"),
            utc.time(2018,1,2,3,4,5),
            string("somejson")
        },
        model_info{}
    };
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ(R"#([{"id":1,"name":"aname","created":1514862245.0,"json":"somejson"},{"id":0,"name":"","created":null,"json":""}])#",ps);
    ps.clear();
    a.clear();
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ("[]",ps);
}

TEST_CASE("hyd_connect_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    hydro_connection a{connection_role::main,nullptr};
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ(R"_({"role":"main","target":null})_",ps);
    ps.clear();
    a.role=connection_role::input;
    a.target=std::make_shared<waterway>(123,"wx");
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ(R"_({"role":"input","target":"W123"})_",ps);
}
TEST_CASE("hyd_connect_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto wr=std::make_shared<waterway>(123,"wx");
    auto rsv=std::make_shared<reservoir>(456,"rx");
    std::vector<hydro_connection> a{
        {connection_role::main,wr},
        {connection_role::main,nullptr},
        {connection_role::input,rsv}
    };
    shyft::web_api::generator::emit(sink,a);
    FAST_CHECK_EQ(R"_([{"role":"main","target":"W123"},{"role":"main","target":null},{"role":"input","target":"R456"}])_",ps);
}
TEST_CASE("hydro_component_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto wru=std::make_shared<waterway>(1,"wru");
    auto wr=std::make_shared<waterway>(3,"wrd");
    auto rsv=std::make_shared<reservoir>(2,"rx","json");
    hydro_component::connect(rsv,connection_role::main,wr);
    hydro_component::connect(wru,rsv);
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(rsv));
    FAST_CHECK_EQ(R"_({"id":2,"name":"rx","json":"json","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]})_",ps);
    ps.clear();
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(wr));
    FAST_CHECK_EQ(R"_({"id":3,"name":"wrd","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[]})_",ps);

}

TEST_CASE("power_station_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto a1=std::make_shared<unit>(1,"a1");
    auto a2=std::make_shared<unit>(2,"a2");
    auto p=std::make_shared<power_plant>(3,"p1");
        power_plant::add_unit(p,a1);
        power_plant::add_unit(p,a2);
    shyft::web_api::generator::emit(sink,p);
    FAST_CHECK_EQ(R"_({"id":3,"name":"p1","json":"","units":[1,2]})_",ps);
}

TEST_CASE("web_api_hps_gen") {
    auto hps=test::web_api::build_hps("A");
    std::string ps;
    auto sink=std::back_inserter(ps);
    shyft::web_api::generator::emit(sink,hps);
    FAST_CHECK_EQ(R"_({"id":0,"name":"A","created":null,"units":[{"type":"A","hydro_component":{"id":1,"name":"the one","json":"power_station_data...","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]}},{"type":"A","hydro_component":{"id":2,"name":"the other","json":"power_station_data(","upstreams":[{"role":"input","target":"W4"}],"downstreams":[{"role":"main","target":"W5"}]}}],"waterways":[{"type":"W","hydro_component":{"id":1,"name":"r1-p1","json":"","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]},"gates":[{"id":1,"name":"2","json":"json{3:'x'}"}]},{"type":"W","hydro_component":{"id":2,"name":"r2-r1 river","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R1"}]},"gates":[]},{"type":"W","hydro_component":{"id":3,"name":"p to ocean","json":"","upstreams":[{"role":"input","target":"A1"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":4,"name":"r3-p2","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"A2"}]},"gates":[]},{"type":"W","hydro_component":{"id":5,"name":"p2 to ocean","json":"","upstreams":[{"role":"input","target":"A2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":6,"name":"r3-bypass ocean","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":7,"name":"r2-flood ocean","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]}],"reservoirs":[{"type":"R","hydro_component":{"id":1,"name":"r1","json":"xxxr","upstreams":[{"role":"input","target":"W2"}],"downstreams":[{"role":"main","target":"W1"}]}},{"type":"R","hydro_component":{"id":2,"name":"r2","json":"reservoir_data(100, 110, 10)","upstreams":[],"downstreams":[{"role":"main","target":"W2"},{"role":"flood","target":"W7"}]}},{"type":"R","hydro_component":{"id":3,"name":"r3","json":"reservoir_data(100, 110, 10)","upstreams":[],"downstreams":[{"role":"main","target":"W4"},{"role":"bypass","target":"W6"}]}},{"type":"R","hydro_component":{"id":4,"name":"ocean","json":" reservoir_data(0, 0.01, 1e100)","upstreams":[{"role":"input","target":"W3"},{"role":"input","target":"W5"},{"role":"input","target":"W6"},{"role":"input","target":"W7"}],"downstreams":[]}}],"power_plants":[{"id":3,"name":"pp","json":"json{}","units":[1,2]}]})_",ps);

}
}

namespace experiment {
    using shyft::energy_market::core::proxy_attr_m;
    using namespace shyft::energy_market;
    //using namespace shyft::web_api::generator;
     // -- experiment section: 
     // -- goal: want one line pr. attr for set/get/exist.
     //             declarative
     //             if possible, derived from the proxy-types in the stm::reservoir class.
     // 
     //--ok, now lets' pretend we got a web-request, with 16606, and rsv_attr::hrl, lrl'
     ///  web-request: reservoir(oid).attr(aid) [.a_value_type.]
     ///  web-request: ot, oid, aid, value?
     /// -reply:: json (type-script-type)
     // for req:requests:
     ///  generate_json( req.o_d, req.a_id ,req.type_id)
     //
    
    /** @brief proxy_attr_exists
     * 
     * @detail just an example, the function could be generate_json etc..
     * 
     * @tparam C the class, like stm::reservoir
     * @tparam E the enum class, like stm::rsv_attr
     * @tparam E e the enum member, like stm::rsv_attr::lrl
     * @param o the const object ref to class C, e.g. stm::reservoir
     * @return true if the C::o have attribute e set to a value
     */
    
    template<typename C, typename E, E  e>
    bool proxy_attr_exists(C const & o) noexcept {
      return (o.*proxy_attr_m<C,E,e>::member()).exists();
    }
    

    
    
    
    /**  fx to compile time roll out the table map [int alias rsv_attr] proxy_attr_exists fx pointers.
     * @note that this template works for any stm class with proxy stuff in it
    */
    template<typename C,typename E, int...a> 
    constexpr std::array<bool(*)(C const&),sizeof...(a)>
    cmake_attr_exists_table(std::integer_sequence<int,a...>) { 
        return {{&proxy_attr_exists<C,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }
    
    // finally, easy to create the function that does the runtime-job task
    // assuming
    // C::e_attr enum is defined 
    // C::e_attr_seq_t is defined
    template <class C>
    bool object_attr_exists(C const&r, typename C::e_attr a) {
        static constexpr auto _attr_exists_fx =cmake_attr_exists_table<C,typename C::e_attr>(typename C::e_attr_seq_t{});
        return _attr_exists_fx[int(a)](r);
    }
    
    // now for the emitter:
    /** @brief emits a proxy attribute
     * 
     * @detail using the generator::emit(oi,T), or emit_null if the 
     *  proxy_attr is null (!exists()).
     * 
     */
    template<typename C,typename OutputIterator, typename E, E e>
    void emit_attr(C const &o,OutputIterator &oi) noexcept {
        auto const &a=o.*proxy_attr_m<C,E,e>::member();
        if(a.exists()) {
            shyft::web_api::generator::emit(oi,a.get());
        } else {
            shyft::web_api::generator::emit_null(oi);
        }
    }
    
    template<typename C,typename OutputIterator, typename E, int...a> 
    constexpr std::array<void(*)(C const&,OutputIterator&),sizeof...(a)>
    cmake_emit_attr_table(std::integer_sequence<int,a...>) { 
        return {{&emit_attr<C,OutputIterator,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }
    
    template<class C,typename OutputIterator>
    void emit_attr_for_object(C const&o,typename C::e_attr a, OutputIterator&oi) {
        static constexpr auto _attr_emit_fx =cmake_emit_attr_table<C,OutputIterator,typename C::e_attr>(typename C::e_attr_seq_t{});
        _attr_emit_fx[int(a)](o,oi);
    }
    
    
}

TEST_SUITE("stm_mp") {
TEST_CASE("stm_attribute_study") {
    // try to figure out the best possible query/response
    // patterns
    using namespace shyft::energy_market;
    using std::runtime_error;
    using shyft::core::utctime;
    using t_double = stm::t_double_::element_type;
    using t_xy=stm::t_xy_::element_type;

    utctime t0{ 0 };
    //--arrange the test
    auto sys=make_shared<stm::stm_system>(1,"first","");
    auto ull=make_shared<stm::stm_hps>(1,"Ulla-førre");
    ull->ids = make_shared<stm::hps_ds>();// attach empty ids
    sys->hps.push_back(ull);
    sys->market.push_back(make_shared<stm::energy_market_area>(1,"NO1","null",sys));
    auto b = make_shared<stm::reservoir>(16606,"blåsjø","",ull);
    ull->reservoirs.push_back(b);
    FAST_CHECK_EQ(b->hrl.exists(),false);// none set yet.
	auto t_d = make_shared<t_double>();
    (*t_d)[t0] = 1233.0;
	b->hrl = t_d;
     stm::t_double_ y=b->hrl;
        //-- assert
     FAST_CHECK_EQ((*y)[t0],doctest::Approx(1233.0));
     auto t_r_vol=make_shared<t_xy>();
     auto vol_curve=make_shared<xy_point_curve>(vector<double>{0.0,3500.0},vector<double>{1200.0,1234.0});
     (*t_r_vol)[t0]= vol_curve;
     b->volume_descr=t_r_vol;
     FAST_CHECK_EQ(b->volume_descr.exists(),true);
     
     //--- ok now we are ready for the  experiment section: 
     // imagine we got these two in the web-request.
     // knowing from request it was a reservoir, located in this system
     int r_attr=int(stm::rsv_attr::hrl);// from the request,
     int64_t oid=16606;
     // just imagine we looked up b from oid
     auto r=std::dynamic_pointer_cast<stm::reservoir>(sys->hps[0]->find_reservoir_by_id(oid));
     // this we like (could be any fx we want to apply to object.proxy_attr):
     FAST_CHECK_EQ(true,experiment::object_attr_exists(*r,static_cast<stm::rsv_attr>(r_attr)));
     FAST_CHECK_EQ(false,experiment::object_attr_exists(*r,stm::rsv_attr::lrl));
     FAST_CHECK_EQ(true,experiment::object_attr_exists(*r,stm::rsv_attr::volume_descr));
     string ps;
     auto  sink=std::back_inserter(ps);
     experiment::emit_attr_for_object(*r,stm::rsv_attr::hrl,sink);
     FAST_CHECK_EQ(true,ps.size()>0);
     // this works: std::cout<<"hrl:"<<ps<<"\n";
     
}

}
