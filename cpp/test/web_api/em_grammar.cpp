#include "test_pch.h"
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/energy_market/srv/model_info.h>

#include "test_parser.h"

using namespace shyft::core;

namespace shyft::web_api {
    using shyft::core::utcperiod;
    using shyft::core::utctime;
    using std::string;
    /** Request a list of all registered models 
    *
    * The response should be a list with model-infos.
    * 
    * TODO: consider if we really need server-side filtering, or just return all, and let client do filtering?
    */
    struct model_info_request {
        string request_id;///< client side request id, that is echoed back with the response
        string name_pattern;///< regex for the m.name to match, if empty then match any
        utcperiod created_range;///< specify interval for the m.created attribute, if not valid/empty then match any
        bool operator==(model_info_request const &o) const { return request_id==o.request_id && name_pattern==o.name_pattern && created_range==o.created_range;}
        bool operator!=(model_info_request const &o) const {return !this->operator==(o);}
        //TODO: add members to allow filter by created range + other stuff
    };

}

namespace shyft::web_api::grammar {

    /** @brief read ts request grammar
    *
    * Specifies a grammar sufficient to do a read/evaluate request, including
    * caching.
    * In this first version, the list of time-series is ts-urls. Later we could easily
    * make this a list of ts-expressions and then provide similar functionality over
    * the web-api as for the raw socket servers.
    *
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct model_info_request_grammar : public qi::grammar< Iterator, model_info_request(),Skipper> {
        model_info_request_grammar() ;
        qi::rule<Iterator, model_info_request(),Skipper> start;
        utcperiod_grammar<Iterator,Skipper> p_; // the grammar/rule for specifying time.
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    extern template struct model_info_request_grammar<request_iterator_t,request_skipper_t>;
}

namespace shyft::web_api::grammar {

    inline model_info_request mk_model_info_request(std::string const&req_id, utcperiod const& p,std::string const& pattern) {
        return model_info_request{req_id,pattern,p};
    }

    template<typename Iterator,typename Skipper>
    model_info_request_grammar<Iterator,Skipper>::model_info_request_grammar() : model_info_request_grammar::base_type(start,"model_info_request_request") {
        start = (
             lit("qinfo") >lit('{')
                > lit("\"request_id\"")    > ':' > quoted_string > ','
                > lit("\"created_range\"") > ':' > p_ > ','
                > lit("\"find_pattern\"" ) > ':' > quoted_string >
             lit('}')
        )
        [ _val = phx::bind(mk_model_info_request,_1,_2,_3) ];
        start.name("model_info_request");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct model_info_request_grammar<request_iterator_t,request_skipper_t>;
}
// elsewhere in .cpp


using std::vector;
using shyft::core::utctime;

TEST_SUITE("web_api_grammar") {
    TEST_CASE("stm_models_request") {
        shyft::web_api::grammar::model_info_request_grammar<const char*> req_;
        using shyft::web_api::model_info_request;
        model_info_request a;
        FAST_CHECK_EQ(true,test::phrase_parser(
        R"_(
        qinfo {
            "request_id": "a",
            "created_range": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
            "find_pattern" : "HydroModel-.*"
        }
        )_"
        ,req_,a));
        calendar utc;
        model_info_request e{"a","HydroModel-.*",utcperiod{utc.time(2018,10,9),utc.time(2018,11,1,2)}};
        FAST_CHECK_EQ(a,e);

    }
}
