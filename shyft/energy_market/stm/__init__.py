from os import environ, name as os_name
from pathlib import Path

lib_path = str((Path(__file__).parent.parent/'lib').absolute())

if os_name == 'nt':
    if lib_path not in environ['PATH']:
        environ['PATH'] = lib_path + ';' + environ['PATH']
else:
    if 'LD_LIBRARY_PATH' not in environ:
        environ['LD_LIBRARY_PATH'] = lib_path
    elif lib_path not in environ['LD_LIBRARY_PATH']:
        environ['LD_LIBRARY_PATH'] = lib_path + ':' + environ['LD_LIBRARY_PATH']
environ['ICC_COMMAND_PATH'] = lib_path

from ..core import _core  # need to pull in dependent  base-types
from ._stm import *


def wtr_self_input_from(wtr, o, cr=None):
    if cr is None:
        _core.wtr_input_from(wtr, o)
    else:
        _core.wtr_input_from(wtr, o, cr)
    return wtr


def wtr_self_output_to(wtr, o, cr=None):
    if cr is None:
        _core.wtr_output_to(wtr, o)
    else:
        _core.wtr_output_to(wtr, o, cr)
    return wtr

# backward compatible names after renaming
Aggregate=Unit
AggregateList=UnitList
WaterRoute=Waterway
PowerStation=PowerPlant
HydroPowerSystem.create_aggregate=HydroPowerSystem.create_unit
HydroPowerSystem.create_power_station=HydroPowerSystem.create_power_plant
HydroPowerSystem.create_water_route=HydroPowerSystem.create_waterway
# end backward compat section

Reservoir.input_from = lambda self, wtr: _core.rsv_input_from(self, wtr)
Reservoir.output_to = lambda self, wtr, role: _core.rsv_output_to(self, wtr, role)

Waterway.input_from = wtr_self_input_from
Waterway.output_to = wtr_self_output_to
