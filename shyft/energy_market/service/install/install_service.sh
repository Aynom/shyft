#!/usr/bin/env bash

service_name=em_model
user=srv_ltmapprod
group="domain users"

# We will install two files:
# 1) Service file to configure systemctl
service_file_name=${service_name}.service

# 2) Executable/script invoked by systemctl
executable_file_name=${service_name}.sh

# Full path of current script
script_path="$( cd "$(dirname "$0")" ; pwd -P )"

# Full path of files
service_file=${script_path}/${service_file_name}
executable=${script_path}/${executable_file_name}

# Check if the service is running
running=True
systemctl status ${service_name} > /dev/null 2>&1
if [[ $? -ne 0 ]]; then unset is_running; fi

if [[ ${running} ]]; then
    # Stop the service
    systemctl stop ${service_name}
fi

# Install service file
install -o "${user}" -g "${group}" -m 0644 "${service_file}" /etc/systemd/system/

# Install executable
install -o "${user}" -g "${group}" -m 0755 "${executable}" /usr/local/bin/

# Notify that the service file has changed
systemctl daemon-reload

# Register for start on boot
systemctl enable ${service_name}

if [[ ${running} ]]; then
    # Restart
    systemctl start ${service_name}
fi
